unit _wndMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  Vcl.StdCtrls, Vcl.ComCtrls,
  MSXML, Generics.Collections, Vcl.ExtCtrls, Vcl.Samples.Spin, IdCookieManager, IdCookie, FarmBotClasses,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxCustomData,
  cxStyles, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTextEdit,
  dxRibbonSkins, cxClasses, dxRibbon, dxBar, dxRibbonForm, System.Actions,
  Vcl.ActnList, dxStatusBar, dxRibbonStatusBar, Vcl.ImgList, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter;

type
  TwndFarmBot = class(TdxRibbonForm)
    pcWork: TPageControl;
    bmMain: TdxBarManager;
    rbtMain: TdxRibbonTab;
    rbMain: TdxRibbon;
    barUser: TdxBar;
    barActions: TdxBar;
    bbStart: TdxBarLargeButton;
    alMain: TActionList;
    actStart: TAction;
    bbStop: TdxBarLargeButton;
    actStop: TAction;
    sbMain: TdxRibbonStatusBar;
    dxBarLargeButton1: TdxBarLargeButton;
    actCustomize: TAction;
    ilLarge: TImageList;
    TrayIcon: TTrayIcon;
    bbClearLog: TdxBarLargeButton;
    actClearLog: TAction;
    rpmClear: TdxRibbonPopupMenu;
    actClearAllLog: TAction;
    bbClearAllLog: TdxBarButton;
    ilSmall: TImageList;
    rpmStartAll: TdxRibbonPopupMenu;
    rpmStopAll: TdxRibbonPopupMenu;
    actStartAll: TAction;
    actStopAll: TAction;
    bbStartAll: TdxBarLargeButton;
    bbStopAll: TdxBarLargeButton;
    bbIgnoreSiesta: TdxBarLargeButton;
    actIgnorSiesta: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actStartExecute(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
    procedure actStartUpdate(Sender: TObject);
    procedure actStopUpdate(Sender: TObject);
    procedure UserButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actCustomizeUpdate(Sender: TObject);
    procedure actCustomizeExecute(Sender: TObject);
    procedure TrayIconDblClick(Sender: TObject);
    procedure ApplicationMinimize(Sender: TObject);
    procedure actClearLogExecute(Sender: TObject);
    procedure actClearAllLogExecute(Sender: TObject);
    procedure actStartAllExecute(Sender: TObject);
    procedure actStopAllExecute(Sender: TObject);
    procedure actIgnorSiestaExecute(Sender: TObject);
  private
    FSettings: IXMLDOMDocument3;
    FFarmUserList: TList<TFarmUser>;
    FServerUpdRev: Integer;

    procedure LoadUsers;
    procedure CreateUser(const aXMLNode: IXMLDOMElement);

//    procedure ProcessHive(const AFieldNode: IXMLDOMNode);
  end;

var
  wndFarmBot: TwndFarmBot;

implementation

{$R *.dfm}

const
  c_MarinaFlashVars = 'api%5Fserver%3Dhttp%3A%2F%2Fapi%2Eodnoklassniki%2E' +
    'ru%2Fapiconnection%3D41984%5F1360311330513application%5Fkey%3DCBAFKBABABABABABAauth%5F' +
    'sig%3D3630cddfdd0ef2a63115ea8a4768a622authorized%3D1clientLog%3D0first%5Fstart%3D0' +
    'ip%5Fgeo%5Flocation%3DUA%2C12%2CKievlogged%5Fuser%5Fid%3D891977924495495650refplace%3D' +
    'user%5Fappssession%5Fkey%3DdLXNmWNFDCYpI7TlCGeNkBNijo9rg6NFgGaIifxFgFguIATmgpeNG7Slgmbsession%5F' +
    'secret%5Fkey%3D04ac1fcde49a7c1b76a1680c4c1af712web%5Fserver%3Dwww%2Eodnoklassniki%2Eru&';

  c_VadimFlashVars = 'api%5Fserver%3Dhttp%3A%2F%2Fapi%2Eodnoklassniki%2Eru%2F' +
        'apiconnection%3D41984%5F1360242900897application%5Fkey%3DCBAFKBABABABABABAauth%5F' +
        'sig%3D6821f6eceb4026f30f81451e48216dd7authorized%3D1clientLog%3D0first%5Fstart%3D0ip%5F' +
        'geo%5Flocation%3DUA%2C12%2CKievlogged%5Fuser%5Fid%3D891977944520640563refplace%3Duser%5F' +
        'appssession%5Fkey%3D2DBujeuFgo9qgfwBhq6MF7MDjIdIj7OhfmbtH8ziCGcpgeQAcDXNIBN6jCcsession%5Fsecret%5F' +
        'key%3D3b6fa372b41958c4907ab48341ef52e8web%5Fserver%3Dwww%2Eodnoklassniki%2Eru&';


  c_LenaFlashVars = 'api%5Fserver%3Dhttp%3A%2F%2Fapi%2Eodnoklassniki%2Eru%2Fapiconnection' +
        '%3D41984%5F1360583667098application%5Fkey%3DCBAFKBABABABABABAauth%5Fsig%3Dcf1d73' +
        '37e19359510d6e81c36f805c26authorized%3D1clientLog%3D0first%5Fstart%3D0ip%5Fgeo%5' +
        'Flocation%3DUA%2C12%2CKievlogged%5Fuser%5Fid%3D891977945332279256refplace%3Duser' +
        '%5Fappssession%5Fkey%3D6CXNgbyEgKfukcNFkJgsmdMiEE7uHcv7epculZT9fLZFg9QDiCBpi7O6B' +
        'pcsession%5Fsecret%5Fkey%3Dd9404202001eabd6131294bd8499cb54web%5Fserver%3Dwww%2E' +
        'odnoklassniki%2Eru';





//procedure TwndFarmBot.ProcessHive(const AFieldNode: IXMLDOMNode);
//var
//  i, J, ChldID, nCount, CloverID: Integer;
//  L, LClover: IXMLDOMNodeList;
//  nOutputFill, nChild, nClover: IXMLDOMNode;
//  Cmd: TCachedCommand;
//  ProcessedIDs: TList<Integer>;
//begin
//  if not chbHoney.Checked then Exit;
//
//  L := AFieldNode.selectNodes('hive');
//  if not Assigned(L) then
//    Exit;
//
//
//  ProcessedIDs := TList<Integer>.Create;
//  LClover := AFieldNode.selectNodes('clover');
//  try
//    for I := 0 to L.length - 1 do
//    begin
//      nChild := L.item[i];
//      ChldID := nChild.attributes.getNamedItem('id').nodeValue;
//
//      nOutputFill := nChild.attributes.getNamedItem('output_fill');
//      if Assigned(nOutputFill) then
//      begin
//        nCount := Integer(nOutputFill.nodeValue);
//        for J := 0 to nCount - 1 do
//        begin
//          Cmd := TCachedCommand.Create(Self);
//          Cmd.ItemID := ChldID;
//          Cmd.Room := 0;
//          Cmd.Command := 'pick';
//          Cmd.ItemClass := '';
//
//          FCmdList.Add(Cmd);
//        end;
//      end;
//
//      for J := 0 to LClover.length - 1 do
//      begin
//        nClover := LClover.item[J];
//
//        if not Assigned(nClover.attributes.getNamedItem('output_fill')) then
//          Continue;
//
//        if Assigned(nClover.attributes.getNamedItem('pollinated')) then
//          Continue;
//
//        CloverID := nClover.attributes.getNamedItem('id').nodeValue;
//
//        if ProcessedIDs.IndexOf(CloverID) >= 0 then
//          Continue;
//
//
//        Cmd := TCachedCommand.Create(Self);
//        Cmd.ItemID := ChldID;
//        Cmd.Room := 0;
//        Cmd.Command := 'bee_pollinate';
//        Cmd.ItemClass := '';
//        Cmd.SecondID := CloverID;
//
//        FCmdList.Add(Cmd);
//
//        ProcessedIDs.Add(CloverID);
//        Break;
//      end;{for }
//    end;
//  finally
//    L := nil;
//    LClover := nil;
//    ProcessedIDs.Free;
//  end;
//end;

procedure TwndFarmBot.UserButtonClick(Sender: TObject);
begin
  pcWork.ActivePage := TFarmUser(TdxBarLargeButton(Sender).Tag).TabPage;
end;


procedure TwndFarmBot.actClearAllLogExecute(Sender: TObject);
var
  P: TUserTabSheet;
  i: Integer;
begin
  for i := 0 to pcWork.PageCount - 1 do
  begin
    P := TUserTabSheet(pcWork.Pages[i]);
    P.User.ClearLog;
  end;
end;

procedure TwndFarmBot.actClearLogExecute(Sender: TObject);
var
  U: TFarmUser;
begin
  if Assigned(pcWork.ActivePage) then
  begin
    U := TUserTabSheet(pcWork.ActivePage).User;
    U.ClearLog;
  end;
end;

procedure TwndFarmBot.actCustomizeExecute(Sender: TObject);
var
  U: TFarmUser;
  Itm: TKnownElementSimple;
begin
  if Assigned(pcWork.ActivePage) then
  begin
    U := TUserTabSheet(pcWork.ActivePage).User;
    if not Assigned(U.Tree.FocusedNode) then Exit;

    if not (TObject(U.Tree.FocusedNode.Data) is TKnownElementSimple) then Exit;

    Itm := TKnownElementSimple(U.Tree.FocusedNode.Data);
    if not Assigned(Itm) or not Itm.CanCustomize then Exit;

    Itm.Customize;
  end;
end;

procedure TwndFarmBot.actCustomizeUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := Assigned(pcWork.ActivePage) and
    Assigned(TUserTabSheet(pcWork.ActivePage).User.Tree.FocusedNode) and
    (TObject(TUserTabSheet(pcWork.ActivePage).User.Tree.FocusedNode.Data) is TKnownElementSimple) and
    TKnownElementSimple(TUserTabSheet(pcWork.ActivePage).User.Tree.FocusedNode.Data).CanCustomize;
end;

procedure TwndFarmBot.actIgnorSiestaExecute(Sender: TObject);
var
  P: TUserTabSheet;
  i: Integer;
begin
  actIgnorSiesta.Checked := not actIgnorSiesta.Checked;
  for i := 0 to pcWork.PageCount - 1 do
  begin
    P := TUserTabSheet(pcWork.Pages[i]);
//    if not P.User.IsRunning then
      P.User.IgnoreSiesta := actIgnorSiesta.Checked;
  end;
end;

procedure TwndFarmBot.actStartAllExecute(Sender: TObject);
var
  P: TUserTabSheet;
  i: Integer;
begin
  for i := 0 to pcWork.PageCount - 1 do
  begin
    P := TUserTabSheet(pcWork.Pages[i]);
    if not P.User.IsRunning then
      P.User.Start;
  end;
end;

procedure TwndFarmBot.actStartExecute(Sender: TObject);
var
  U: TFarmUser;
begin
  if not Assigned(pcWork.ActivePage) then Exit;

  U := TUserTabSheet(pcWork.ActivePage).User;

  if not U.IsRunning then
    U.Start;
end;

procedure TwndFarmBot.actStartUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := Assigned(pcWork.ActivePage) and
    not TUserTabSheet(pcWork.ActivePage).User.IsRunning;
end;

procedure TwndFarmBot.actStopAllExecute(Sender: TObject);
var
  P: TUserTabSheet;
  i: Integer;
begin
  for i := 0 to pcWork.PageCount - 1 do
  begin
    P := TUserTabSheet(pcWork.Pages[i]);
    if P.User.IsRunning then
      P.User.Stop;
  end;
end;

procedure TwndFarmBot.actStopExecute(Sender: TObject);
var
  U: TFarmUser;
begin
  if not Assigned(pcWork.ActivePage) then Exit;

  U := TUserTabSheet(pcWork.ActivePage).User;

  if U.IsRunning then
    U.Stop;
end;

procedure TwndFarmBot.actStopUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := Assigned(pcWork.ActivePage) and
    TUserTabSheet(pcWork.ActivePage).User.IsRunning;
end;

procedure TwndFarmBot.ApplicationMinimize(Sender: TObject);
begin
  Hide;
end;

procedure TwndFarmBot.CreateUser(const aXMLNode: IXMLDOMElement);
var
  Us: TFarmUser;
  BB: TdxBarLargeButton;
begin
  if  VarToStr(aXMLNode.getAttribute('hidden')) = '1' then
    Exit;

  Us := TFarmUser.Create(aXMLNode.nodeName, FServerUpdRev, pcWork);
  Us.LoadSettings(aXMLNode);

//  CreateUser('Vadim', '891977944520640563', '9944282d522b503a57619df3f44172e9', c_VadimFlashVars);
//  CreateUser('Lena', '891977945332279256', '84288dfb57ddf50d0d148ba161f76335', c_LenaFlashVars);
//  CreateUser('Marina', '891977924495495650', '7ae374121a1c86f52c13a4762a811ac1', c_MarinaFlashVars);

//  if SameText(Us.Name, 'vadim') then
//  begin
//    Us.AuthKey := '9944282d522b503a57619df3f44172e9';
//    Us.FlashVars := c_VadimFlashVars;
//  end else
//
//  if SameText(Us.Name, 'lena') then
//  begin
//    Us.AuthKey := '84288dfb57ddf50d0d148ba161f76335';
//    Us.FlashVars := c_LenaFlashVars;
//    Us.Login := 'shao@ua.fm';
//    Us.Password := '9212cn';
//  end else
//
//  if SameText(Us.Name, 'marina') then
//  begin
//    Us.AuthKey := '7ae374121a1c86f52c13a4762a811ac1';
//    Us.FlashVars := c_MarinaFlashVars;
//    Us.Login := 'star_4@urk.net';
//    Us.Password := 'vfcz-marina1';
//  end;

  Us.ShowKnownElements;
  FFarmUserList.Add(Us);

  BB := TdxBarLargeButton.Create(Self);
  BB.Tag := Integer(US);
  BB.LargeImageIndex := 3;
  BB.Caption := Us.Name;
  BB.ButtonStyle := bsChecked;
  BB.AllowAllUp := False;
  BB.GroupIndex := 1;
//  BB.Down := True;
  BB.OnClick := UserButtonClick;
  barUser.ItemLinks.Add.Item := BB;
end;

procedure TwndFarmBot.FormCreate(Sender: TObject);
begin
  Application.OnMinimize := ApplicationMinimize;

  FFarmUserList := TList<TFarmUser>.Create;

  FSettings := CoDOMDocument60.Create;
  FSettings.load(ChangeFileExt(Application.ExeName, '.xml'));

  LoadUsers;

  TdxBarLargeButton(barUser.ItemLinks[0].Item).Down := True;
  pcWork.Align := alClient;
  pcWork.ActivePageIndex := 0;
end;

procedure TwndFarmBot.FormDestroy(Sender: TObject);
var
  U: TFarmUser;
  nRoot: IXMLDOMElement;
begin
  nRoot := FSettings.selectSingleNode('farm_bot') as IXMLDOMElement;
  if not Assigned(nRoot) then
  begin
    nRoot := FSettings.createElement('farm_bot');
    FSettings.appendChild(nRoot);
  end;

  for U in FFarmUserList do
    U.SaveSettings(nRoot);

  FSettings.save(ChangeFileExt(Application.ExeName, '.xml'));
  FSettings := nil;

  FFarmUserList.Free;
end;

procedure TwndFarmBot.LoadUsers;
var
  i: Integer;
  nRoot: IXMLDOMNode;
  nUser: IXMLDOMElement;
begin
  nRoot := FSettings.selectSingleNode('farm_bot');
  if not Assigned(nRoot) then Exit;


  FServerUpdRev := (nRoot as IXMLDOMElement).getAttribute('server_update_revision');

  for I := 0 to nRoot.childNodes.length - 1 do
  begin
    nUser := nRoot.childNodes.item[i] as IXMLDOMElement;

    CreateUser(nUser);
  end;
end;

procedure TwndFarmBot.TrayIconDblClick(Sender: TObject);
begin
  Application.Restore;
  Show;
  Application.BringToFront;
end;

procedure TwndFarmBot.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  i: Integer;
  U: TFarmUser;
begin
  for i := 0 to pcWork.PageCount - 1 do
  begin
    U := TUserTabSheet(pcWork.Pages[i]).User;
    if not U.IsRunning then Continue;

    U.Stop;

    while U.IsRunning do
      Sleep(2);
  end;
end;

//function TwndFarmBot.Login: Boolean;
//var
//  R: TStringStream;
//begin
//  R := TStringStream.Create;
//  try
//    if FSessionKey = '' then
//    begin
//      WriteLog('Login...');
////      DetectRevision;
//      idHttp1.Get('http://88.212.226.153/assets/app.html?authorized=1&' +
//        'api_server=http%3A%2F%2Fapi.odnoklassniki.ru%2F&ip_geo_location=UA%2C12%2CKiev&' +
//        'apiconnection=41984_1360244739122&first_start=0&session_secret_key=e933aad7f02c8edb4142c76f3cc6c994&' +
//        'clientLog=0&refplace=user_apps&application_key=CBAFKBABABABABABA&auth_sig=01105d54ff9ceb9a07054d69f49ea339&' +
//        'session_key=fFBHhfvjgm9uJaOiDn7NmASlDmdpEcvDkF9MmauAiKfFieyADF8tm6M6kFc&logged_user_id=891977924495495650&' +
//        'web_server=www.odnoklassniki.ru&sig=bc4e4e5857e04e2679403775cb7351de', R);
//
////      idHttp1.Get('http://88.212.226.153/assets/app.html?authorized=1&' +
////        'api_server=http%3A%2F%2Fapi.odnoklassniki.ru%2F&ip_geo_location=UA%2C12%2CKiev&apiconnection=41984_1360052285740&' +
////        'first_start=0&session_secret_key=5015372ff43f39d4907361b0044474ea&clientLog=0&refplace=user_apps&' +
////        'application_key=CBAFKBABABABABABA&auth_sig=924ab0e2bc68b6faffb9aa04607da8d4&' +
////        'session_key=1pZsifwBjp7NfeKEDB9NIbPCcm7IkfxAjFdMi8SicG9ulAOChE6KEAO7dpa&logged_user_id=891977944520640563&' +
////        'web_server=www.odnoklassniki.ru&sig=31bf25c8fda8ce2751785fd57a7df1cc', R);
//
////      R.Clear;
//    end;
//  finally
//    R.Free;
//  end;
//end;
//


end.
