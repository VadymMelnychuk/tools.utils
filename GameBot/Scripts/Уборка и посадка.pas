const
  x_Offset = 36.75;
  y_Offset = 19.25;
  SizeX = 10;
  SizeY = 10;

function Min(A, B: Integer): Integer;
begin
  if A < B then Result := A else Result := B;
end;
  
var
  Wnd,
  StartX, StartY, CurX, CurY,
  i, J, I_Part, J_Part,
  XPartCnt, YPartCnt: Integer;
begin
  GetMousePos(StartX, StartY);
  Wnd := GetWindowFromPos(StartX, StartY);
  ScreenToClient(Wnd, StartX, StartY);

  XPartCnt := SizeX div 10;
  if (SizeX mod 10) > 0 then
    Inc(XPartCnt);
  YPartCnt := SizeY div 10;
  if (SizeY mod 10) > 0 then
    Inc(YPartCnt);

  I_Part := 0;
  while I_Part <= YPartCnt - 1 do
  begin
    J_Part := 0;
    while J_Part <= XPartCnt - 1 do
    begin
      for i := 0 to Min(9, SizeY - I_Part * 10 - 1) do
      begin
        for j := 0 to Min(9, SizeX - J_Part * 10 - 1) do
        begin
          MouseClickEx(Wnd, Trunc(StartX + J * X_Offset + I * X_Offset),
            Trunc(StartY - J * Y_Offset + I * Y_Offset), 1);
        end;{for j}
      end;{for i}

      Pause(3000);

      // move map to next horizontal block
      if J_Part < XPartCnt - 1 then
      begin
        MoveMapEx(Wnd, Trunc(StartX + 10 * X_Offset),
          Trunc(StartY - 10 * Y_Offset), StartX, StartY);
      end;

      Inc(J_Part);
    end;{while j_part}

    // move map to first horizontal block
    for I := 0 to XPartCnt - 2 do
    begin
      MoveMapEx(Wnd, StartX, StartY, Trunc(StartX + 10 * X_Offset),
        Trunc(StartY - 10 * Y_Offset));
    end;

    // move map to next vertical block
    if I_Part < YPartCnt - 1 then
    begin
      MoveMapEx(Wnd, Trunc(StartX + 10 * X_Offset),
        Trunc(StartY + 10 * Y_Offset), StartX, StartY);
    end;
    Inc(I_Part);
  end;{while i_part}
end.  