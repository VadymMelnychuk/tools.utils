program AutoBuild;

uses
  Vcl.Forms,
  _wndMain in '_wndMain.pas' {wndMain},
  Vcl.Themes,
  Vcl.Styles,
  Vcl.SvcMgr,
  System.SysUtils,
  AutoBuildClasses in 'AutoBuildClasses.pas',
  _dlgCommandEdit in '_dlgCommandEdit.pas' {dlgCommandEdit},
  TaskbarListIntf in '..\_common\TaskbarListIntf.pas',
  D2CommonUtils in '..\_common\D2CommonUtils.pas',
  _svcMain in '_svcMain.pas' {svcAutoBuilder: TService},
  _UniServiceParams in '..\_common\_UniServiceParams.pas';

{$R *.res}

var
  P1, S: string;
begin
  P1 := '';
  if ParamCount > 0 then
  begin
    P1 := LowerCase(ParamStr(1));
    if P1 <> '' then
    begin
      if CharInSet(P1[1], ['/', '-', '\'])  then
        Delete(P1, 1, 1);
    end;
  end;

  S := 'Auto bilder';

  if (P1 = 'service') or (P1 = 'install') or (P1 = 'uninstall') then
  begin
    if not Vcl.SvcMgr.Application.DelayInitialize or Vcl.SvcMgr.Application.Installing then
      Vcl.SvcMgr.Application.Initialize;
    Vcl.SvcMgr.Application.Title := S;
    Vcl.SvcMgr.Application.CreateForm(TsvcAutoBuilder, svcAutoBuilder);
  Vcl.SvcMgr.Application.Run;
  end else begin
    Vcl.Forms.Application.Initialize;
    Vcl.Forms.Application.Title := S;
    Vcl.Forms.Application.MainFormOnTaskbar := True;
    Vcl.Forms.Application.CreateForm(TwndMain, wndMain);
    Vcl.Forms.Application.Run;
  end;
end.
