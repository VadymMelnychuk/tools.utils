unit formMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, ComCtrls, ShlObj, cxShellCommon, dxSkinsCore,
  dxSkinsDefaultPainters, cxShellListView, cxContainer, cxShellTreeView,
  ShellAnimations, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter, dxBar, dxRibbon,
  dxRibbonGallery, dxStatusBar, dxRibbonStatusBar, cxSplitter, cxClasses, dxRibbonForm,
  ImgList, ActnList;

type
  TwndMain = class(TdxRibbonForm)
    tlShell: TcxShellTreeView;
    lvShell: TcxShellListView;
    bmMain: TdxBarManager;
    barMain: TdxBar;
    cxSplitter1: TcxSplitter;
    rbtMain: TdxRibbonTab;
    Ribbon: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    bbExit: TdxBarButton;
    brAppMenu: TdxBarApplicationMenu;
    bbConvert: TdxBarLargeButton;
    barQuickAccess: TdxBar;
    barOptions: TdxBar;
    LookAndFeelController: TcxLookAndFeelController;
    bbPreserveForm: TdxBarButton;
    bbNoEpigraphs: TdxBarButton;
    bbNoDescription: TdxBarButton;
    bbNoConvertCharset: TdxBarButton;
    bbNoEmptyLines: TdxBarButton;
    bbFixCount: TdxBarButton;
    bbNoQuotesConvertion: TdxBarButton;
    bbNoFootNotes: TdxBarButton;
    bbNoItalic: TdxBarButton;
    bbNoPoems: TdxBarButton;
    bbNoRestoreBrokenParagraphs: TdxBarButton;
    bbNoHeaders: TdxBarButton;
    bbIgnoreLineIndent: TdxBarButton;
    bbNoLongDashes: TdxBarButton;
    bbTextStruct: TdxBarCombo;
    ActionList1: TActionList;
    actConvert: TAction;
    rbtTextOptions: TdxRibbonTab;
    rbtLinkOptions: TdxRibbonTab;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bbExitClick(Sender: TObject);
    procedure bbOptionsClick(Sender: TObject);
    procedure actConvertUpdate(Sender: TObject);
    procedure actConvertExecute(Sender: TObject);
  private
    FAny2FB2Obj: Variant;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  wndMain: TwndMain;

implementation

{$R *.dfm}
{$R windowsxp.res}

uses
  ComObj, XMLIntf;

procedure TwndMain.FormCreate(Sender: TObject);
begin
  FAny2FB2Obj := CreateOleObject('any_2_fb2.any2fb2');
end;

procedure TwndMain.FormDestroy(Sender: TObject);
begin
  FAny2FB2Obj := Unassigned;
end;

procedure TwndMain.actConvertExecute(Sender: TObject);
var
  i: Integer;
  S: string;
  DOM: Variant;
  Itm: TListItem;
  Pidl: PItemIDList;
  OldCur: TCursor;
begin
//  FAny2FB2Obj.ConvertInteractive(0, True);

  if lvShell.InnerListView.SelCount <= 0 then Exit;

  OldCur := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    for i := 0 to lvShell.InnerListView.Items.Count - 1 do
    begin
      Itm := lvShell.InnerListView.Items[i];
      if not Itm.Selected then Continue;

      Pidl := lvShell.GetItemAbsolutePIDL(Itm.Index);
      if not assigned(Pidl) then Continue;

      S := GetPidlName(Pidl);
      if not FileExists(S) then Continue;

      DoM := FAny2FB2Obj.Convert(S);
      if VarIsType(Dom, varDispatch) and Assigned(TVarData(Dom).VDispatch) then
        Dom.save(ChangeFileExt(S, '.fb2'));
      Dom := Unassigned;
    end;{for i}
  finally
    Screen.Cursor := OldCur;
    ShowMessage('Complete');
  end;
end;

procedure TwndMain.actConvertUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := lvShell.InnerListView.SelCount > 0;
end;

procedure TwndMain.bbExitClick(Sender: TObject);
begin
  Close;
end;

procedure TwndMain.bbOptionsClick(Sender: TObject);
begin
  if Sender = bbPreserveForm then
		FAny2FB2Obj.PreserveForm := TdxBarButton(Sender).Down
  else

  if Sender = bbNoEpigraphs then
		FAny2FB2Obj.NoEpigraphs := TdxBarButton(Sender).Down
  else

  if Sender = bbNoDescription then
		FAny2FB2Obj.NoDescription := TdxBarButton(Sender).Down
  else

  if Sender = bbNoConvertCharset then
		FAny2FB2Obj.NoConvertCharset := TdxBarButton(Sender).Down
  else

  if Sender = bbNoEmptyLines then
		FAny2FB2Obj.NoEmptyLines := TdxBarButton(Sender).Down
  else

  if Sender = bbFixCount then
		FAny2FB2Obj.FixCount := TdxBarButton(Sender).Down
  else

  if Sender = bbNoQuotesConvertion then
		FAny2FB2Obj.NoQuotesConvertion := TdxBarButton(Sender).Down
  else

  if Sender = bbNoFootNotes then
		FAny2FB2Obj.NoFootNotes := TdxBarButton(Sender).Down
  else

  if Sender = bbNoItalic then
		FAny2FB2Obj.NoItalic := TdxBarButton(Sender).Down
  else

  if Sender = bbNoRestoreBrokenParagraphs then
		FAny2FB2Obj.NoRestoreBrokenParagraphs := TdxBarButton(Sender).Down
  else

  if Sender = bbNoPoems then
		FAny2FB2Obj.NoPoems := TdxBarButton(Sender).Down
  else

  if Sender = bbNoHeaders then
		FAny2FB2Obj.NoHeaders := TdxBarButton(Sender).Down
  else

  if Sender = bbIgnoreLineIndent then
		FAny2FB2Obj.IgnoreLineIndent := TdxBarButton(Sender).Down
  else

  if Sender = bbNoLongDashes then
		FAny2FB2Obj.NoLongDashes := TdxBarButton(Sender).Down
  else

  if (Sender = bbTextStruct) and (bbTextStruct.ItemIndex > 0) then
		FAny2FB2Obj.TextType := bbTextStruct.ItemIndex;
end;

end.
