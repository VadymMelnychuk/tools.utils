unit _dlgCustomCommand;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TdlgCustomCommand = class(TForm)
    memCustomCommand: TMemo;
    btnOK: TButton;
    btnCancel: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function ShowCustomCommand(var ACommand: string): Boolean;

implementation

{$R *.dfm}

function ShowCustomCommand(var ACommand: string): Boolean;
var
  D: TdlgCustomCommand;
begin
  D := TdlgCustomCommand.Create(Application);
  try
    D.memCustomCommand.Lines.Delimiter := '&';
    D.memCustomCommand.Lines.DelimitedText := ACommand;

    Result := D.ShowModal = mrOK;

    ACommand := D.memCustomCommand.Lines.DelimitedText;
  finally
    D.Free;
  end;
end;

end.
