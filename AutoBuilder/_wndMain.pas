unit _wndMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  dxRibbon, dxRibbonForm, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTLdxBarBuiltInMenu, dxRibbonSkins, cxPCdxBarPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxPC, dxBar, cxClasses, cxInplaceContainer, cxCheckBox,
  cxTextEdit, Vcl.StdCtrls, System.Generics.Collections, System.Generics.Defaults,
  Vcl.ActnList, Vcl.ImgList,
  cxImageComboBox, AutoBuildClasses, cxContainer, cxEdit, cxMemo,
  TaskbarListIntf;

type

  TwndMain = class(TdxRibbonForm, IabTaskBarProgress)
    tlCommands: TcxTreeList;
    RibbonTab1: TdxRibbonTab;
    Ribbon: TdxRibbon;
    BarManager: TdxBarManager;
    barMain: TdxBar;
    RibbonStatusBar: TdxRibbonStatusBar;
    bbRun: TdxBarLargeButton;
    bbCheckAll: TdxBarLargeButton;
    cnCheck: TcxTreeListColumn;
    clnName: TcxTreeListColumn;
    clnCmd: TcxTreeListColumn;
    clnParams: TcxTreeListColumn;
    il24: TImageList;
    alMain: TActionList;
    actExecute: TAction;
    il16: TImageList;
    clnImage: TcxTreeListColumn;
    bbUncheckAll: TdxBarLargeButton;
    actCheckAll: TAction;
    actUncheckAll: TAction;
    bbSaveAs: TdxBarLargeButton;
    bbLoad: TdxBarLargeButton;
    actSaveAs: TAction;
    actLoad: TAction;
    dlgOpen: TOpenDialog;
    dlgSave: TSaveDialog;
    memOut: TcxMemo;
    cxLookAndFeelController1: TcxLookAndFeelController;
    btnEdit: TdxBarLargeButton;
    actEditCmd: TAction;
    bbConnect: TdxBarLargeButton;
    actConnect: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tlCommandsFocusedNodeChanged(Sender: TcxCustomTreeList;
      APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure FormShow(Sender: TObject);
    procedure tlCommandsCollapsing(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; var Allow: Boolean);
    procedure cnCheckPropertiesChange(Sender: TObject);
    procedure actExecuteExecute(Sender: TObject);
    procedure actUncheckAllExecute(Sender: TObject);
    procedure actLoadExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actSaveAsExecute(Sender: TObject);
    procedure actEditCmdExecute(Sender: TObject);
    procedure actEditCmdUpdate(Sender: TObject);
    procedure actConnectExecute(Sender: TObject);
  private
    FDlg: TTaskDialog;
    FCmdParams: TDictionary<string, string>;
    FActionNode: TDictionary<TabBaseAction, TcxTreeListNode>;
    FExec: TLocalActionsExecutor<TabLocalUIAction>;
    FRemoteExec: TRemoteActionsExecutor<TabRemoteAction>;
    FTaskBarList: ITaskBarList4;
    FLastServer: string;
    procedure SL_Change(Sender: TObject);
    procedure PopulateActions<T: TabBaseAction>(Arr: TArray<TPair<string, T>>);
    procedure LoadConfig(const AFile: string);
    procedure SaveConfig(const AFile: string);
    procedure OnStateChangeEvent<T: TabBaseAction>(Action: T);
    procedure UpdateTaskBarProgress(APosition, ATotal: Integer);
  public
    { Public declarations }
  end;

  TCommandComparer<TValue> = class(TComparer<TPair<string, TValue>>)
  public
    function Compare(const Left, Right: TPair<string, TValue>): Integer; override;
  end;

var
  wndMain: TwndMain;

implementation

{$R *.dfm}

uses
  janXMLParser2, Math, ComObj,
  D2CommonUtils, _dlgCommandEdit;



{ TwndMain }

procedure TwndMain.actConnectExecute(Sender: TObject);
//var
//  F1, F2: TUniServiceParams;
begin
//  F1 := TUniServiceParams.Create;
//  F2 := TUniServiceParams.Create;
//  try
//    F1.AddParam('MSG_ID', 1);
//    F1.AddParam('VALUE', True);
//
//    F2.Assign(F1);
//  finally
//    F1.Free;
//    F2.Free;
//  end;
//  exit;
//
  if not InputQuery('Remote server', 'Connect to remote server', FLastServer) then
    Exit;

  //F
end;

procedure TwndMain.actEditCmdExecute(Sender: TObject);
var
  ProjCmd: TabBaseAction;
  N, C, T: string;
begin
  if not Assigned(tlCommands.FocusedNode) then Exit;

  ProjCmd := TabBaseAction(tlCommands.FocusedNode.Data);
  if ProjCmd is TabLocalUIAction then
  begin
    N := ProjCmd.Name;
    C := ProjCmd.Code;
    T := TabLocalUIAction(ProjCmd).CommandText;
    if not ShowCmdEditor(C, N, T) then Exit;

    ProjCmd.Name := N;
    ProjCmd.Code := C;
    TabLocalUIAction(ProjCmd).CommandText := T;
    tlCommands.FocusedNode.Values[1] := N;
  end;
end;

procedure TwndMain.actEditCmdUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := Assigned(tlCommands.FocusedNode);
end;

procedure TwndMain.actExecuteExecute(Sender: TObject);
var
  I: Integer;
  Nd: TcxTreeListNode;
  ProjCmd: TabBaseAction;
begin
  for I := 0 to tlCommands.Count - 1 do
  begin
    ND := tlCommands.Items[i];

    ProjCmd := TabBaseAction(ND.Data);

    if not (ND.Values[0]) then
    begin
      ProjCmd.State := csSkipped;
      Continue;
    end;

    ProjCmd.State := csWait;
  end;

  if not Assigned(FDlg) then
  begin
    FDlg := TTaskDialog.Create(nil);
    FExec.Dialog := FDlg;
  end;

  FExec.Execute;
end;

procedure TwndMain.actLoadExecute(Sender: TObject);
begin
  if dlgOpen.Execute then
    LoadConfig(dlgOpen.FileName);
end;

procedure TwndMain.actSaveAsExecute(Sender: TObject);
begin
  if dlgSave.Execute then
    SaveConfig(dlgSave.FileName);
end;

procedure TwndMain.actUncheckAllExecute(Sender: TObject);
var
  i: Integer;
  J: Integer;
  ND: TcxTreeListNode;
begin
  tlCommands.BeginUpdate;
  try
    for I := 0 to tlCommands.Count - 1 do
    begin
      ND := tlCommands.Items[i];
      ND.Values[0] := (TAction(Sender).Tag = 1);
      for J := 0 to ND.Count - 1 do
        ND.Items[J].Values[0] := (TAction(Sender).Tag = 1);
    end;
  finally
    tlCommands.EndUpdate;
  end;
end;

procedure TwndMain.cnCheckPropertiesChange(Sender: TObject);
var
  i: Integer;
  R, ND: TcxTreeListNode;
  B: Boolean;
begin
  ND := tlCommands.FocusedNode;
  if not Assigned(ND) then Exit;

  if Nd.Level = 0 then
  begin
    for I := 0 to ND.Count - 1 do
      ND.Items[i].Values[0] := ND.Values[0];
  end else

  if Nd.Level = 1 then
  begin
    R := ND.Parent;
    B := Boolean(ND.Values[0]);
    for I := 0 to R.Count - 1 do
      if Boolean(R.Items[i].Values[0]) <> B then
        Exit;
    R.Values[0] := B;
  end;
end;

procedure TwndMain.SL_Change(Sender: TObject);
begin
  memOut.Clear;
  memOut.Lines.AddStrings(TStringList(Sender));
end;

procedure TwndMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Assigned(FTaskBarList) then
    FTaskBarList := nil;

  SaveConfig('');
end;

procedure TwndMain.FormCreate(Sender: TObject);
var
  i, N: Integer;
  S: string;
  TBIntf: ITaskbarList;
begin
  FCmdParams := TDictionary<string, string>.Create;
  FExec := TLocalActionsExecutor<TabLocalUIAction>.Create;
  FExec.OnStateChange := OnStateChangeEvent<TabLocalUIAction>;
  FRemoteExec := nil;

  FActionNode := TDictionary<TabBaseAction, TcxTreeListNode>.Create;
  for I := 1 to ParamCount do
  begin
    S := ParamStr(I);
    if CharInSet(S[1], ['-', '/', '\']) then
      Delete(S, 1, 1);

    N := Pos(':', S);
    if N > 0 then
      FCmdParams.Add(UpperCase(Copy(S, 1, N - 1)), Copy(S, N + 1, Length(S) - N))
    else
      FCmdParams.Add(UpperCase(S), '');
  end;

  if IsWin7 then
  begin
    TBIntf := CreateComObject(CLSID_TaskbarList) as ITaskbarList;
    if TBIntf.QueryInterface(ITaskbarList4, FTaskBarList) <> 0 then
    begin
      TBIntf := nil;
      Integer(FTaskBarList) := 0;
    end else
      FTaskBarList.HrInit;
  end else
    FTaskBarList := nil;
end;

procedure TwndMain.FormDestroy(Sender: TObject);
begin
  FCmdParams.Free;
  if Assigned(FExec) then
    FreeAndNil(FExec);
  if Assigned(FRemoteExec) then
    FreeAndNil(FRemoteExec);
  FActionNode.Free;
  if Assigned(FDlg) then
    FDlg.Free;
end;

procedure TwndMain.FormShow(Sender: TObject);
begin
  if FCmdParams.ContainsKey('CONFIG') then
    LoadConfig(FCmdParams.Items['CONFIG'])
  else
    LoadConfig('');
end;

procedure TwndMain.UpdateTaskBarProgress(APosition, ATotal: Integer);
var
  hWND: THandle;
begin
  if IsWin7 and Assigned(FTaskBarList) then
  begin

    if Application.MainFormOnTaskBar then
      hWND := Handle
    else
      hWND := Application.Handle;

    if APosition = -1 then
      FTaskBarList.SetProgressState(hWND, TBPF_NOPROGRESS)
    else
      FTaskBarList.SetProgressValue(hWND, aPosition, ATotal);
  end;
end;

procedure TwndMain.LoadConfig(const AFile: string);
var
  FN: string;
begin
  FN := AFile;
  if FN = '' then
    FN := ChangeFileExt(Application.ExeName, '.xml');

  FExec.Load(FN);
  PopulateActions<TabLocalUIAction>(FExec.Actions.ToArray);
end;

procedure TwndMain.OnStateChangeEvent<T>(Action: T);
const
  c_SuccessImg = 1;
  c_ErrorImg = 2;
  c_InProcessImg = 3;
  c_SkipImg = 4;
  c_WaitImg = 5;
var
  ND: TcxTreeListNode;
begin
  if not FActionNode.TryGetValue(Action, ND) then Exit;

  case Action.State of
    csWait: ND.Values[4] := c_WaitImg;
    csInProcess:  ND.Values[4] := c_InProcessImg;
    csSkipped:  ND.Values[4] := c_SkipImg;
    csSuccess:  ND.Values[4] := c_SuccessImg;
    csError:  ND.Values[4] := c_ErrorImg;
  end;
end;

procedure TwndMain.PopulateActions<T>(Arr: TArray<TPair<string, T>>);
var
  I: Integer;
  FN: string;
  cxProjND: TcxTreeListNode;
  V: T;
begin
  TArray.Sort<TPair<string, T>>(Arr, TCommandComparer<T>.Create);

  tlCommands.BeginUpdate;
  try
    tlCommands.Clear;
    FActionNode.Clear;

    for I := Low(Arr) to High(Arr) do
    begin
      V := Arr[i].Value;
      cxProjND := tlCommands.Add;
      cxProjND.Values[0] := V.State <> csSkipped;
      cxProjND.Values[1] := V.NAME ;
      cxProjND.Data := TabBaseAction(V);

      FActionNode.Add(V, cxProjND);
    end;
  finally
    tlCommands.EndUpdate;
  end;
end;

procedure TwndMain.SaveConfig(const AFile: string);
var
  i: Integer;
  ND: TcxTreeListNode;
  FN: string;
begin
  FN := AFile;
  if FN = '' then
    FN := ChangeFileExt(Application.ExeName, '.xml');

  for I := 0 to tlCommands.Count - 1 do
  begin
    ND := tlCommands.Items[i];

    if not (ND.Values[0]) then
      TabBaseAction(ND.Data).State := csSkipped
    else
      TabBaseAction(ND.Data).State := csWait;
  end;{for i}

  FExec.Save(FN);
end;

procedure TwndMain.tlCommandsCollapsing(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode; var Allow: Boolean);
begin
  Allow := False;
end;

procedure TwndMain.tlCommandsFocusedNodeChanged(Sender: TcxCustomTreeList;
  APrevFocusedNode, AFocusedNode: TcxTreeListNode);
begin
  if Assigned(APrevFocusedNode) and Assigned(APrevFocusedNode.Data) then
    TabBaseAction(APrevFocusedNode.Data).StdOut.OnChange := nil;

  if Assigned(AFocusedNode) and Assigned(AFocusedNode.Data) then
  begin
    memOut.Clear;
    memOut.Lines.AddStrings(TabBaseAction(AFocusedNode.Data).StdOut);
    TabBaseAction(AFocusedNode.Data).StdOut.OnChange := SL_Change;
  end;
end;


function TCommandComparer<TValue>.Compare(const Left,
  Right: TPair<string, TValue>): Integer;
var
  i, L, L1, L2: Integer;
  Ch1, Ch2: Char;
begin
  L1 := Length(Left.Key);
  L2 := Length(Right.Key);

  L := Min(L1, L2);
  I := 1;
  Result := 0;

  while I <= L do
  begin
    Ch1 := UpperCase(Left.Key[i])[1];
    Ch2 := UpperCase(Right.Key[i])[1];
    if Ch1 <> Ch2 then
    begin
      Result := IfThen(Ch1 > Ch2, 1, -1);
      Break;
    end;
    Inc(I);
  end;

  if (Result = 0) and (L1 <> L2) then
    Result := IfThen(L1 > L2, 1, -1);
//  Result := CompareText(Left.Key, Right.Key);
end;

end.
