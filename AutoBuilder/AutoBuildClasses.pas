unit AutoBuildClasses;

interface

uses
  Winapi.Windows, System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, System.Variants,
  Vcl.Dialogs, System.Generics.Collections, System.Generics.Defaults, Vcl.Forms,
  cxTL, cxPC,
  janXMLParser2;

type
  IabTaskBarProgress = interface
    ['{DAC42E2F-AE39-45DC-B7BE-1E9BBDB94853}']
    procedure UpdateTaskBarProgress(APosition, ATotal: Integer);
  end;

  TCommandState = (csWait, csInProcess, csSkipped, csSuccess, csError);

  TabBaseAction = class
  private
    type
      TOnStateChange = procedure(Sender: TabBaseAction) of object;
  private
    FCode: string;
    FState: TCommandState;
    FName: string;
    FOnStateChange: TOnStateChange;
    FStdOut: TStringList;
   procedure SetState(const Value: TCommandState);
  protected
    procedure Load(const ANode: TjanXMLNode2); virtual;
    procedure Save(const ANode: TjanXMLNode2); virtual;
    property OnStateChange: TOnStateChange read FOnStateChange write FOnStateChange;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    property Code: string read FCode write FCode;
    property Name: string read FName write FName;
    property State: TCommandState read FState write SetState;
    property StdOut: TStringList read FStdOut;
  end;

  TabCustomLocalAction = class(TabBaseAction)
  private
    FDepends: TObjectDictionary<string, TabCustomLocalAction>;
    FCommandText: string;
  protected
    procedure Load(const ANode: TjanXMLNode2); override;
    procedure Save(const ANode: TjanXMLNode2); override;
  public
    constructor Create; override;
    destructor Destroy; override;

    property CommandText: string read FCommandText write FCommandText;
    property Depends: TObjectDictionary<string, TabCustomLocalAction> read FDepends write FDepends;
  end;

  TabLocalUIAction = class(TabCustomLocalAction)
  protected
    procedure Save(const ANode: TjanXMLNode2); override;
  end;

  TabLocalServiceAction = class(TabCustomLocalAction)
  protected
    procedure Save(const ANode: TjanXMLNode2); override;
  end;

  TabRemoteAction = class(TabBaseAction)
  protected
    procedure Load(const ANode: TjanXMLNode2); override;
  end;

  TCustomActionsExecutor<T: TabBaseAction, constructor> = class
  private
    FActions: TObjectDictionary<string, T>;
    FDlg: TTaskDialog;
    procedure DoOnStateChange(Sender: TabBaseAction);
    procedure SetDlg(const Value: TTaskDialog);
  protected
    procedure OnTimer(Sender: TObject; TickCount: Cardinal; var Reset: Boolean); virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure Load(const AXML: TjanXMLParser2); overload; virtual;
    procedure Load(const AStream: TStream); overload; virtual;

    procedure Save(const AXML: TjanXMLParser2); overload; virtual;
    procedure Save(const AStream: TStream); overload; virtual;

    procedure Execute; virtual; abstract;

    property Actions: TObjectDictionary<string, T> read FActions;
    property Dialog: TTaskDialog read FDlg write SetDlg;
  public
    type
      TOnActionStateChange = procedure(Action: T) of object;
  private
    FOnStateChange: TOnActionStateChange;
  public
    property OnStateChange: TOnActionStateChange read FOnStateChange write FOnStateChange;
  end;

  TLocalActionsExecutor<T: TabCustomLocalAction, constructor> = class(TCustomActionsExecutor<T>)
  private
    type
      TActionExecuteThread = class(TThread)
      private
        FData: T;
        FSyncStr: TStrings;
        FNewState: TCommandState;
        function RunAndStdOutToStrings(const CommandLine: string;
          Str: TStrings): Boolean;

        function WaitDepends(const AData: T): Boolean;
      protected
        procedure Execute; override;
        procedure SyncMemoUpdate;
        procedure SyncUpdateState;
      public
        constructor Create(const AData: T);
      end;
  private
    FThreadList: TList;
    FStartTime: Cardinal;
    procedure OnThreadTerminateEvent(Sender: TObject);
  protected
    procedure OnTimer(Sender: TObject; TickCount: Cardinal; var Reset: Boolean); override;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Load(const AXML: TjanXMLParser2); overload; override;
    procedure Load(const AFile: string); overload;
    procedure Save(const AFile: string); overload;
    procedure Execute; override;
  end;

  TRemoteActionsExecutor<T: TabBaseAction, constructor> = class(TCustomActionsExecutor<T>)
  public
    procedure Execute; override;
  end;

implementation

uses
  ShellApi, DateUtils, Soap.EncdDecd;

{ TBaseprojectCommand }

constructor TabBaseAction.Create;
begin
  inherited;
  FCode := '';
  FName := '';
  FState := csWait;
  FStdOut := TStringList.Create;
end;

destructor TabBaseAction.Destroy;
begin
  FStdOut.OnChange := nil;
  FStdOut.Free;
  inherited;
end;

procedure TabBaseAction.Load(const ANode: TjanXMLNode2);
begin
  Code := ANode.GetAttr('CODE', '');
  NAME := ANode.GetAttr('NAME', '<Undefined>');
  State := TCommandState(ANode.GetIntAttr('STATE', Integer(csWait)));
end;

procedure TabBaseAction.Save(const ANode: TjanXMLNode2);
begin
  ANode.attribute['CODE'] := Code;
  ANode.attribute['NAME'] := NAME;
  ANode.attribute['STATE'] := Integer(State);
end;

procedure TabBaseAction.SetState(const Value: TCommandState);
begin
  FState := Value;
  if Assigned(FOnStateChange) then
    FOnStateChange(Self);
end;

{ TabCustomLocalAction }

constructor TabCustomLocalAction.Create;
begin
  inherited;
  FDepends := TObjectDictionary<string, TabCustomLocalAction>.Create([]);
end;

destructor TabCustomLocalAction.Destroy;
begin
  FDepends.Free;
  inherited;
end;

procedure TabCustomLocalAction.Load(const ANode: TjanXMLNode2);
begin
  inherited Load(ANode);
  CommandText := ANode.GetAttr('CMD_TEXT', '');
end;

procedure TabCustomLocalAction.Save(const ANode: TjanXMLNode2);
begin
  inherited Save(ANode);

  ANode.attribute['CMD_TEXT'] := CommandText;
end;

{ TabLocalUIAction }

procedure TabLocalUIAction.Save(const ANode: TjanXMLNode2);
begin
  inherited Save(ANode);

  with Depends.GetEnumerator do
    try
      while MoveNext do
        ANode.addChildByName('DEPEND').attribute['CODE'] := Current.Key;
    finally
      Free;
    end;
end;

{ TabLocalServiceAction }

procedure TabLocalServiceAction.Save(const ANode: TjanXMLNode2);
begin
  inherited Save(ANode);

  if Assigned(StdOut) then
    ANode.attribute['LOG_TEXT'] := StdOut.Text;
end;

{ TabRemoteAction }

procedure TabRemoteAction.Load(const ANode: TjanXMLNode2);
begin
  inherited Load(ANode);

  if Assigned(StdOut) and ANode.hasAttribute('LOG_TEXT') then
    StdOut.Text := ANode.GetAttr('LOG_TEXT');
end;

{ TCustomActionsExecutor<T> }

constructor TCustomActionsExecutor<T>.Create;
begin
  FActions := TObjectDictionary<string, T>.Create([doOwnsValues]);
end;

destructor TCustomActionsExecutor<T>.Destroy;
begin
  FActions.Free;

  inherited;
end;

procedure TCustomActionsExecutor<T>.DoOnStateChange(Sender: TabBaseAction);
begin
  if Assigned(FOnStateChange) then
    FOnStateChange(Sender);
end;

procedure TCustomActionsExecutor<T>.SetDlg(const Value: TTaskDialog);
begin
  FDlg := Value;
  if Assigned(FDlg) then
  begin
    FDlg.Caption := 'Actions progress';
    FDlg.OnTimer := OnTimer;
    FDlg.Flags := [tfShowProgressBar, tfCallbackTimer, tfPositionRelativeToWindow];
    with FDlg.Buttons.Add do
    begin
      Caption := 'Close';
      ModalResult := mrClose;
    end;
  end;
end;

procedure TCustomActionsExecutor<T>.Load(const AXML: TjanXMLParser2);
var
  ND: TjanXMLNode2;
  Data: T;
  I: Integer;
begin
  FActions.Clear;

  for I := 0 to AXML.childCount - 1 do
  begin
    ND := AXML.childNode[i];

    if ND.name_s <> 'ACTION' then
      Continue;

    Data := T.Create;
    Data.OnStateChange := DoOnStateChange;
    Data.Load(ND);
    FActions.Add(Data.Code, Data);
  end;{for i}
end;

procedure TCustomActionsExecutor<T>.Load(const AStream: TStream);
var
  P: TjanXMLParser2;
begin
  P := TjanXMLParser2.Create;
  try
    P.LoadXMLFromStream(AStream);
    Load(P);
  finally
    P.Free;
  end;
end;

procedure TCustomActionsExecutor<T>.OnTimer(Sender: TObject;
  TickCount: Cardinal; var Reset: Boolean);
begin
//
end;

procedure TCustomActionsExecutor<T>.Save(const AXML: TjanXMLParser2);
begin
  with FActions.GetEnumerator do
    try
      while MoveNext do
        Current.Value.Save(AXML.addChildByName('ACTION'));
    finally
      Free;
    end;

end;

procedure TCustomActionsExecutor<T>.Save(const AStream: TStream);
var
  P: TjanXMLParser2;
begin
  P := TjanXMLParser2.Create;
  try
    Save(P);
    P.SaveXMLToStream(AStream);
  finally
    P.Free;
  end;
end;

{ TLocalActionsExecutor }

constructor TLocalActionsExecutor<T>.Create;
begin
  inherited Create;
  FThreadList := TList.Create;
  FDlg := nil;
end;

destructor TLocalActionsExecutor<T>.Destroy;
begin
  FThreadList.Free;

  inherited;
end;

procedure TLocalActionsExecutor<T>.Execute;
var
  i: Integer;
  Thr: TActionExecuteThread;
  En: TObjectDictionary<string, T>.TValueEnumerator;
begin
  FStartTime := GetTickCount;
  En := FActions.Values.GetEnumerator;
  try
    while En.MoveNext do
    begin
      En.Current.StdOut.Clear;

      if En.Current.State = csSkipped then Continue;

      Thr := TActionExecuteThread.Create(En.Current);
      Thr.OnTerminate := OnThreadTerminateEvent;
      FThreadList.Add(Thr);
    end;
  finally
    En.Free;
  end;

  if Assigned(FDlg) then
  begin
    FDlg.ProgressBar.Max := FThreadList.Count;
    FDlg.CommonButtons := [];
    FDlg.Buttons[0].Enabled := False;
    FDlg.Text := 'Starting...';
  end;

  for i := 0 to FThreadList.Count - 1 do
    TActionExecuteThread(FThreadList[i]).Suspended := False;

  if Assigned(FDlg) then
    FDlg.Execute;
end;


procedure TLocalActionsExecutor<T>.Load(const AXML: TjanXMLParser2);
var
  ND, Chld: TjanXMLNode2;
  I, j: Integer;
  Code, RefCode: string;
  Cmd, PrevCmd: T;
begin
  inherited Load(AXML);


  for I := 0 to AXML.childCount - 1 do
  begin
    ND := AXML.childNode[i];

    if ND.name_s <> 'ACTION' then
      Continue;

    Code := ND.GetAttr('CODE', '');
    if Code = '' then Exit;

    Cmd := Actions.Items[Code];
    if not Assigned(Cmd) then Exit;

    for j := 0 to ND.childCount - 1 do
    begin
      Chld := ND.childNode[j];
      if Chld.name <> 'DEPEND' then Continue;

      RefCode := Chld.GetAttr('CODE');
      if RefCode = '' then Continue;

      PrevCmd := Actions.Items[RefCode];
      if not Assigned(PrevCmd) then Continue;

      Cmd.Depends.Add(PrevCmd.Code, PRevCmd);
    end;{for i}
  end;
end;

procedure TLocalActionsExecutor<T>.Load(const AFile: string);
var
  F: TFileStream;
begin
  F := TFileStream.Create(AFile, fmOpenRead);
  try
    inherited Load(F);
  finally
    F.Free;
  end;
end;

procedure TLocalActionsExecutor<T>.Save(const AFile: string);
var
  F: TFileStream;
begin
  F := TFileStream.Create(AFile, fmCreate);
  try
    inherited Save(F);
  finally
    F.Free;
  end;
end;

procedure TLocalActionsExecutor<T>.OnThreadTerminateEvent(Sender: TObject);
var
  N: Integer;
begin
  N := FThreadList.IndexOf(Sender);
  if N >= 0 then
    FThreadList.Delete(N);
end;

procedure TLocalActionsExecutor<T>.OnTimer(Sender: TObject; TickCount: Cardinal;
  var Reset: Boolean);
var
  I: IabTaskBarProgress;
begin
  if not Assigned(FDlg) then Exit;

  FDlg.ProgressBar.Position := FDlg.ProgressBar.Max - FThreadList.Count;

  if Application.MainForm.GetInterface(IabTaskBarProgress, I) then
  begin
    if FDlg.ProgressBar.Position = FDlg.ProgressBar.Max then
      I.UpdateTaskBarProgress(-1, -1)
    else
      I.UpdateTaskBarProgress(FDlg.ProgressBar.Position, FDlg.ProgressBar.Max);
  end;
  if not FDlg.Buttons[0].Enabled then
    FDlg.Text := 'Elapsed time: ' + TimeToStr((GetTickCount - FStartTime) / MSecsPerDay);

  if (FThreadList.Count = 0) and (FDlg.CommonButtons = []) and (not FDlg.Buttons[0].Enabled) then
  begin
    FDlg.Buttons[0].Enabled := True;
    FDlg.Text := FDlg.Text + '. Done: ' + TimeToStr(Time);
  end;
  Application.ProcessMessages;
end;

{ TActionExecuteThread }

constructor TLocalActionsExecutor<T>.TActionExecuteThread.Create(const AData: T);
begin
  inherited Create(True);
  FreeOnTerminate := True;
  FData := AData;
end;

function TLocalActionsExecutor<T>.TActionExecuteThread.WaitDepends(
  const AData: T): Boolean;
var
  En: TObjectDictionary<string, TabCustomLocalAction>.TPairEnumerator;
  B: Boolean;
begin
  Result := False;
  if AData.Depends.Count > 0 then
  begin
    B := True;
    repeat
      En := AData.Depends.GetEnumerator;
      try
        while En.MoveNext do
        begin
          if En.Current.Value.State = csError then
          begin
            FNewState := csError;
            Synchronize(SyncUpdateState);
            Exit;
          end;

          if En.Current.Value.State = csSkipped then
          begin
            Result := WaitDepends(En.Current.Value);
            if not Result then Exit;

            B := True;
          end else
            B := En.Current.Value.State = csSuccess;

          if not B then
            Break;
        end;
      finally
        En.Free;
      end;

      if not B then
        Sleep(250);
    until B;
  end;
  Result := True;
end;

procedure TLocalActionsExecutor<T>.TActionExecuteThread.Execute;
var
  TmpPath, TmpCmd: string;
begin
  FSyncStr := nil;
  try
    Synchronize(SyncMemoUpdate);

    if not WaitDepends(FData) then
    begin
      FNewState := csSkipped;
      Synchronize(SyncUpdateState);
      Exit;
    end;

    FNewState := csInProcess;
    Synchronize(SyncupdateState);

    if FData.CommandText <> '' then
    begin
      SetLength(TmpPath, MAX_PATH);
      SetLength(TmpCmd, MAX_PATH);

      GetTempPath(MAX_PATH, PChar(TmpPath));
      TmpPath := PChar(TmpPath);
      GetTempFileName(PChar(TmpPath), 'arb', 0, PChar(TmpCmd));
      TmpCmd := PChar(TmpCmd);
      DeleteFile(TmpCmd);
      TmpCmd := ChangeFileExt(TmpCmd, '.cmd');


      with TStringList.Create do
        try
          Text := FData.CommandText;
          SaveToFile(TmpCmd);
        finally
          Free;
        end;

      try
        if RunAndStdOutToStrings(TmpCmd, FData.StdOut) then
          FNewState := csSuccess
        else
          FNewState := csError;
        Synchronize(SyncUpdateState);
      finally
        if FileExists(TmpCmd) then
          DeleteFile(TmpCmd);
      end;
    end else begin
      FNewState := csSuccess;
      Synchronize(SyncUpdateState);
    end;
  finally
    FSyncStr := nil;
  end;
end;

function TLocalActionsExecutor<T>.TActionExecuteThread.RunAndStdOutToStrings(const CommandLine: string; Str: TStrings): Boolean;
var
  tRead, cWrite: NativeUInt;
  SA: TSecurityAttributes;
  PI: TProcessInformation;
  SI: TStartupInfo;
  sBuff: THandleStream;
  StringBuf: TStringList;
  ret: cardinal;
  m: TMemoryStream;
  fla: boolean;
  SCmd, SLastStr: string;
  ECode: cardinal;
begin
  Result := False;
  // �������������
  SA.nLength := SizeOf(SECURITY_ATTRIBUTES);
  SA.bInheritHandle := True;
  SA.lpSecurityDescriptor := nil;
  if not CreatePipe(tRead, cWrite, @SA, 0) then
    Exit;
  try
    ZeroMemory(@SI, SizeOf(TStartupInfo));
    SI.cb := SizeOf(TStartupInfo);
    SI.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
    SI.wShowWindow := SW_HIDE;
    SI.hStdOutput := cWrite;
    // �������� �������...

    SCmd := CommandLine;
    UniqueString(SCmd);
    if not CreateProcess(nil, PChar(SCmd), nil, nil, True, 0, nil, nil, SI, PI) then
      Exit;

    try
      sBuff := THandleStream.Create(tRead);
      StringBuf := TStringList.Create();
      m := TMemoryStream.Create;
      try
        SLastStr := '';
        FSyncStr := StringBuf;
        repeat
          // ���� N-����� �����
          ret := WaitForSingleObject(PI.hProcess, 100);
          StringBuf.Clear();
          if sBuff.Size > 0 then
          begin
            fla := (m.Size > 0) and not(PByteArray(m.Memory)^[m.Size - 1] in [13,
              10]);
            m.Size := 0;
            m.LoadFromStream(sBuff);
            m.Position := 0;
            StringBuf.LoadFromStream(m); // �������� ���� � �����
            if StringBuf.Count > 0 then
            begin
//              if (SLastStr <> '') and fla then
//                StringBuf.Strings[0] := SLastStr + StringBuf.Strings[0];
              if (SLastStr <> '') then
              begin
                if fla then
                  StringBuf.Strings[0] := SLastStr + StringBuf.Strings[0]
                else
                  StringBuf.Insert(0, SLastStr);
              end;

              SLastStr := StringBuf.Strings[StringBuf.Count - 1];
              StringBuf.Delete(StringBuf.Count - 1);
            end;
            // ��������� ���� �� ������
            Synchronize(SyncMemoUpdate);
//              Str.AddStrings(StringBuf);
          end;
          // �� ���� �� pipe ?
          // PeekNamedPipe(tRead, nil, 0, nil, @dwAvail, nil);
        until (ret <> WAIT_TIMEOUT);

        if SLastStr <> '' then
        begin
          StringBuf.Clear;
          StringBuf.Add(SLastStr);
          Synchronize(SyncMemoUpdate);
        end;

        GetExitCodeProcess(pi.hProcess, ECode);
        Result := ECode = 0;
      finally
        m.Free;
        StringBuf.Free;
        sBuff.Free;
      end;
    finally
      CloseHandle(PI.hProcess);
      CloseHandle(PI.hThread);
    end; // if CreateProcess
  finally
    CloseHandle(tRead);
    CloseHandle(cWrite);
  end;
end;

procedure TLocalActionsExecutor<T>.TActionExecuteThread.SyncMemoUpdate;
begin
  if not Assigned(FData.StdOut) then Exit;

  if not Assigned(FSyncStr) then
    FData.StdOut.Clear
  else begin
    FData.StdOut.AddStrings(FSyncStr);
  end;
end;

procedure TLocalActionsExecutor<T>.TActionExecuteThread.SyncUpdateState;
begin
  FData.State := FNewState;
end;

{ TRemoteActionsExecutor<T> }

procedure TRemoteActionsExecutor<T>.Execute;
begin

end;


end.
