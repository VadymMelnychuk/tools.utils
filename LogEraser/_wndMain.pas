unit _wndMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, cxCustomData, cxStyles,
  cxTL, cxCheckBox, cxTextEdit, cxTLdxBarBuiltInMenu, ActnList,
  cxInplaceContainer, StdCtrls, cxButtons, cxMaskEdit, cxButtonEdit, ExtCtrls,
  dxSkinsdxStatusBarPainter, cxProgressBar, dxStatusBar, cxSplitter;

type
  TwndMain = class(TForm)
    LookAndFeelController: TcxLookAndFeelController;
    Panel1: TPanel;
    Label1: TLabel;
    edBasePath: TcxButtonEdit;
    Panel2: TPanel;
    btnScan: TcxButton;
    Panel3: TPanel;
    Panel4: TPanel;
    Label2: TLabel;
    tlFiles: TcxTreeList;
    clnFiles: TcxTreeListColumn;
    clnFileName: TcxTreeListColumn;
    Panel5: TPanel;
    Label3: TLabel;
    tlExt: TcxTreeList;
    clnExt: TcxTreeListColumn;
    clnExtName: TcxTreeListColumn;
    btnErase: TcxButton;
    ActionList: TActionList;
    actScan: TAction;
    actErase: TAction;
    dxStatusBar1: TdxStatusBar;
    dxStatusBar1Container0: TdxStatusBarContainerControl;
    pbProgress: TcxProgressBar;
    PopupMenu: TPopupMenu;
    miCheckAll: TMenuItem;
    miUncheckAll: TMenuItem;
    miInvert: TMenuItem;
    cxSplitter1: TcxSplitter;
    procedure actScanExecute(Sender: TObject);
    procedure actScanUpdate(Sender: TObject);
    procedure actEraseExecute(Sender: TObject);
    procedure actEraseUpdate(Sender: TObject);
    procedure edBasePathPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure clnExtPropertiesChange(Sender: TObject);
    procedure miCheckAllClick(Sender: TObject);
    procedure miInvertClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  wndMain: TwndMain;

implementation

{$R *.dfm}

uses
   ActiveX, ShlObj, ShellApi, Math;

//------------------------------------------------------------------------------

const
//  faNormal       = $00000080; // FILE_ATTRIBUTE_NORMAL              0x00000080
//  faTemporary    = $00000100; // FILE_ATTRIBUTE_TEMPORARY           0x00000100
//  faSparse       = $00000200; // FILE_ATTRIBUTE_SPARSE_FILE         0x00000200
//  faReparsePoint = $00000400; // FILE_ATTRIBUTE_REPARSE_POINT       0x00000400
//  faCompressed   = $00000800; // FILE_ATTRIBUTE_COMPRESSED          0x00000800
//  faOffline      = $00001000; // FILE_ATTRIBUTE_OFFLINE             0x00001000
//  faNotIndexed   = $00002000; // FILE_ATTRIBUTE_NOT_CONTENT_INDEXED 0x00002000
//  faEncrypted    = $00004000; // FILE_ATTRIBUTE_ENCRYPTED           0x00004000

  faAnyFiles     = faAnyFile or faNormal;
  faOnlyFiles    = faAnyFiles and not (faDirectory or faVolumeID);

var
  _Caption: string;
  _FSelectedFolder: string;

function _BrowseFolderDlgCallBack(Wnd: HWND; Msg: UINT; Param,
  Data: LPARAM):Integer; stdcall;
var
	TempPath: array[0..MAX_PATH] of Char;
	SHFolder: IShellFolder;
  Display: TStrRet;
begin
  Result := 0;

  case Msg of

    BFFM_INITIALIZED:
      begin
      // Change caption and status text if specified
        if _Caption <> '' then SetWindowText(Wnd, PChar(_Caption));
      end;

    BFFM_SELCHANGED:
      begin

      // Update Item property
        SHGetPathFromIDList(PItemIDList(Param), TempPath);
        _FSelectedFolder := StrPas(TempPath);

      // Update staus text, set current path if path should be shown
        //Path := abfPackPathString(FSelectedFolder, cMaxPathDisplayLength);
        if _FSelectedFolder = '' then
          if SHGetDeskTopFolder(SHFolder) = NOERROR then
          begin
            SHFolder.GetDisplayNameOf(PItemIDList(Param), SHGDN_FORPARSING,
              Display);
            if Display.uType = STRRET_CSTR then _FSelectedFolder := StrPas(Display.cStr);
          end;
      end;{case of BFFM_SELCHANGED}
  end;{case Msg of}
end;{function _BrowseFolderDlgCallBack}

//------------------------------------------------------------------------------

function BrowseForFolder(const AText, ACaption: string; AOwner: THandle;
  var APath, ADisplayName: string): Boolean;
var
  iList: PItemIDList;
  BrowseInfo: TBrowseInfo;
	TempPath: array[0..MAX_PATH] of Char;
 // FDisplayName: string;

  //-------------------------------------

  procedure _InitBrowseInfo;
  var
    S: string;
  begin
    S := AText;
    FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
    with BrowseInfo do
    begin
      pszDisplayName := PChar(ADisplayName);
      lpszTitle := PChar(S);

      ulFlags := {BIF_NEWDIALOGSTYLE or} BIF_USENEWUI or BIF_NONEWFOLDERBUTTON or BIF_RETURNONLYFSDIRS or
        BIF_STATUSTEXT or BIF_SHAREABLE;
      lpfn := @_BrowseFolderDlgCallBack;
      //lParam := Integer(Self);
      hwndOwner := AOwner;
      SHGetSpecialFolderLocation(0, CSIDL_DRIVES, pidlRoot);
    end;
  end;{Internal procedure _InitBrowseInfo}

  //-------------------------------------
var
//  FSelectedFolder: string;
  FRestart: Boolean;
begin
  Result := False;

  SetLength(aDisplayName, MAX_PATH);
	iList := nil;
	try
    _InitBrowseInfo;

  // Start dialog "loop"
		_FSelectedFolder := APath;
		repeat
			FRestart := False;
			APath := _FSelectedFolder;
      try
     	  iList := SHBrowseForFolder(BrowseInfo);
      finally
       // DestroyCreateButton; // Destroy a "Create button"
//        ResetHandles; // Reset all handles to prevent wrong window catching
      end;
		until not FRestart;

  // Analyse the result
    if iList <> nil then
    begin
      Result := True;
      SHGetPathFromIDList(iList, TempPath);
      APath := StrPas(TempPath);
      _FSelectedFolder := APath;
//			FImageIndex := BrowseInfo.iImage;
    end;

	finally
    ADisplayName := PChar(ADisplayName);

    //ResetPosition; // Drop saved position
		CoTaskMemFree(iList);
		CoTaskMemFree(BrowseInfo.pidlRoot);
	end;
end;


procedure TwndMain.actEraseExecute(Sender: TObject);
const
  cBlock = 4096;
var
  SPath, SMsg: string;
  S: array[0..cBlock - 1] of Byte;
  ND: TcxTreeListNode;
  K, I: Integer;
  FS: TFileStream;
  SLErr: TStringList;
begin
  if MessageDlg('All selected file will be cleared. Continue?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Exit;

  FillChar(S, cBlock, 0);

  pbProgress.Properties.Max := tlFiles.Count;
  Screen.Cursor := crHourGlass;
  SLErr := TStringList.Create;
  try
    for I := 0 to tlFiles.Count - 1 do
    begin
      pbProgress.Position := i;
      ND := tlFiles.Items[i];
      if not Boolean(ND.Values[0]) then
        Continue;

      SPath := IncludeTrailingBackslash(edBasePath.Text) + ND.Values[1];

      try
        FS := TFileStream.Create(SPath, fmOpenReadWrite);
        try
          K := 0;
          repeat
            FS.Write(S[0], Min(cBlock, FS.Size - (K * cBlock)));
            Inc(K);
          until ((K * cBlock) > FS.Size) or (FS.Size <= cBlock);
        finally
          FS.Free;
        end;
      except
        SLErr.Add(SPath);

      end;

    end;
  finally
    SMsg := 'Operation complette.';
    SPath := ExtractFilePath(Application.ExeName) + 'Error_erase_' + FormatDateTime('yyyy.mm.dd hh-nn-ss', Now) + '.log';
    if SLErr.Count > 0 then
    begin
      SLErr.SaveToFile(SPath);
      SMsg := SMsg + #13#10 + Format('Cannot errase %d files', [SLErr.Count]) + #13#10 +
        Format('Log file saved as "%s"', [SPath]);
    end;

    SLErr.Free;
    Screen.Cursor := crDefault;
    pbProgress.Position := 0;
    MessageDlg(SMsg, mtInformation, [mbOK], 0);
  end;
end;

procedure TwndMain.actEraseUpdate(Sender: TObject);
begin
  TAction(SendeR).Enabled := tlFiles.Count > 0;
end;

procedure TwndMain.actScanExecute(Sender: TObject);

  procedure _Recurs(const APath: string);
  var
    SR: TSearchRec;
    SRel: string;
    R: Integer;
    ND: TcxTreeListNode;
  begin
    SRel := APath;
    Delete(SRel, 1, Length(edBasePath.Text));
    R := FindFirst(APath + '*.*', faOnlyFiles, SR);
    if R = 0 then
    begin
      try
        repeat
          if (SR.Name <> '.') and (SR.Name <> '..') then
          begin
            ND := tlFiles.Add;
            ND.Values[0] := True;
            ND.Values[1] := SRel + SR.Name;

            if not Assigned(tlExt.FindNodeByText(ExtractFileExt(SR.Name), clnExtName)) then
            begin
              ND := tlExt.Add;
              ND.Values[0] := True;
              ND.Values[1] := ExtractFileExt(SR.Name);
            end;
          end;
          R := FindNext(SR);
        until R <> 0;
      finally
        FindClose(SR);
      end;
    end;

    R := FindFirst(APath + '*.*', faDirectory, SR);
    if R <> 0 then Exit;

    try
      repeat
        if (SR.Name <> '.') and (SR.Name <> '..') then
          _Recurs(APath + SR.Name + '\');
        R := FindNext(SR);
      until R <> 0;
    finally
      FindClose(SR);
    end;
  end;

begin
  tlFiles.BeginUpdate;
  tlExt.BeginUpdate;
  Screen.Cursor := crHourGlass;
  try
    _Recurs(IncludeTrailingBackslash(edBasePath.Text));
  finally
    tlFiles.EndUpdate;
    tlExt.EndUpdate;
    Screen.Cursor := crDefault;
  end;
end;

procedure TwndMain.actScanUpdate(Sender: TObject);
begin
  TAction(SendeR).Enabled := edBasePath.Text <> '';
end;

procedure TwndMain.clnExtPropertiesChange(Sender: TObject);
var
  i: Integer;
  ExtND, ND: TcxTreeListNode;
  E, S: string;
  B: Boolean;
begin
  if not Assigned(tlExt.FocusedNode) then
  begin
    MessageDlg('Error get extension node', mtWarning, [mbOK], 0);
    Exit;
  end;

  B := tlExt.FocusedNode.Values[0];
  E := tlExt.FocusedNode.Values[1];

  tlExt.BeginUpdate;
  Screen.Cursor := crHourGlass;
  try
    for I := 0 to tlFiles.Count - 1 do
    begin
      ND := tlFiles.Items[i];
      S := ND.Values[1];
      if AnsiSameText(ExtractFileExt(S), E) then
        ND.Values[0] := B;
    end;{for i}
  finally
    tlExt.EndUpdate;
    Screen.Cursor := crDefault;
  end;

end;

procedure TwndMain.edBasePathPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  S, DN: string;
begin
  S := edBasePath.Text;
  if not BrowseForFolder('Select root folder', '', Handle, S, DN) then
    Exit;

  edBasePath.Text := S;
end;

procedure TwndMain.miCheckAllClick(Sender: TObject);
var
  i: Integer;
  tl: TcxTreeList;
begin
  if tlFiles.Focused then
    tl := tlFiles
  else
    tl := tlExt;

  tl.BeginUpdate;
  Screen.Cursor := crHourGlass;
  try
    for I := 0 to tl.Count - 1  do
      tl.Items[i].Values[0] := (TComponent(Sender).Tag = 0);
  finally
    tl.EndUpdate;
    Screen.Cursor := crDefault;
  end;

end;

procedure TwndMain.miInvertClick(Sender: TObject);
var
  i: Integer;
  tl: TcxTreeList;
begin
  if tlFiles.Focused then
    tl := tlFiles
  else
    tl := tlExt;

  tl.BeginUpdate;
  Screen.Cursor := crHourGlass;
  try
    for I := 0 to tl.Count - 1  do
      tl.Items[i].Values[0] := not Boolean(tl.Items[i].Values[0]);
  finally
    tl.EndUpdate;
    Screen.Cursor := crDefault;
  end;
end;

end.
