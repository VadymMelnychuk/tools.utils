unit _srvMain;
{.$DEFINE DebugService}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  MemoryIniFile;

type
  THelperThread = class(TThread)
  private
{$IFDEF DebugService}
    FDebugServiceFile: string;
    procedure WriteLog(const ALogTxt: string);
{$ENDIF DebugService}
  protected
//    Lst: TStringList;
    Ini: TMemoryIniFile;
    SleepTime: Integer;
    procedure Execute; override;
    procedure GetShares(const AList: TStringList);
  public
    constructor Create;
    destructor Destroy; override;

  end;

  TsrvDemon = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
  private
    FThread: THelperThread;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

  TWlStringList = class(TStringList)
  protected
    function CompareStrings(const S1, S2: string): Integer; override;
  end;

var
  srvDemon: TsrvDemon;

implementation

{$R *.DFM}

uses
  ShlObj, ShellApi, LM;

const
  S_Section_Settings = '*DemmonSrv*Settings*DemmonSrv*';
  S_Section_RemoveShares = '*DemmonSrv*RemoveShares*DemmonSrv*';
  S_Section_RemoveUsers = '*DemmonSrv*RemoveUsers*DemmonSrv*';
  S_Section_RemoveAllUsersExceptThis = '*DemmonSrv*RemoveAllUsersExceptThis*DemmonSrv*';
  S_Section_ShellCommands = '*DemmonSrv*ShellCommands*DemmonSrv*';



(*type
 TShareInfo2 = packed record
  shi2_netname : PWChar;
  shi2_type: DWORD;
  shi2_remark :PWChar;
  shi2_permissions: DWORD;
  shi2_max_uses : DWORD;
  shi2_current_uses : DWORD;
  shi2_path : PWChar;
  shi2_passwd : PWChar;
 end;

 TShareInfo50 = packed record
  shi50_netname : array [0..12] of Char;
  shi50_type : Byte;
  shi50_flags : Word;
  shi50_remark : PChar;
  shi50_path : PChar;
  shi50_rw_password : array [0..8] of Char;
  shi50_ro_password : array [0..8] of Char;
 end;

 TSessionInfo502 = packed record
   Sesi502_cname: PWideChar;
   Sesi502_username: PWideChar;
   Sesi502_num_opens: DWORD;
   Sesi502_time: DWORD;
   Sesi502_idle_time: DWORD;
   Sesi502_user_flags: DWORD;
   Sesi502_cltype_name: PWideChar;
   Sesi502_transport: PWideChar;
 End;
 PSessionInfo502 = ^TSessionInfo502;
 TSessionInfo502Array = array[0..512] of TSessionInfo502;
 PSessionInfo502Array = ^TSessionInfo502Array;

 TShareInfo2Array = array [0..512] of TShareInfo2;
 PShareInfo2Array = ^ TShareInfo2Array;


var
  NetShareEnumNT:function (	servername:PWChar;
                          level:DWORD;
                          bufptr:Pointer;
                          prefmaxlen:DWORD;
                          entriesread,
                          totalentries,
                          resume_handle:LPDWORD): DWORD; stdcall;

  NetShareEnum:function (	pszServer	: PChar;
                        sLevel		: Cardinal;
                        pbBuffer	: Pchar;
                        cbBuffer	: Cardinal;
                        pcEntriesRead,
                        pcTotalAvail: Pointer):DWORD; stdcall;

  NetShareDelNT:function (servername: PWideChar;
                        netname: PWideChar;
                        reserved: DWORD): LongInt; stdcall;

  NetShareDel:function (  pszServer,
                        pszNetName:PChar;
                        usReserved:Word): DWORD; stdcall;

*)
procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  srvDemon.Controller(CtrlCode);
end;

function IsNT(var Value: Boolean): Boolean;
var
  Ver: TOSVersionInfo;
begin
  Ver.dwOSVersionInfoSize := SizeOf(TOSVersionInfo);
  Result := GetVersionEx(Ver);
  if not Result then Exit;

  Value := Ver.dwPlatformId = VER_PLATFORM_WIN32_NT;
end;


function TsrvDemon.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

{ THelperThread }


constructor THelperThread.Create;

  function _GetDebugServiceFileName: string;
  var
    S: string;
  begin
    Result := 'd:\DebulSrv.log';
    if FileExists(result) then DeleteFile(Result);
    Exit;

    GetModuleFileName(hInstance, PChar(S), MAX_PATH);
    S := PChar(S);
    if S <> '' then
      S := ExtractFilePath(S) + 'DebugLog.txt';

    Result := S;
  end;

  
var
  S: string;
//  OSNT: Boolean;
  Lst: TStringList;
begin
  inherited Create(False);
  FreeOnTerminate := True;

//  Lst := TStringList.Create;
  Ini := TMemoryIniFile.Create;
  SetLength(S, MAX_PATH);
  GetModuleFileName(hInstance, PChar(S), MAX_PATH);
  S := PChar(S);
  if S <> '' then
  begin
    S := ExtractFilePath(S) + 'ShellCommand.ini';
    if FileExists(S) then
    begin
      Lst := TStringList.Create;
      try
        Lst.LoadFromFile(S);
        Ini.SetStrings(Lst);

      finally
        Lst.Free;
      end;
    end;
    SleepTime := Ini.ReadInteger(S_Section_Settings, 'SleepTime', 60);
  end;

{$IFDEF DebugService}
  FDebugServiceFile := _GetDebugServiceFileName;
{$ENDIF DebugService}
(*  @NetShareEnumNT := nil;
  @NetShareDelNT := nil;

  @NetShareEnum := nil;
  @NetShareDel := nil;

  if not IsNT(OSNT) then Exit;

  if OSNT then
  begin
    FNetApiNTHandle := LoadLibrary('NETAPI32.DLL'); //��������� ����������
    if FNetApiNTHandle = 0 then Exit;

    @NetShareEnumNT := GetProcAddress(FNetApiNTHandle, 'NetShareEnum');
    @NetShareDelNT := GetProcAddress(FNetApiNTHandle, 'NetShareDel');
  end else begin
    FNetApiHandle := LoadLibrary('SVRAPI.DLL'); //��������� ����������
    @NetShareEnum := GetProcAddress(FNetApiHandle, 'NetShareEnum');
    @NetShareDel := GetProcAddress(FNetApiHandle,'NetShareDel');
  end;
*)
end;

destructor THelperThread.Destroy;
begin
(*  @NetShareEnumNT := nil;
  @NetShareDelNT := nil;

  @NetShareEnum := nil;
  @NetShareDel := nil;

  if FNetApiNTHandle <> 0 then
    FreeLibrary(FNetApiNTHandle);
  if FNetApiHandle <> 0 then
    FreeLibrary(FNetApiHandle);   *)

  Ini.Free;
  inherited;
end;

{$IFDEF DebugService}
procedure THelperThread.WriteLog(const ALogTxt: string);
var
  Lst: TStringList;
begin
  Lst := TStringList.Create;
  try
    Lst.Clear;
    if FDebugServiceFile <> '' then
    begin
      if FileExists(FDebugServiceFile) then
        Lst.LoadFromFile(FDebugServiceFile);
      Lst.Add(DateTimeToStr(Now) + ': ' + ALogTxt);
      Lst.SaveToFile(FDebugServiceFile);
    end;
  finally
    Lst.Free;
  end;
end;
{$ENDIF DebugService}


procedure THelperThread.Execute;
var
  Lst: TStringList;

  procedure _RemoveShares;
  type
    TShareInfoArray2 = array[0..0] of TShareInfo2;
  var
    i, J: Integer;
//    Shares: TStringList;
    ShareNT : ^TShareInfoArray2;
    entriesread,totalentries:DWORD; //<= ��� Windows NT
    R: NET_API_STATUS;
    WS: PWideChar;
  begin
    Lst.Clear;
    ShareNT := nil; //������� ��������� �� ������ ��������

    //����� �������
    if NetShareEnum(nil, 2, Pointer(ShareNT), DWORD(-1), entriesread, totalentries, nil) <> 0 then
      Exit;
    try
      if entriesread <= 0 then Exit;

      if Terminated then Exit;

      Ini.ReadSectionText(S_Section_RemoveShares, Lst);
      for I := 0 to Lst.Count - 1 do
      begin
        if Terminated then Exit;

        WS := StringToOleStr(AnsiUpperCase(Lst[i]));

        for j := 0 to entriesread - 1 do
        begin
          if not WideSameStr(AnsiUpperCase(ShareNT^[j].shi2_path), WS) then Continue;
          R := NetShareDel(nil, ShareNT^[j].shi2_netname, 0);
        end;{for j}
      end;{for i}
    finally
      NetApiBufferFree(Pointer(ShareNT));
    end;
  end;

  procedure _RemoveUsers;
  type
    TGroupInfoArray0 = array[0..0] of TLocalGroupInfo0;
    TGroupMemberArray2 = array[0..0] of TLocalGroupMembersInfo2;
  var
    I, J: Integer;
    P: Pointer;
    GI: ^TGroupInfoArray0;
    GM: ^TGroupMemberArray2;
    N, NTotal, NUs, NUsTotal: Cardinal;
    S: string;
  begin
    GI := nil;
    GM := nil;
    Lst.Clear;
    Ini.ReadSectionText(S_Section_RemoveUsers, Lst);

    if NetLocalGroupEnum(nil, 0, P, Cardinal(-1), N, NTotal, nil) <> NERR_Success then Exit;
    try
      GI := P;
      for I := 0 to N - 1 do
      begin
        P := nil;
        if NetLocalGroupGetMembers(nil, GI^[i].lgrpi0_name, 2, P, Cardinal(-1), NUs, NUsTotal, nil) <> NERR_Success then Continue;
        try
          GM := P;
          for j := 0 to NUs - 1 do
          begin
            S := WideCharToString(GM^[j].lgrmi2_domainandname);
            if Lst.IndexOf(S) >= 0 then
              NetLocalGroupDelMember(nil, GI^[i].lgrpi0_name, GM^[j].lgrmi2_sid);
          end;{for j}
        finally
          NetApiBufferFree(Pointer(GM));
        end;
      end;{for i}
    finally
      NetApiBufferFree(Pointer(GI));
    end;
  end;

  procedure _RemoveAllGroupMembersExceptThisByGroup;
  type
    TGroupInfoArray0 = array[0..0] of TLocalGroupInfo0;
    TGroupMemberArray2 = array[0..0] of TLocalGroupMembersInfo2;
  var
    I, J: Integer;
    P: Pointer;
    GI: ^TGroupInfoArray0;
    GM: ^TGroupMemberArray2;
    N, NTotal, NUs, NUsTotal: Cardinal;
    S, SectName: string;
  begin
    GI := nil;
    GM := nil;

    if NetLocalGroupEnum(nil, 0, P, Cardinal(-1), N, NTotal, nil) <> NERR_Success then Exit;
    try
      GI := P;

      for I := 0 to N - 1 do
      begin
        P := nil;

        SectName := GI^[i].lgrpi0_name;
        Lst.Clear;
        if not Ini.SectionExists(SectName) then Continue;

        Ini.ReadSectionText(SectName, Lst);

        if NetLocalGroupGetMembers(nil, PChar(SectName), 2, P, Cardinal(-1), NUs, NUsTotal, nil) <> NERR_Success then Continue;
        try
          GM := P;
          for j := 0 to NUs - 1 do
          begin
            S := WideCharToString(GM^[j].lgrmi2_domainandname);
            if Lst.IndexOf(S) < 0 then
              NetLocalGroupDelMember(nil, PChar(SectName), GM^[j].lgrmi2_sid);
          end;{for j}
        finally
          NetApiBufferFree(Pointer(GM));
        end;
      end;{for i}
    finally
      NetApiBufferFree(Pointer(GI));
    end;
  end;

  procedure _RemoveAllUsersExceptThis;
  type
    TUserInfo0Array = array[0..0] of TUserInfo0;
  var
    i: Integer;
    NUs, NTotal: Cardinal;
    Usrs: ^TUserInfo0Array;
  begin
    if not Ini.SectionExists(S_Section_RemoveAllUsersExceptThis) then Exit;

    Lst.Clear;
    Ini.ReadSectionText(S_Section_RemoveAllUsersExceptThis, Lst);
    if Lst.Count <= 0 then Exit;

    if NetUserEnum(nil, 0, FILTER_NORMAL_ACCOUNT, Pointer(Usrs), MAX_PREFERRED_LENGTH, NUs, NTotal, nil) <> NERR_Success then Exit;
    try
      for i := 0 to NUs - 1 do
      begin
        if Lst.IndexOf(Usrs^[i].usri0_name) < 0 then
          NetUserDel(nil, Usrs^[i].usri0_name);
      end;{for i}
    finally
      NetApiBufferFree(Pointer(Usrs));
    end;
  end;

  procedure _ShellCommands;
  var
    i, N: Integer;
    S, SFN, SParam: string;
  begin
    Lst.Clear;
    Ini.ReadSectionText(S_Section_ShellCommands, Lst);
    for I := 0 to Lst.Count - 1 do
    begin
      if Terminated then Exit;

      S := Lst[i];

      N := Pos(' ', S);
      if N > 0 then
      begin
        SFN := Copy(S, 1, N - 1);
        SParam := Copy(S, N + 1, Length(S) - N - 1);
      end else
      begin
        SFN := S;
        SParam := '';
      end;

      ShellExecute(0, 'OPEN', PChar(SFN), PChar(SParam), nil, SW_HIDE);
    end;{for i}
  end;

var
  i: Integer;
  OSNT: Boolean;
{$IFDEF DebugService}
  S: string;
{$ENDIF DebugService}
begin
  if not IsNT(OSNT) then Exit;

  if not OSNT then Exit;

  Lst := TStringList.Create;
  try
    Lst.CaseSensitive := False;
    while not Terminated do
    begin
      try
{$IFDEF DebugService}
        S := '_RemoveShares';
{$ENDIF DebugService}
        // Shared folders
        _RemoveShares;

{$IFDEF DebugService}
        S := '_RemoveUsers';
{$ENDIF DebugService}
        // users
        _RemoveUsers;

{$IFDEF DebugService}
        S := '_RemoveAllGroupMembersExceptThisByGroup';
{$ENDIF DebugService}
        // users
        _RemoveAllGroupMembersExceptThisByGroup;

{$IFDEF DebugService}
        S := '_RemoveAllUsersExceptThis';
{$ENDIF DebugService}
        // users
        _RemoveAllUsersExceptThis;

{$IFDEF DebugService}
        S := '_Shellcommands';
{$ENDIF DebugService}
        // Shell Commands
        _Shellcommands;
      except
{$IFDEF DebugService}
        on E: Exception do
        begin
          WriteLog(S + ':' + E.Message);
        end;
{$ENDIF DebugService}
      end;

      i := 0;
      while i < SleepTime * 10 do
      begin
        Sleep(100);
        if Terminated then Exit;

        Inc(I);
      end;{while }
    end;{while}

  finally
    Lst.Free;
  end;
end;

procedure THelperThread.GetShares(const AList: TStringList);
type
  TShareInfoArray2 = array[0..0] of TShareInfo2;
var
  i:Integer;
  ShareNT : ^TShareInfoArray2;
  entriesread,totalentries:DWORD; //<= ��� Windows NT
  OSNT: Boolean;
begin
  if not IsNT(OSNT) then Exit; //���������� ��� �������

  if not OSNT then Exit;

  ShareNT := nil; //������� ��������� �� ������ ��������
  //����� �������
  if NetShareEnum(nil, 2, Pointer(ShareNT), DWORD(-1), entriesread, totalentries, nil) <> 0 then
    Exit;
  try
    if entriesread > 0 then //��������� �����������
      for i := 0 to entriesread - 1 do
        AList.Add(ShareNT^[i].shi2_path);
  finally
    NetApiBufferFree(Pointer(ShareNT));
  end;
end;


procedure TsrvDemon.ServiceContinue(Sender: TService;
  var Continued: Boolean);
begin
  if Assigned(FThread) then
    FThread.Resume;
end;

procedure TsrvDemon.ServiceCreate(Sender: TObject);
begin
  FThread := nil;
end;

procedure TsrvDemon.ServiceDestroy(Sender: TObject);
begin
  if Assigned(FThread) then
  begin
    FThread.Terminate;
    FThread := nil;
  end;
end;

procedure TsrvDemon.ServicePause(Sender: TService; var Paused: Boolean);
begin
  if Assigned(FThread) then
    FThread.Suspend;
end;

procedure TsrvDemon.ServiceStart(Sender: TService; var Started: Boolean);
begin
  FThread := THelperThread.Create;
end;

procedure TsrvDemon.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  if Assigned(FThread) then
  begin
    FThread.Terminate;
    FThread := nil;
  end;
end;

{ TWlStringList }

function TWlStringList.CompareStrings(const S1, S2: string): Integer;
begin
  if CaseSensitive then
    Result := AnsiCompareStr(S1, S2)
  else
    Result := AnsiCompareStr(AnsiUpperCase(S1), AnsiUpperCase(S2));
end;

end.
