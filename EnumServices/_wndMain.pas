unit _wndMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxCustomData, cxStyles, cxTL, cxLabel,
  cxTextEdit, cxInplaceContainer, cxControls, ImgList,
  cxLookAndFeelPainters, cxLookAndFeels, cxButtons, XPMan, dxBar,
  dxLayoutControl, cxContainer, cxEdit, cxSplitter, ExtCtrls, ActnList,
  cxClasses;

type
  TwndMain = class(TForm)
    tlServices: TcxTreeList;
    clnName: TcxTreeListColumn;
    clnDispName: TcxTreeListColumn;
    clnPath: TcxTreeListColumn;
    clnStatus: TcxTreeListColumn;
    clnExeName: TcxTreeListColumn;
    ilSmall: TImageList;
    LookAndFeelController: TcxLookAndFeelController;
    XPManifest: TXPManifest;
    pnRight: TPanel;
    Splitter: TcxSplitter;
    lcgRootGroup_Root: TdxLayoutGroup;
    lcgRoot: TdxLayoutControl;
    edName: TcxTextEdit;
    edDisplayName: TcxTextEdit;
    edExeName: TcxTextEdit;
    edFullPath: TcxTextEdit;
    lciName: TdxLayoutItem;
    lciDisplayName: TdxLayoutItem;
    lciExeName: TdxLayoutItem;
    lciFullPath: TdxLayoutItem;
    dxBarDockControl1: TdxBarDockControl;
    BarManager: TdxBarManager;
    btnStop: TdxBarButton;
    btnPause: TdxBarButton;
    btnStart: TdxBarButton;
    btnRestart: TdxBarButton;
    btnRefresh: TdxBarButton;
    ActionList: TActionList;
    actStop: TAction;
    actPause: TAction;
    actStart: TAction;
    actRestart: TAction;
    actRefresh: TAction;
    dxBarButton1: TdxBarButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actRefreshExecute(Sender: TObject);
    procedure tlServicesDeletion(Sender: TObject; ANode: TcxTreeListNode);
    procedure tlServicesFocusedNodeChanged(Sender: TObject;
      APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure actRestartExecute(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
    procedure actStopUpdate(Sender: TObject);
    procedure actPauseExecute(Sender: TObject);
    procedure actPauseUpdate(Sender: TObject);
    procedure actStartExecute(Sender: TObject);
    procedure actStartUpdate(Sender: TObject);
  private
    { Private declarations }
    ServMan: THandle;
    procedure ClearPanel;
    procedure RefreshNode(const ANode: TcxTreeListNode);
  public
    { Public declarations }
  end;

var
  wndMain: TwndMain;

implementation

{$R *.dfm}

uses
  WinSvc;

type
  TshServiceConfig = packed record
    dwServiceType: DWORD;
    dwStartType: DWORD;
    dwErrorControl: DWORD;
    lpBinaryPathName: string;
    lpLoadOrderGroup: string;
    dwTagId: DWORD;
    lpDependencies: string;
    lpServiceStartName: string;
    lpDisplayName: string;
  end;

  TshServiceInfo = class
  private
    FDisplayName: string;
    FName: string;
    FStatus: TServiceStatus;
    FConfig: TshServiceConfig;
    procedure SetConfig(const Value: TshServiceConfig);
    procedure SetStatus(const Value: TServiceStatus);
    function GetFullExeName: string;
  public
    function UpdateServiceStatus(AServMan: THandle): Boolean;
    function ServiceControl(AServMan: THandle; AControlFlag: Cardinal): Boolean;
    function ServiceStart(AServMan: THandle): Boolean;
    function GetServiceConfig(AServMan: THandle): Boolean;
    function GetServiceStatus(AServMan: THandle): Boolean;

    property Name: string read FName write FName;
    property DisplayName: string read FDisplayName write FDisplayName;
    property FullExeName: string read GetFullExeName;
    property Status: TServiceStatus read FStatus write SetStatus;
    property Config: TshServiceConfig read FConfig write SetConfig;

    procedure FillInfo(const AServMan: THandle; const AServStatusRec: TEnumServiceStatus);
  end;


//==============================================================================
// Utils
//==============================================================================

function _GetServiceStatus(AStatus: Cardinal): string;
begin
  case AStatus of
    SERVICE_CONTINUE_PENDING: Result := 'Continue is pending'; //The service continue is pending.
    SERVICE_PAUSE_PENDING: Result := 'Pause is pending'; //The service pause is pending.
    SERVICE_PAUSED: Result := 'Paused'; //The service is paused.
    SERVICE_RUNNING: Result := 'Running'; //The service is running.
    SERVICE_START_PENDING: Result := 'Starting'; //The service is starting.
    SERVICE_STOP_PENDING: Result := 'Stopping'; //The service is stopping.
    SERVICE_STOPPED: Result := 'Stopped';
  end;{case}
end;{Internal _GetServiceStatus}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
//  YurtehService.Controller(CtrlCode);
end;

//==============================================================================
// TServiceInfo
//==============================================================================
{ TServiceInfo }



procedure TshServiceInfo.FillInfo(const AServMan: THandle;
  const AServStatusRec: TEnumServiceStatus);
begin
  Name := AServStatusRec.lpServiceName;
  DisplayName := AServStatusRec.lpDisplayName;
  Status := AServStatusRec.ServiceStatus;

  GetServiceConfig(AServMan);
end;

//------------------------------------------------------------------------------

function TshServiceInfo.GetFullExeName: string;
begin
  Result := Config.lpBinaryPathName;
end;

//------------------------------------------------------------------------------

function TshServiceInfo.GetServiceConfig(AServMan: THandle): Boolean;
var
  C: Cardinal;
  Serv: THandle;
  ServConfig: PQueryServiceConfig;
begin
  Serv := OpenService(AServMan, PChar(FName), GENERIC_READ);
  Result := Serv <> 0;
  if not Result then Exit;

  try
    ServConfig := nil;
    QueryServiceConfig(Serv, ServConfig, SizeOf(ServConfig), C);
    GetMem(ServConfig, C);
    try
      QueryServiceConfig(Serv, ServConfig, C, C);

      FConfig.dwServiceType      := ServConfig^.dwServiceType;
      FConfig.dwStartType        := ServConfig^.dwStartType;
      FConfig.dwErrorControl     := ServConfig^.dwErrorControl;
      FConfig.lpBinaryPathName   := ServConfig^.lpBinaryPathName;
      FConfig.lpLoadOrderGroup   := ServConfig^.lpLoadOrderGroup;
      FConfig.dwTagId            := ServConfig^.dwTagId;
      FConfig.lpDependencies     := ServConfig^.lpDependencies;
      FConfig.lpServiceStartName := ServConfig^.lpServiceStartName;
      FConfig.lpDisplayName      := ServConfig^.lpDisplayName;
    finally
      FreeMem(ServConfig);
    end;
  finally
    CloseServiceHandle(Serv)
  end;
end;

//------------------------------------------------------------------------------

function TshServiceInfo.GetServiceStatus(AServMan: THandle): Boolean;
var
  C: Cardinal;
  Serv: THandle;
  ServConfig: PQueryServiceConfig;
begin
  Serv := OpenService(AServMan, PChar(FName), GENERIC_READ);
  Result := Serv <> 0;
  if not Result then Exit;

  try
    Result := QueryServiceStatus(Serv, FStatus);
  finally
    CloseServiceHandle(Serv);
  end;
end;

//------------------------------------------------------------------------------


procedure TshServiceInfo.SetConfig(const Value: TshServiceConfig);
begin
  FConfig := Value;
end;

//------------------------------------------------------------------------------

procedure TshServiceInfo.SetStatus(const Value: TServiceStatus);
begin
  FStatus := Value;
end;

//------------------------------------------------------------------------------

function TshServiceInfo.UpdateServiceStatus(AServMan: THandle): Boolean;
var
  C: Cardinal;
  Serv, StatusHandle: THandle;
  ServConfig: PQueryServiceConfig;
  DD: DWORD;
begin
  Serv := OpenService(AServMan, PChar(FName), GENERIC_ALL);
  Result := Serv <> 0;
  if not Result then Exit;

  try

    DD := FConfig.dwTagId;
    Result := ChangeServiceConfig(Serv, FConfig.dwServiceType,
      SERVICE_NO_CHANGE, SERVICE_NO_CHANGE, nil, nil, nil, nil, nil, nil, nil);

  finally
    CloseServiceHandle(Serv);
  end;
end;

//------------------------------------------------------------------------------

function TshServiceInfo.ServiceControl(AServMan: THandle; AControlFlag: Cardinal): Boolean;
var
  Serv: THandle;
  ServConfig: PQueryServiceConfig;
begin
  Serv := OpenService(AServMan, PChar(FName), SERVICE_ALL_ACCESS);

  Result := Serv <> 0;
  if not Result then Exit;

  try
    Result := ControlService(Serv, AControlFlag, FStatus);
  finally
    CloseServiceHandle(Serv);
  end;
end;

//------------------------------------------------------------------------------

function TshServiceInfo.ServiceStart(AServMan: THandle): Boolean;
var
  Serv: THandle;
  ServConfig: PQueryServiceConfig;
  S: PChar;
begin
  Serv := OpenService(AServMan, PChar(FName), SERVICE_ALL_ACCESS);

  Result := Serv <> 0;
  if not Result then Exit;

  try
    S := nil;
    Result := StartService(Serv, 0, S);
    if not Result then
      Exit;
      
    GetServiceConfig(AServMan);
  finally
    CloseServiceHandle(Serv);
  end;
end;

//==============================================================================
// TwndMain
//==============================================================================
{ TwndMain }

procedure TwndMain.FormShow(Sender: TObject);
begin
  ServMan := OpenSCManager(nil, nil, GENERIC_ALL);
  actRefresh.Execute;

  WindowState := wsMaximized;
end;

//------------------------------------------------------------------------------

procedure TwndMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseServiceHandle(ServMan);
end;

//------------------------------------------------------------------------------

procedure TwndMain.ClearPanel;
begin
  edName.Text := '';
  edDisplayName.Text := '';
  edExeName.Text := '';
  edFullPath.Text := '';
end;

procedure TwndMain.dxBarButton1Click(Sender: TObject);
var
  ServData: TshServiceInfo;
  N: Cardinal;
begin
  ServData := tlServices.FocusedNode.Data;
  N := ServData.Config.dwServiceType;

  if (N and SERVICE_INTERACTIVE_PROCESS) = SERVICE_INTERACTIVE_PROCESS then
    N := N and not SERVICE_INTERACTIVE_PROCESS
  else
    N := N or SERVICE_INTERACTIVE_PROCESS;

  ServData.FConfig.dwServiceType := N;
  if not Servdata.UpdateServiceStatus(ServMan) then
    MessageDlg(SysErrorMessage(GetLastError), mtError, [mbOK], 0);  
end;

//------------------------------------------------------------------------------

procedure TwndMain.RefreshNode(const ANode: TcxTreeListNode);
var
  ServData: TshServiceInfo;
begin
  ServData := ANode.Data;
  with ANode do
  begin
    Values[0] := ServData.Name;
    Values[1] := ServData.DisplayName;
    Values[2] := ExtractFileName(ServData.FullExeName);
    Values[3] := ServData.FullExeName;
    Values[4] := _GetServiceStatus(ServData.Status.dwCurrentState);
    case ServData.Status.dwCurrentState of
      SERVICE_PAUSED: ImageIndex := 0;
      SERVICE_RUNNING: ImageIndex := 1;
      SERVICE_STOPPED: ImageIndex := 2;
    end;{case}
    SelectedIndex := ImageIndex;
  end;
end;

//------------------------------------------------------------------------------

procedure TwndMain.actRefreshExecute(Sender: TObject);

  //-------------------------------------

  procedure _AddService(const AServData: TshServiceInfo);
  var
    ND: TcxTreeListNode;
  begin
    ND := tlServices.Add;
    ND.Data := AServData;
    RefreshNode(ND);
  end;{Internal _AddService}

  //-------------------------------------


type
  TServStatusArr = array[0..0] of TEnumServiceStatus;
var
  i: Integer;
  ServInfo: TEnumServiceStatus;
  ServStatus: ^TServStatusArr;
  R, C, R1: Cardinal;
  ServData: TshServiceInfo;
begin
  R1 := 0; R := 0; C := 0;
  EnumServicesStatus(ServMan, SERVICE_WIN32, SERVICE_ACTIVE or SERVICE_INACTIVE,
    ServInfo, 0, R, C, R1);

  tlServices.BeginUpdate;
  GetMem(ServStatus, R);
  try
    tlServices.Clear;
    ZeroMemory(ServStatus, R);
    if not EnumServicesStatus(ServMan, SERVICE_WIN32, SERVICE_ACTIVE or
      SERVICE_INACTIVE, TEnumServiceStatus(ServStatus^), R, R, C, R1) then
    begin
      MessageDlg('Cannot enum services', mtWarning, [mbOK], 0);
      Exit;
    end;

    for i := 0 to C - 1 do
    begin
      ServInfo := ServStatus^[i];
      ServData := TshServiceInfo.Create;
      ServData.FillInfo(ServMan, ServInfo);
      _AddService(ServData);
    end;{for i}
  finally
    FreeMem(ServStatus);
    tlServices.EndUpdate;
  end;
end;

//------------------------------------------------------------------------------

procedure TwndMain.actRestartExecute(Sender: TObject);
begin
  //
end;

//------------------------------------------------------------------------------

procedure TwndMain.actStartExecute(Sender: TObject);
var
  SI: TshServiceInfo;
  B: Boolean;
  OldCur: TCursor;
begin
  if not Assigned(tlServices.FocusedNode) then Exit;

  SI := TshServiceInfo(tlServices.FocusedNode.Data);
  if not Assigned(SI) then Exit;

  B := False;

  OldCur := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if SI.Status.dwCurrentState = SERVICE_RUNNING then Exit
    else

    if SI.Status.dwCurrentState = SERVICE_PAUSED then
      B := SI.ServiceControl(ServMan, SERVICE_CONTROL_CONTINUE)
    else

    if SI.Status.dwCurrentState = SERVICE_STOPPED then
      B := SI.ServiceStart(ServMan);

    if B then
    begin
      while (SI.FStatus.dwCurrentState = SERVICE_START_PENDING) or
            (SI.FStatus.dwCurrentState = SERVICE_CONTINUE_PENDING) or
            (SI.FStatus.dwCurrentState = SERVICE_STOPPED)
      do begin
        Sleep(100);
        SI.GetServiceStatus(ServMan);
      end;
      RefreshNode(tlServices.FocusedNode);
    end;
  finally
    Screen.Cursor := OldCur;
  end;
end;

//------------------------------------------------------------------------------

procedure TwndMain.actStartUpdate(Sender: TObject);
var
  SI: TshServiceInfo;
begin
  if not Assigned(tlServices.FocusedNode) then
  begin
    TAction(Sender).Enabled := False;
    Exit;
  end;

  SI := TshServiceInfo(tlServices.FocusedNode.Data);
  TAction(Sender).Enabled := Assigned(SI)
    and ((SI.Status.dwCurrentState = SERVICE_STOPPED) or
         (SI.Status.dwCurrentState = SERVICE_PAUSED));
end;

//------------------------------------------------------------------------------

procedure TwndMain.actStopExecute(Sender: TObject);
var
  SI: TshServiceInfo;
  OldCur: TCursor;
begin
  if not Assigned(tlServices.FocusedNode) then Exit;

  SI := TshServiceInfo(tlServices.FocusedNode.Data);
  if not Assigned(SI) then Exit;

  if SI.Status.dwCurrentState = SERVICE_STOPPED then Exit;

  OldCur := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if SI.ServiceControl(ServMan, SERVICE_CONTROL_STOP) then
    begin
      while SI.FStatus.dwCurrentState = SERVICE_STOP_PENDING do
      begin
        Sleep(100);
        SI.GetServiceStatus(ServMan);
      end;

      RefreshNode(tlServices.FocusedNode);
    end;
  finally
    Screen.Cursor := OldCur;
  end;
end;

//------------------------------------------------------------------------------

procedure TwndMain.actStopUpdate(Sender: TObject);
var
  SI: TshServiceInfo;
begin
  if not Assigned(tlServices.FocusedNode) then
  begin
    TAction(Sender).Enabled := False;
    Exit;
  end;

  SI := TshServiceInfo(tlServices.FocusedNode.Data);
  TAction(Sender).Enabled := Assigned(SI)
    and (SI.Status.dwCurrentState <> SERVICE_STOPPED);
end;

//------------------------------------------------------------------------------

procedure TwndMain.actPauseExecute(Sender: TObject);
var
  SI: TshServiceInfo;
  OldCur: TCursor;
begin
  if not Assigned(tlServices.FocusedNode) then Exit;

  SI := TshServiceInfo(tlServices.FocusedNode.Data);
  if not Assigned(SI) then Exit;

  if (SI.Status.dwCurrentState = SERVICE_STOPPED) or (SI.Status.dwCurrentState = SERVICE_PAUSED)
  then Exit;

  OldCur := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if SI.ServiceControl(ServMan, SERVICE_CONTROL_PAUSE) then
    begin
      while SI.FStatus.dwCurrentState = SERVICE_PAUSE_PENDING do
      begin
        Sleep(100);
        SI.GetServiceStatus(ServMan);
      end;

      RefreshNode(tlServices.FocusedNode);
    end;
  finally
    Screen.Cursor := OldCur;
  end;
end;

//------------------------------------------------------------------------------

procedure TwndMain.actPauseUpdate(Sender: TObject);
var
  SI: TshServiceInfo;
begin
  if not Assigned(tlServices.FocusedNode) then
  begin
    TAction(Sender).Enabled := False;
    Exit;
  end;

  SI := TshServiceInfo(tlServices.FocusedNode.Data);
  TAction(Sender).Enabled := Assigned(SI)
    and (SI.Status.dwCurrentState = SERVICE_RUNNING);
end;

//------------------------------------------------------------------------------

procedure TwndMain.tlServicesDeletion(Sender: TObject;
  ANode: TcxTreeListNode);
begin
  TObject(ANode.Data).Free;
end;

//------------------------------------------------------------------------------

procedure TwndMain.tlServicesFocusedNodeChanged(Sender: TObject;
  APrevFocusedNode, AFocusedNode: TcxTreeListNode);
var
  Data: TshServiceInfo;
begin
  if not Assigned(AFocusedNode) then
  begin
    ClearPanel;
    Exit;
  end;

  Data := TshServiceInfo(AFocusedNode.Data);

  edName.Text := Data.Name;
  edDisplayName.Text := Data.DisplayName;
  edExeName.Text := ExtractFileName(Data.FullExeName);
  edFullPath.Text := Data.FullExeName;
end;

//------------------------------------------------------------------------------

end.
