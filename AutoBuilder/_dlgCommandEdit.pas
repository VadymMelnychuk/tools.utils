unit _dlgCommandEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.StdCtrls, Vcl.ExtCtrls,
  cxMemo, cxTextEdit;

type
  TdlgCommandEdit = class(TForm)
    edCode: TcxTextEdit;
    edCaption: TcxTextEdit;
    memText: TcxMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function ShowCmdEditor(var ACode, ACaption, AText: string): Boolean;

implementation

{$R *.dfm}

function ShowCmdEditor(var ACode, ACaption, AText: string): Boolean;
var
  d: TdlgCommandEdit;
begin
  d := TdlgCommandEdit.Create(Application);
  try
    d.edCode.Text := ACode;
    d.edCaption.Text := ACaption;
    d.memText.Text := AText;

    Result := D.ShowModal = mrOK;
    if not Result then Exit;

    ACode := d.edCode.Text;
    ACaption := d.edCaption.Text;
    AText := d.memText.Text;
  finally
    D.Free;
  end;
end;

end.
