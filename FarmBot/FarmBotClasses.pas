unit FarmBotClasses;
{.$DEFINE UseDetectEngine}

interface

uses
  Winapi.Windows, Winapi.Messages, System.Generics.Collections, System.SysUtils, System.Classes,
  VCL.StdCtrls, VCL.Forms,
  Vcl.Controls, Vcl.ComCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  MSXML, cxTL, cxEdit, cxExtEditRepositoryItems, cxEditRepositoryItems;

type
  TFarmUser = class;
  TKnownElementSimple = class;
  TKnownElementSelectable = class;

  TFarmBarnItem = class
  private
    FName: string;
    FCount: Integer;
    FID: Integer;
  public
    property ID: Integer read FID;
    property Name: string read FName;
    property Count: Integer read FCount write FCount;
  end;

  TFarmBarn = class(TObjectList<TFarmBarnItem>)
  public
    constructor Create;

    procedure AddBarnItem(const AName: string; const AID, ACount: Integer);
    function GetBarnItem(const AName: string): TFarmBarnItem; overload;
    function GetBarnItem(const ID: Integer): TFarmBarnItem; overload;

    procedure LoadBarn(const ACountry: IXMLDOMNode);
  end;

  TCachedCommand = class
  private
    FOwner: TObjectList<TCachedCommand>;
    FUser: TFarmUser;
    FItemID: Integer;
    FRoom: Integer;
    FCommand: string;
    FItemClass: string;
    FSecondID: Integer;
  public
    constructor Create(const AOwner: TObjectList<TCachedCommand>; const aUser: TFarmUser);
    function AsString: string;

    property Command: string read FCommand write FCommand;
    property Room: Integer read FRoom write FRoom;
    property ItemID: Integer read FItemID write FItemID;
    property ItemClass: string read FItemClass write FItemClass;
    property SecondID: Integer read FSecondID write FSecondID;
  end;


  TSelectableRes = class
  private
    FName: string;
    FChecked: Boolean;
  public
    property Name: string read FName write FName;
    property Checked: Boolean read FChecked write FChecked;
  end;

  TCustomizeForm = class(TForm)
  public
    procedure ItemToForm(const AItem: TKnownElementSimple); virtual; abstract;
    procedure FormToItem(const AItem: TKnownElementSimple); virtual; abstract;
  end;
  TCustomizeFormClass = class of TCustomizeForm;

  TKnownElementSimple = class
  private
    FName: string;
    FFabricNames: TStrings;
    FFixedRes: TStrings;
    FChecked: Boolean;
    FRoomID: Integer;
    FLastCommandCount: Integer;
//    FLastActiveTime: Cardinal;
//    FSleepTime: Integer;
    function GetCompletteItemCount(const AStr: string): Integer;
    function GetNSName: string;
    function DetectCount(const ANode: IXMLDOMNode): Integer;
  protected
    FOwner: TFarmUser;
    procedure AddCommand(const ACmd, AItemClass: string; const ARoomID, AItemID: Integer;
      const AList: TObjectList<TCachedCommand>); overload;
    procedure AddCommand(const ACmd, AItemClass: string; const ARoomID, AItemID, ASecondID: Integer;
      const AList: TObjectList<TCachedCommand>); overload;
    procedure PickNode(const AItemNode: IXMLDOMNode; const aList: TObjectList<TCachedCommand>;
      const AOutputNodeName: string); virtual;
    procedure ProcessSimpleNode(const AItemNode: IXMLDOMNode; const ARes: TFarmBarnItem;
      const aList: TObjectList<TCachedCommand>; const ABarn: TFarmBarn);virtual;
    function GetCustomizeDlgClass: TCustomizeFormClass; virtual;
    function getOneResource: Boolean; virtual;
  public
    constructor Create(const AOwner: TFarmUser; const AName: string;
      const AFabricNames, AFixedRes: array of string;
      const ARoomID: Integer = 0);
    destructor Destroy; override;

    procedure PrepareCommands(const AList: TObjectList<TCachedCommand>;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn); virtual;

    procedure Customize; virtual;
    function CanCustomize: Boolean;
    procedure SaveSettings(const ARootNode: IXMLDOMElement); virtual;
    procedure LoadSettings(const ARootNode: IXMLDOMElement); virtual;

    property Name: string read FName;
    property NSName: string read GetNSName;
    property FabricNames: TStrings read FFabricNames;
    property FixedRes: TStrings read FFixedRes;

    property RoomID: Integer read FRoomID write FRoomID;
    property Checked: Boolean read FChecked write FChecked;
    property OneResource: Boolean read getOneResource;
    property LastCommandCount: Integer read FLastCommandCount;
//    property LastActiveTime: Cardinal read FLastActiveTime;
//    property SleepTime: Integer read FSleepTime write FSleepTime;
  end;

  TKnownElementSimpleClass = class(TKnownElementSimple);

  TKnownElementPumpkin = class(TKnownElementSimple)
  protected
    procedure ProcessSimpleNode(const AItemNode: IXMLDOMNode; const ARes: TFarmBarnItem;
      const aList: TObjectList<TCachedCommand>; const ABarn: TFarmBarn); override;
  end;

  TKnownElementHive = class(TKnownElementSimple)
  public
    procedure PrepareCommands(const AList: TObjectList<FarmBotClasses.TCachedCommand>;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn); override;
  end;

  TKnownElementSelectable = class(TKnownElementSimple)
  private
    FSelectableRes: TObjectList<TSelectableRes>;
  protected
    function GetCustomizeDlgClass: TCustomizeFormClass; override;
  public
    constructor Create(const AOwner: TFarmUser;
      const AName: string; const AFabricNames, aFixedRes, ASelectableRes: array of string;
      const ARoomID: Integer = 0);
    destructor Destroy; override;

    procedure PrepareCommands(const AList: TObjectList<TCachedCommand>;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn); override;

    procedure SaveSettings(const ARootNode: IXMLDOMElement); override;
    procedure LoadSettings(const ARootNode: IXMLDOMElement); override;

    property SelectableRes: TObjectList<TSelectableRes> read FSelectableRes;
  end;

  TKnownElementTimedFarm = class(TKnownElementSelectable)
  protected
    function getOneResource: Boolean; override;
  public
    procedure PrepareCommands(const AList: TObjectList<FarmBotClasses.TCachedCommand>;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn); override;
  end;

  TKnownElementMultiSelectable = class(TKnownElementSelectable)
  private
    procedure ProcessMultipleNode(const AItemNode: IXMLDOMNode;
      AResMap: TDictionary<string, TFarmBarnItem>;
      const aList: TObjectList<TCachedCommand>);
  public
    procedure PrepareCommands(const AList: TObjectList<TCachedCommand>;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn); override;
  end;

  TKnownElementOther = class(TKnownElementSimple)
  private
    function KnownNode(const ANode: IXMLDOMNode): Boolean;
  protected
    function GetCustomizeDlgClass: TCustomizeFormClass; override;
    function IsHiveActive: Boolean;
  public
    constructor Create(const AOwner: TFarmUser; const AName: string; const ARoomID: Integer);
    procedure PrepareCommands(const AList: TObjectList<TCachedCommand>;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn); override;
  end;

  TKnownElementSoil = class(TKnownElementSelectable)
  private
    FPlantClass: string;
    FPlantClasses: TStrings;
  protected
    function getOneResource: Boolean; override;
  public
    constructor Create(const AOwner: TFarmUser; const AName: string; const ARoomID: Integer);
    destructor Destroy; override;

    procedure SaveSettings(const ARootNode: IXMLDOMElement); override;
    procedure LoadSettings(const ARootNode: IXMLDOMElement); override;

    procedure PrepareCommands(const AList: TObjectList<TCachedCommand>;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn); override;
    property PlantClass: string read FPlantClass write FPlantClass;
    property PlantClasses: TStrings read FPlantClasses;
  end;


  TFarmRoom = class
  private
    FName: string;
    FSleepTime: Integer;
    FLastCmdCount: Integer;
    FID: Integer;
    FLastActiveTime: Integer;
  public
    constructor Create(AID: Integer; const AName: string;
      ASleepTime: Integer);
    property ID: Integer read FID;
    property Name: string read FName;
    property SleepTime: Integer read FSleepTime write FSleepTime;
    property LastActiveTime: Integer read FLastActiveTime write FLastActiveTime;
    property LastCmdCount: Integer read FLastCmdCount write FLastCmdCount;
  end;

  TFarmUser = class
  private
    FAuthKey: string;
    FFlashVars: string;
    FUserID: string;
    FKnowns: TObjectList<TKnownElementSimple>;
    FRooms: TObjectList<TFarmRoom>;

    FLog: TMemo;
    FPage: TTabSheet;
    FTree: TcxTreeList;

    FBaseTickCount: Cardinal;
    FBaseTime: Cardinal;
    FCurExp: Integer;
    FName: string;
    cxEditRepository: TcxEditRepository;
    cxEditRepositoryCheckBoxItem: TcxEditRepositoryCheckBoxItem;
    cxEditRepositoryLabel: TcxEditRepositoryLabel;
    fIsInited: Boolean;
    fLogin: string;
    fPassword: string;
    FRollCounter: Integer;
    fThread: TThread;
    FServerUpdateRevision: Integer;
    FPostVersion: Integer;
    FNextExp: Integer;
    FServerTime: Integer;
    FTimeInitedST: Cardinal;
    FIgnoreSiesta: Boolean;
//    FMsgStack: TStringList;
//    hMutex: THandle;

    procedure InitKnownElements;
    procedure InitRooms;
    procedure CalcBaseTime;
    procedure PropCheckChange(Sender: TObject);
    procedure SetFlashVars(const AVars: string);
//    function GetLog: TStrings;
    procedure GetElementsByRoom(const ARoomID: Integer; const AList: TList<TKnownElementSimple>;
      AllElements: Boolean);
    procedure TreeListColumnGetEditProperties(
      Sender: TcxTreeListColumn; ANode: TcxTreeListNode;
      var EditProperties: TcxCustomEditProperties);
    procedure TreeListCollapsing(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; var Allow: Boolean);
    procedure TreeListIsGroupNode(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; var IsGroup: Boolean);

    procedure ThreadTerminated(Sender: TObject);
    procedure SetServerTime(const Value: Integer);
  public
    constructor Create(const AName: string; const AServUpdRevision: Integer; const APageControl: TPageControl);
    destructor Destroy; override;

    procedure Start;
    procedure Stop;
    function IsRunning: Boolean;
    function InitUser(aHttp: TIdHTTP): Boolean;


    function GetUserStatQuery(const ARoomID: Integer; const ARevision: string; const AAsFirstRun: Boolean): string;
    function PrepareExecQuery(const ARoomID: Integer; const ARev, ASes: string;
      const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn; var ACmdCount: Integer): string;

    procedure LoadSettings(const AUserNode: IXMLDOMElement);
    procedure SaveSettings(const ARootNode: IXMLDOMElement);
    procedure ShowKnownElements;

    procedure WriteLog(const AMsg: string);
    procedure ClearLog;
    function GetDeltaTime: Integer;

    property Name: string read FName;
    property UserID: string read FUserID;
    property AuthKey: string read FAuthKey write FAuthKey;
    property FlashVars: string read FFlashVars write SetFlashVars;
    property Knowns: TObjectList<TKnownElementSimple> read FKnowns;
    property CurExp: Integer read FCurExp write FCurExp;
    property NextExp: Integer read FNextExp write FNextExp;
    property TabPage: TTabSheet read FPage;
    property Tree: TcxTreeList read FTree;
    property Rooms: TObjectList<TFarmRoom> read FRooms;
    property RollCounter: Integer read FRollCounter write FRollCounter;
    property Login: string read FLogin write FLogin;
    property Password: string read fPassword write fPassword;
    property ServerUpdateRevision: Integer read FServerUpdateRevision;
    property PostVersion: Integer read FPostVersion write FPostVersion;
    property ServerTime: Integer read FServerTime write SetServerTime;
    property IgnoreSiesta: Boolean read FIgnoreSiesta write FIgnoreSiesta;
//    property MsgStack: TStringList read FMsgStack;
  end;



  TExecutorThread = class(TThread)
  private
    FIdHTTP: TIdHTTP;
    FUser: TFarmUser;
    fCurServerNo: Integer;
    FPostErrorCount: Integer;
    fRevision: string;
    FSessionKey: string;
    FSyncMsg: string;
    FCheckThread: TThread;

    procedure DetectRevision;
    function GetCurServer: string;
    procedure WriteLog(const AMsg: string);
    procedure SyncWriteLog;

    procedure RecreateHTTP(NeedSleep: Boolean);
//    procedure SyncWriteLog2(const AMsg: string);
    function InitVariable(const AXML: IXMLDOMDocument3; var aCountry, aField: IXMLDOMNode; aInitField: Boolean): Boolean;
  protected
    function IsSiestaTime(var AEndTime: TTime): Boolean;
    function IsNight: Boolean;
    function PostData(const AUrl, AData: string; const ARes: TStringStream): Boolean;
    procedure Execute; override;
    procedure Pause(aSec: Integer = 5);

  public
    constructor Create(const AUser: TFarmUser);
    destructor Destroy; override;

  end;

  THTTPUnfreez = class(TThread)
  private
    fUser: TFarmUser;
//    FCheckThread: TExecutorThread;
  protected
    procedure Execute; override;
  public
    constructor Create(const AThread: TExecutorThread);
//    destructor Destroy; override;
  end;

  TUserTabSheet = class(TTabSheet)
  private
    FUser: TFarmUser;
  public
    destructor Destroy; override;

    property User: TFarmUser read FUser write FUser;
  end;


function ConvertStr(const AString: string): string;

implementation

uses
  Dialogs, ComObj, ActiveX, Variants, HTTPApp,
  DateUtils, System.Types,
  IdStack,
  cxCheckbox,
  _dlgSelectableResources, ZLibExGZ;

const
//  cFarmServers: array[0..9] of string = ('http://88.212.226.150', 'http://88.212.226.151', 'http://88.212.226.153',
//    'http://88.212.226.154', 'http://88.212.226.155', 'http://88.212.226.156', 'http://88.212.226.36',
//    'http://88.212.226.37', 'http://88.212.226.196', 'http://91.243.116.7');
  cFarmServers: array[0..12] of string = ('http://88.212.226.36', 'http://88.212.226.37',
    'http://88.212.226.150', 'http://88.212.226.154', 'http://88.212.226.155', 'http://88.212.226.156', 'http://88.212.226.157',
    'http://88.212.226.196', 'http://88.212.226.251',
    'http://91.243.116.7', 'http://91.243.116.8', 'http://91.243.116.9', 'http://91.243.116.10');


  c_MainRoom = 0;
  c_Reservation = 1;
  c_TropicIsland = 2;
  c_ZooIsland = 3;
  c_DinoIsland = 4;
  c_MagicIsland = 5;
  c_MountainValey = 6;

function ConvertStr(const AString: string): string;
var
  i: Integer;
  S: string;
begin
  Result := '';
  S := LowerCase(AString);
  for I := 1 to Length(S) do
  begin
    if not CharInSet(S[i], ['a'..'z', 'A'..'Z', '0'..'9']) then
      Result := Result + '%' + IntToHex(Ord(AString[i]), 2)
    else
      Result := Result + AString[i];
  end;
end;

{ TFermBarn }


constructor TFarmBarn.Create;
begin
  inherited Create(True);
end;

procedure TFarmBarn.AddBarnItem(const AName: string; const AID,
  ACount: Integer);
var
  Itm: TFarmBarnItem;
begin
  Itm := TFarmBarnItem.Create;
  Itm.FName := AName;
  Itm.FCount := ACount;
  Itm.FID := AID;
  Add(Itm);
end;

function TFarmBarn.GetBarnItem(const AName: string): TFarmBarnItem;
var
  i: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[i].Name = AName then
    begin
      Result := Items[i];
      Break;
    end;
end;

function TFarmBarn.GetBarnItem(const ID: Integer): TFarmBarnItem;
var
  i: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[i].ID = ID then
    begin
      Result := Items[i];
      Break;
    end;
end;

procedure TFarmBarn.LoadBarn(const ACountry: IXMLDOMNode);
var
  I: Integer;
  N, nID, nQuant, nItm: IXMLDOMNode;
  L: IXMLDOMNodeList;
begin
  Clear;
  N := ACountry.selectSingleNode('barn');
  if not Assigned(N) then Exit;

  L := N.childNodes;
  for I := 0 to L.length - 1 do
  begin
    nItm := l.item[i];

    nID := nItm.attributes.getNamedItem('id');
    nQuant := nItm.attributes.getNamedItem('quantity');
    if not Assigned(nID) or not Assigned(nQuant) then
      Exit;

    AddBarnItem(nItm.nodeName, nID.nodeValue, nQuant.nodeValue);
  end;{for i}

end;

{ TSimpleKnownElement }

constructor TKnownElementSimple.Create(const AOwner: TFarmUser; const AName: string;
  const AFabricNames, AFixedRes: array of string; const ARoomID: Integer);
var
  i: Integer;
begin
  FOwner := AOwner;
  FFabricNames := TStringList.Create;
  FFixedRes := TStringList.Create;

  FName := AName;
  FChecked := False;
  FRoomID := ARoomID;
  FLastCommandCount := 0;
//  FLastActiveTime := 0;
//  FSleepTime := 1000;

  for I := 0 to High(AFabricNames) do
    FFabricNames.Add(AFabricNames[i]);

  for I := 0 to High(AFixedRes) do
    FFixedRes.Add(AFixedRes[i]);
end;

function TKnownElementSimple.CanCustomize: Boolean;
begin
  Result := GetCustomizeDlgClass <> nil;
end;

procedure TKnownElementSimple.Customize;
var
  D: TCustomizeForm;
begin
  if not CanCustomize then Exit;

  D := GetCustomizeDlgClass.Create(Application);
  try
    D.ItemToForm(Self);
    if D.ShowModal = mrOK then
      D.FormToItem(Self);
  finally
    D.Free;
  end;
end;

destructor TKnownElementSimple.Destroy;
begin
  FFabricNames.Free;
  FFixedRes.Free;
  inherited;
end;

procedure TKnownElementSimple.PrepareCommands(
  const AList: TObjectList<TCachedCommand>; const AFieldNode: IXMLDOMNode;
  const ABarn: TFarmBarn);
var
  i, J: Integer;
  L: IXMLDOMNodeList;
  nChild: IXMLDOMNode;
  s: string;
  rItm: TFarmBarnItem;
begin
  FLastCommandCount := 0;
//  FLastActiveTime := GetTickCount;

  L := AFieldNode.childNodes;

  rItm := nil;
  for S in FixedRes do
  begin
    rItm := ABarn.GetBarnItem(S);
    if Assigned(rItm) and (rItm.Count > 0) then
      Break;
  end;

  for I := 0 to L.length - 1 do
  begin
    nChild := L.item[i];

    for J := 0 to FFabricNames.Count - 1 do
    begin
      if nChild.nodeName = FFabricNames[J] then
        ProcessSimpleNode(nChild, rItm, AList, ABarn);
    end;{for j}
  end;{for i}
end;

function TKnownElementSimple.GetCompletteItemCount(const AStr: string): Integer;
var
  i: Integer;
begin
  Result := 1;
  for i := 1 to Length(AStr) do
    if AStr[i] = ',' then
      Inc(Result);
end;

function TKnownElementSimple.GetCustomizeDlgClass: TCustomizeFormClass;
begin
  Result := nil;
end;

function TKnownElementSimple.getOneResource: Boolean;
begin
  Result := False;
end;

function TKnownElementSimple.GetNSName: string;
begin
  Result := StringReplace(Name, ' ', '_', [rfReplaceAll]);
end;

procedure TKnownElementSimple.SaveSettings(const ARootNode: IXMLDOMElement);
begin
//  ARootNode.setAttribute('sleep_time', SleepTime);
end;

procedure TKnownElementSimple.LoadSettings(const ARootNode: IXMLDOMElement);
begin
//var
//  V: Variant;
//begin
//  V := ARootNode.getAttribute('sleep_time');
//  if not VarIsNull(V) then
//    SleepTime := Integer(V);
end;

procedure TKnownElementSimple.AddCommand(const ACmd, AItemClass: string; const ARoomID,
  AItemID: Integer; const AList: TObjectList<TCachedCommand>);
begin
  AddCommand(ACmd, AItemClass, ARoomID, AItemID, 0, AList);
end;

procedure TKnownElementSimple.AddCommand(const ACmd, AItemClass: string;
  const ARoomID, AItemID, ASecondID: Integer;
  const AList: TObjectList<TCachedCommand>);
var
  Cmd: TCachedCommand;
begin
  Cmd := TCachedCommand.Create(aList, FOwner);
  Cmd.ItemID := AItemID;
  Cmd.Room := ARoomID;
  Cmd.Command := ACmd;
  Cmd.ItemClass := AItemClass;
  Cmd.SecondID := ASecondID;

  AList.Add(Cmd);
  Inc(FLastCommandCount);
end;


function TKnownElementSimple.DetectCount(const ANode: IXMLDOMNode): Integer;
var
  S: string;
begin
  S := aNode.nodeValue;
  if Pos(',', S) > 0 then
    Result := GetCompletteItemCount(S)
  else begin
    Result := Integer(aNode.nodeValue);
    if Result > 6 then
      Result := 1;
  end;
end;

procedure TKnownElementSimple.PickNode(const AItemNode: IXMLDOMNode;
  const aList: TObjectList<TCachedCommand>; const AOutputNodeName: string);
var
  nOutputFill: IXMLDOMNode;
  ChldID, J, nCount: Integer;
begin
  nOutputFill := AItemNode.attributes.getNamedItem(AOutputNodeName);
  if Assigned(nOutputFill) then
  begin
    ChldID := AItemNode.attributes.getNamedItem('id').nodeValue;

    nCount :=  DetectCount(nOutputFill);

    for J := 0 to nCount - 1 do
      AddCommand('pick', '', RoomID, ChldID, aList);
  end;{if }
end;

procedure TKnownElementSimple.ProcessSimpleNode(const AItemNode: IXMLDOMNode;
  const ARes: TFarmBarnItem; const aList: TObjectList<TCachedCommand>;
  const ABarn: TFarmBarn);
var
  nInputFill: IXMLDOMNode;
  ChldID, J, nCount: Integer;
begin
  PickNode(AItemNode, aList, 'output_fill');

  if not Assigned(ARes) then Exit;

  ChldID := AItemNode.attributes.getNamedItem('id').nodeValue;
  if aRes.Count > 0 then
  begin
    nInputFill := AItemNode.attributes.getNamedItem('input_fill');
    if Assigned(nInputFill) then
      nCount := 3 - DetectCount(nInputFill)
    else
      nCount := 3;

    if aRes.Count < nCount then
      nCount := aRes.Count;

    for J := 0 to nCount - 1 do
    begin
      if Self.ClassType = TKnownElementSimple then
        AddCommand('put', '', RoomID, ChldID, aList)
      else
        AddCommand('put', aRes.Name, RoomID, ChldID, aList);
      aRes.Count := aRes.Count - 1;
    end;
  end;
end;

{ TKnownElementSelectable }

constructor TKnownElementSelectable.Create(const AOwner: TFarmUser;
  const AName: string; const AFabricNames, aFixedRes, ASelectableRes: array of string;
  const ARoomID: Integer);
var
  SR: TSelectableRes;
  i: Integer;
begin
  inherited Create(AOwner, AName, AFabricNames, aFixedRes, ARoomID);

  FSelectableRes := TObjectList<TSelectableRes>.Create(True);
  for I := 0 to High(ASelectableRes) do
  begin
    SR := TSelectableRes.Create;
    SR.Name := ASelectableRes[i];
    SR.Checked := True;

    FSelectableRes.Add(SR);
  end;
end;

destructor TKnownElementSelectable.Destroy;
begin
  FSelectableRes.Free;

  inherited;
end;

function TKnownElementSelectable.GetCustomizeDlgClass: TCustomizeFormClass;
begin
  Result := TdlgSelRes;
end;

procedure TKnownElementSelectable.SaveSettings(const ARootNode: IXMLDOMElement);
var
  R: TSelectableRes;
  N: IXMLDOMElement;
begin
  inherited SaveSettings(ARootNode);

  for R in FSelectableRes do
  begin
    N := ARootNode.selectSingleNode(R.Name) as IXMLDOMElement;
    if not Assigned(N) then
    begin
      N := ARootNode.ownerDocument.createElement(R.Name);
      ARootNode.appendChild(N);
    end;

    N.setAttribute('checked', R.Checked);
  end;
end;

procedure TKnownElementSelectable.LoadSettings(const ARootNode: IXMLDOMElement);
var
  R: TSelectableRes;
  N: IXMLDOMElement;
begin
  inherited LoadSettings(ARootNode);

  for R in FSelectableRes do
  begin
    R.Checked := False;
    N := ARootNode.selectSingleNode(R.Name) as IXMLDOMElement;
    if Assigned(N) then
      R.Checked := Boolean(N.getAttribute('checked'));
  end;
end;


procedure TKnownElementSelectable.PrepareCommands(
  const AList: TObjectList<TCachedCommand>; const AFieldNode: IXMLDOMNode;
  const ABarn: TFarmBarn);
var
  i, J: Integer;
  L: IXMLDOMNodeList;
  nChild: IXMLDOMNode;
  SResClass: TSelectableRes;
  rItm: TFarmBarnItem;
begin
  FLastCommandCount := 0;
//  FLastActiveTime := GetTickCount;

  L := AFieldNode.childNodes;

  rItm := nil;

  for SResClass in SelectableRes do
  begin
    if not SResClass.Checked then Continue;

    rItm := ABarn.GetBarnItem(SResClass.Name);
    if Assigned(rItm) and (rItm.Count > 0) then
      Break;
  end;

  for I := 0 to L.length - 1 do
  begin
    nChild := L.item[i];
    for J := 0 to FabricNames.Count - 1  do
    begin
      if nChild.nodeName = FabricNames[J] then
        ProcessSimpleNode(nChild, rItm, AList, ABarn);
    end;{for j}
  end;{for i}
end;


{ TKnownElementTimedFarm }

function TKnownElementTimedFarm.getOneResource: Boolean;
begin
  Result := True;
end;

procedure TKnownElementTimedFarm.PrepareCommands(
  const AList: TObjectList<FarmBotClasses.TCachedCommand>;
  const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn);
var
  i, J, ChldID: Integer;
  L: IXMLDOMNodeList;
  nChild: IXMLDOMNode;
  SResClass: TSelectableRes;
  SRes: string;
begin
  FLastCommandCount := 0;

  L := AFieldNode.childNodes;


  for SResClass in SelectableRes do
  begin
    if not SResClass.Checked then Continue;

    SRes := SResClass.Name;
    Break;
  end;

  for I := 0 to L.length - 1 do
  begin
    nChild := L.item[i];
    for J := 0 to FabricNames.Count - 1  do
    begin
      if nChild.nodeName = FabricNames[J] then
      begin
        PickNode(nChild, AList, 'contract_output');

        if not Assigned(nChild.attributes.getNamedItem('contract_input')) then
        begin
          ChldID := nChild.attributes.getNamedItem('id').nodeValue;
          AddCommand('put', sRes, RoomID, ChldID, aList);
        end;
      end;
    end;{for j}
  end;{for i}

end;

{ TKnownElementMultiSelectable }


procedure TKnownElementMultiSelectable.ProcessMultipleNode(const AItemNode: IXMLDOMNode;
  AResMap: TDictionary<string, TFarmBarnItem>;
  const aList: TObjectList<TCachedCommand>);

  function _DetectCount(const ANode: IXMLDOMNode): Integer;
  var
    S: string;
  begin
    S := aNode.nodeValue;
    if Pos(',', S) > 0 then
      Result := GetCompletteItemCount(S)
    else begin
      Result := Integer(aNode.nodeValue);
      if Result > 6 then
        Result := 1;
    end;
  end;

var
  nInputFill: IXMLDOMNode;
  i, ChldID, J, res_IN_ID: Integer;
  resCnt: array of Integer;
  in_fill: TStringList;
  arr: TArray<TFarmBarnItem>;
  B: Boolean;
begin
  ChldID := AItemNode.attributes.getNamedItem('id').nodeValue;

  PickNode(AItemNode, aList, 'output_fill');


  Arr := AResMap.Values.ToArray;
  SetLength(resCnt, AResMap.Count);

  for i := 0 to High(resCnt) do
    resCnt[i] := 3;

  nInputFill := AItemNode.attributes.getNamedItem('input_fill');
  if Assigned(nInputFill) then
  begin
    in_fill := TStringList.Create;
    try
      in_Fill.CommaText := nInputFill.nodeValue;
      for I := 0 to in_fill.Count - 1 do
      begin
        if not TryStrToInt(in_fill[i], res_IN_ID) then Exit;

        for J := 0 to High(Arr) do
        begin
          if Arr[J].ID <> res_IN_ID then Continue;

          resCnt[J] := resCnt[J] - 1;
          Break;
        end;{for j}
      end;{for }
    finally
      in_Fill.Free;
    end;
  end;

  B := False;
  for i := 0 to High(resCnt) do
  begin
    B := ResCnt[i] > 0;
    if B then Break;

  end;

  if not B then Exit;

  for i := 0 to High(resCnt) do
  begin
    for J := 0 to resCnt[i] - 1 do
    begin
      AddCommand('put', arr[i].Name, RoomID, ChldID, aList);
      arr[i].Count := arr[i].Count - 1;
    end;
  end;
end;

procedure TKnownElementMultiSelectable.PrepareCommands(
  const AList: TObjectList<TCachedCommand>; const AFieldNode: IXMLDOMNode;
  const ABarn: TFarmBarn);
var
  i, J: Integer;
  L: IXMLDOMNodeList;
  nChild: IXMLDOMNode;
  ResMap: TDictionary<string, TFarmBarnItem>;
  rItm: TFarmBarnItem;
begin
  FLastCommandCount := 0;
//  FLastActiveTime := GetTickCount;

  L := AFieldNode.childNodes;

  ResMap := TDictionary<string, TFarmBarnItem>.Create;
  try
    for I := 0 to FixedRes.Count - 1 do
    begin
      rItm := aBarn.GetBarnItem(FixedRes[i]);
      if not Assigned(rItm) then
        Exit;

      if rItm.Count > 0 then
        ResMap.Add(FixedRes[i], rItm);
    end;

    for I := 0 to SelectableRes.Count - 1 do
    begin
      if not SelectableRes[i].Checked then
        Continue;

      rItm := ABarn.GetBarnItem(SelectableRes[i].Name);
      if not Assigned(rItm) then
        Continue;

      if rItm.Count > 0 then
      begin
        ResMap.Add(rItm.Name, rItm);
        Break;
      end;
    end;

    if ResMap.Count < (FixedRes.Count + 1) then
      Exit;

    for I := 0 to L.length - 1 do
    begin
      nChild := L.item[i];
      for J := 0 to FabricNames.Count - 1 do
      begin
        if nChild.nodeName = FabricNames[J] then
          ProcessMultipleNode(nChild, ResMap, AList);
      end;{for j}
    end;{for i}

  finally
    ResMap.Free;
  end;
end;

{ TKnownElementOther }

constructor TKnownElementOther.Create(const AOwner: TFarmUser;
  const AName: string; const ARoomID: Integer);
begin
  inherited Create(AOwner, AName, [], [], ARoomID);
//  SleepTime := 10 * 60 * 1000;
end;

function TKnownElementOther.GetCustomizeDlgClass: TCustomizeFormClass;
begin
  Result := TdlgSelRes;
end;

function TKnownElementOther.IsHiveActive: Boolean;
var
  Itm: TKnownElementSimple;
begin
  Result := False;
  if RoomID <> c_MainRoom then Exit;

  for Itm in FOwner.Knowns do
  begin
    if not (Itm is TKnownElementHive) then Continue;

    Result := Itm.Checked;
    Exit;
  end;
end;

function TKnownElementOther.KnownNode(const ANode: IXMLDOMNode): Boolean;
const
  c_KnownFields: array[0..4] of string = ('irrigator_well', 'fertilizer', 'pick_enlarger',
    'egg_', 'snowdrop_bag');
var
  Itm: TKnownElementSimple;
  S: string;
begin
  Result := False;

  for Itm in FOwner.FKnowns do
    for S in Itm.FabricNames do
    begin
      Result := S = ANode.nodeName;
      if Result then
        Exit;
    end;

  for S in c_KnownFields do
  begin
    Result := Pos(S, string(ANode.nodeName)) = 1;
    if Result then
      Exit;
  end;
end;

procedure TKnownElementOther.PrepareCommands(
  const AList: TObjectList<TCachedCommand>; const AFieldNode: IXMLDOMNode;
  const ABarn: TFarmBarn);
var
  i: Integer;
  L: IXMLDOMNodeList;
  nChild: IXMLDOMNode;
  BIsHive: Boolean;
begin
  FLastCommandCount := 0;

  L := AFieldNode.childNodes;

  BIsHive := IsHiveActive;
  for I := 0 to L.length - 1 do
  begin
    nChild := L.item[i];

  // �������� �� ��������� ��������
    if KnownNode(nChild) then Continue;

  // �������� �� ���
    if (nChild.nodeName = 'clover') and BIsHive then
    begin
      if not Assigned(nChild.attributes.getNamedItem('pollinated')) then
        Continue;
    end;

  // ������ ����� ����������

    PickNode(nChild, aList, 'output_fill');

//    if (nChild.nodeName = 'clover') then
//      FOwner.WriteLog(nChild.nodeName + ':' + BoolToStr(BIsHive, True) + ':' +
//        BoolToStr(Assigned(nChild.attributes.getNamedItem('pollinated')), True))
//    else
//      FOwner.WriteLog(nChild.nodeName);

//    nOutputFill := nChild.attributes.getNamedItem('output_fill');
//    if Assigned(nOutputFill) and (nOutputFill.nodeValue <> '') and not Assigned(nChild.attributes.getNamedItem('auto')) then
//    begin
//      nCount := GetCompletteItemCount(nOutputFill.nodeValue);
//      for J := 0 to nCount - 1 do
//        AddCommand('pick', '', RoomID, ChldID, AList);
//    end;
  end;

end;

{ TKnownElementSoil }

constructor TKnownElementSoil.Create(const AOwner: TFarmUser;
  const AName: string; const ARoomID: Integer);
begin
  inherited Create(AOwner, AName, [], [], [], ARoomID);
//  FSleepTime := 30 * 60 * 1000;

  FPlantClasses := TStringList.Create;
  FPlantClass := 'clover';

  FPlantClasses.Add('clover');
  FPlantClasses.Add('prickle');
  FPlantClasses.Add('carrot');
  FPlantClasses.Add('potato');
  FPlantClasses.Add('blueberry');
  FPlantClasses.Add('sugarcane');
  FPlantClasses.Add('wheat');
  FPlantClasses.Add('tomato');
  FPlantClasses.Add('flax');
  FPlantClasses.Add('corn');
  FPlantClasses.Add('sunflower');
end;

destructor TKnownElementSoil.Destroy;
begin
  FPlantClasses.Free;
  inherited;
end;

function TKnownElementSoil.getOneResource: Boolean;
begin
  Result := True;
end;

procedure TKnownElementSoil.LoadSettings(const ARootNode: IXMLDOMElement);
//var
//  V: Variant;
begin
  PlantClass := VarToStr(ARootNode.getAttribute('plant_class'));
  if PlantClass = '' then
    PlantClass := FPlantClasses[0];

//  V := ARootNode.getAttribute('sleep_time');
//  if not VarIsNull(V) then
//    SleepTime := Integer(V);
end;

procedure TKnownElementSoil.SaveSettings(const ARootNode: IXMLDOMElement);
begin
  ARootNode.setAttribute('plant_class', PlantClass);
//  ARootNode.setAttribute('sleep_time', SleepTime);
end;

procedure TKnownElementSoil.PrepareCommands(
  const AList: TObjectList<TCachedCommand>; const AFieldNode: IXMLDOMNode;
  const ABarn: TFarmBarn);
var
  i, ChldID: Integer;
  L: IXMLDOMNodeList;
  nChild: IXMLDOMNode;
begin
  FLastCommandCount := 0;

  L := AFieldNode.childNodes;

  for I := 0 to L.length - 1 do
  begin
    nChild := L.item[i];

    ChldID := nChild.attributes.getNamedItem('id').nodeValue;

  // �������
    if (nChild.nodeName = 'soil') then
      AddCommand('apply', PlantClass, RoomID, ChldID, aList);
  end;
end;

{ TFarmUser }

procedure TFarmUser.CalcBaseTime;
begin
  if (GetTickCount - FBaseTickCount) < 10000  then
    FBaseTime := FBaseTickCount + 10000
  else
    FBaseTime := FBaseTickCount + (GetTickCount - FBaseTickCount);
end;

procedure TFarmUser.ClearLog;
begin
  FLog.Clear;
end;

constructor TFarmUser.Create(const AName: string;
  const AServUpdRevision: Integer; const APageControl: TPageControl);
var
  cln: TcxTreeListColumn;
begin
  FServerUpdateRevision := AServUpdRevision;
  FKnowns := TObjectList<TKnownElementSimple>.Create(True);
  FRooms := TObjectList<TFarmRoom>.Create(True);
  fThread := nil;
//  FMsgStack := TStringList.Create;
//  hMutex := CreateMutex(nil, True, nil);

  fIsInited := False;
  FPostVersion := 1;
  FNextExp := 0;

  FName := AName;

//  FUserID := AUserID;
//  FAuthKey := AAuthKey;
//  SetFlashVars(AFlashVars);

  InitRooms;
  InitKnownElements;

  FPage := TUserTabSheet.Create(nil);
  FPage.PageControl := APageControl;
  FPage.Caption := aName;
  FPage.TabVisible := False;
  TUserTabSheet(FPage).User := Self;

//  TUserTabSheet(FPage).Thread := nil;//TExecutorThread.Create(Self);

  cxEditRepository := TcxEditRepository.Create(nil);
  cxEditRepositoryCheckBoxItem := TcxEditRepositoryCheckBoxItem.Create(cxEditRepository);
  cxEditRepositoryCheckBoxItem.Properties.OnChange := PropCheckChange;
  cxEditRepositoryCheckBoxItem.Properties.Alignment := taRightJustify;

  cxEditRepositoryLabel := TcxEditRepositoryLabel.Create(cxEditRepository);

  FTree := TcxTreeList.Create(nil);
  FTree.Parent := FPage;
  FTree.Align := alLeft;
  FTree.Width := 200;
  FTree.LookAndFeel.NativeStyle := True;
  FTree.OptionsView.ColumnAutoWidth := True;
  FTree.OptionsCustomizing.ColumnMoving := False;
  FTree.OptionsCustomizing.ColumnHorzSizing := False;
  FTree.OptionsBehavior.Sorting := False;
  FTree.OptionsData.Deleting := False;
  FTree.OptionsView.ShowRoot := False;
  FTree.OnCollapsing := TreeListCollapsing;
  FTree.OnIsGroupNode := TreeListIsGroupNode;

  Cln :=FTree.CreateColumn;
  Cln.Width := 20;
  Cln.OnGetEditProperties := TreeListColumnGetEditProperties;

  Cln :=FTree.CreateColumn;
  Cln.Caption.Text := 'Name';
  Cln.Options.Editing := False;

//  Cln :=FTree.CreateColumn;
//  Cln.Caption.Text := 'Preview';
//  Cln.Options.Editing := False;
//
//  FTree.Preview.Column := Cln;
//  FTree.Preview.Place := tlppTop;
//  FTree.Preview.Visible := True;

  FLog := TMemo.Create(nil);
  FLog.Parent := FPage;
  FLog.Align := alClient;
  FLog.ScrollBars := ssBoth;
end;

destructor TFarmUser.Destroy;
begin
  if Assigned(fThread) and Assigned(TExecutorThread(fThread).FCheckThread) then
    TExecutorThread(fThread).FCheckThread.Terminate;

  Stop;

  FLog.Free;
  FTree.Free;
  FKnowns.Free;
  FRooms.Free;
  cxEditRepository.Free;
//  FMsgStack.Free;
//  CloseHandle(hMutex);

//  cxEditRepositoryCheckBoxItem.Free;
  inherited;
end;

function TFarmUser.GetDeltaTime: Integer;
begin
  Result := GetTickCount - FTimeInitedST;
end;

procedure TFarmUser.GetElementsByRoom(const ARoomID: Integer;
  const AList: TList<TKnownElementSimple>; AllElements: Boolean);
var
  El: TKnownElementSimple;
//  N: Cardinal;
begin
  AList.Clear;
//  N := GetTickCount;

  for El in FKnowns do
    if El.RoomID = ARoomID then
    begin
      if AllElements then
        AList.Add(El)
      else
        if El.Checked {and ((Integer(N - El.LastActiveTime) > El.SleepTime) {or (El.LastCommandCount > 0))} then
          AList.Add(El);
    end;
end;

//function TFarmUser.GetLog: TStrings;
//begin
//  Result := FLog.Lines;
//end;

function TFarmUser.GetUserStatQuery(const ARoomID: Integer; const ARevision: string;
  const AAsFirstRun: Boolean): string;
var
  SesKey, fSes: string;

begin
  if Assigned(fThread) then
    SesKey := TExecutorThread(fThread).FSessionKey;

  if (AAsFirstRun and (ARoomID = c_MainRoom)) or (SesKey = '') or (SesKey = 'null') then
  begin
    fSes := 'session%5Fkey=null&' +
      'fp%5Fver=11&' +
      'first%5Frequest=true&' +
      'autotest%5Fdisable%5Fdaily%5Fbonus=false&' +
      'try%5Ftake%5Ffir%5Ftree%5Fgifts=2&' +
      'from%5Fie=0&';
  end else begin
    fSes := 'session%5Fkey=' + ConvertStr(SesKey) + '&' +
//      'autotest%5Fdisable%5Fdaily%5Fbonus=false&' +
      'change%5Froom=true&';
  end;

  CalcBaseTime;

  Result := 'serv%5Fver=0&' +
    'cl%5Fver=1&' +
    'fixed%5Fobj%5Fcorr%5Fy=0&' +
    fSes +
    'server%5Fupdate%5Fversion=' + IntToStr(ServerUpdateRevision) + '&' +
    'room%5Fnumber=' + IntToStr(ARoomID) + '&' +
    'rand=' + ConvertStr(FloatToStr(Random(High(Integer)) / High(Integer))) + '&' +
    'avg%5Ffps=29&' +
    'revision=' + aRevision + '&' +
    'user%5Fid=' + UserID + '&' +
    'time%5Fpl=' + IntToStr(FBaseTime) + '&' +
    'auth%5Fkey=' + AuthKey + '&' +
    'version=' + IntToStr(FPostVersion) + '&' +
    'flash%5Fvars=' + FlashVars +
    'avg%5Fmem=345100288';
end;

procedure TFarmUser.InitKnownElements;
var
  R: TFarmRoom;
  Itm: TKnownElementSimple;
begin
  FKnowns.Add(TKnownElementHive.Create(Self, 'Honey', ['hive'], []));

  FKnowns.Add(TKnownElementSimple.Create(Self, 'Cow', ['cow_monts2', 'cow_monts4', 'cow', 'cow_holstein2', 'cow_monts4_points'], ['clover']));
  FKnowns.Add(TKnownElementSimple.Create(Self, 'Cheese',  ['cheese_machine', 'cheese_machine_level2', 'cheese_machine_real'], ['milk']));
  FKnowns.Add(TKnownElementSimple.Create(Self, 'Camel', ['camel_brown'], ['prickle']));
  FKnowns.Add(TKnownElementSimple.Create(Self, 'Rabbit', ['black_rabbit_real'], ['carrot']));
  FKnowns.Add(TKnownElementSimple.Create(Self, 'Mill', ['mill'], ['wheat']));
  FKnowns.Add(TKnownElementSimple.Create(Self, 'Sugar', ['sugar_machine'], ['sugarcane']));
  FKnowns.Add(TKnownElementSimple.Create(Self, 'Ketchup', ['ketchup_machine'], ['tomato']));
  FKnowns.Add(TKnownElementSimple.Create(Self, 'Pond', ['pond5'], ['sunflower']));

  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Perfume', ['perfume_machine'], [], ['orange', 'muscat', 'lemon']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Fur', ['factory_tannic_machine'], [], ['beaver_fur'{�����}, 'chinchilla_fur'{�������},
    'mink_fur'{�����}, 'sable_fur'{������}, 'kalan_fur'{�����}, 'fox_fur'{����}, 'polar_fox_fur'{�����},
    'raccoon_fur'{����}]));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Hats', ['factory_hats'], [], ['beaver_tanned_fur',
    'mink_tanned_fur', 'sable_tanned_fur', 'fox_tanned_fur', 'raccoon_tanned_fur']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Textile', ['textile_machine'], [], ['camel_wool_brown', 'black_angor_wool']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Textile factory', ['textile_factory'], [], ['flax']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'DriedFruits', ['dried_fruit_factory'], [], ['apple', 'black_currants', 'pear', 'banana']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Grill', ['grill_machine'], [], ['meat', 'snow_ram_meat', 'ram_meat', 'meat_goose']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Milk factory', ['milk_machine'], [], ['milk']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Meat factory', ['meat_factory'], [], ['roast', 'ram_meat', 'steak_goose']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Natural dyes factory', ['factory_natural_dyes'], [], ['onion']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Water mill', ['water_mill'], [], ['sunflower']));
  FKnowns.Add(TKnownElementSelectable.Create(Self, 'Corn meal', ['flour_mill_machine'], [], ['corn']));
//  FKnowns.Add(TKnownElementPumpkin.Create(Self, 'Pumpkin contest', ['pumpkin_contest'], []));

  FKnowns.Add(TKnownElementTimedFarm.Create(Self, 'Perl factory', ['pearl_farm', 'pearl_farm_level2'], [], ['white_pearl', 'pink_pearl', 'black_pearl']));
  FKnowns.Add(TKnownElementTimedFarm.Create(Self, 'Fish factory', ['pier_level2'], [], ['caviar']));

  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Chocolate', ['chocolate_machine'], ['cocoa'], ['milk']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Elite chocolate', ['elite_chocolate_factory'], ['chocolate'], ['cobnut', 'almond_nut']));

  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Chips', ['chips_machine'], ['potato'], ['cheese']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Jam', ['jam_machine'], ['honey'],
    ['blueberry', 'garnet', 'apple', 'orange', 'cherry', 'pineapple', 'pear']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Fur Coat', ['fur_coat_factory'], ['flax_cloth'],
    ['chinchilla_tanned_fur', 'polar_fox_tanned_fur', 'coypu_tanned_fur',
     'marten_tanned_fur', 'kalan_tanned_fur', 'otter_tanned_fur']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Ice cream', ['ice_cream_machine'], ['milk'],
    ['mango', 'banana', 'kiwi']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Desert factory', ['desserts_factory'], ['curds'],
    ['dried_black_currants', 'dried_apple', 'dried_banana', 'dried_pear']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Pizze factory', ['pizza_factory'], ['cheese', 'ketchup'],
    ['meat']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Yoghurt', ['yoghurt_machine'], ['milk'],
    ['cherry', 'blueberry']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Confectionary', ['confectionary'], ['flour', 'sugar'],
    ['apple_jam', 'orange_jam', 'blueberry_jam', 'cherry_jam', 'garnet_jam', 'pear_jam', 'pineapple_jam']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Cookies factory', ['factory_cookies_machine'], ['flour', 'milk'],
    ['dragon_fruit', 'papaya']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Canned fruit', ['canned_fruit_machine'], ['sugar'],
    ['carambola', 'plum']));
  FKnowns.Add(TKnownElementMultiSelectable.Create(Self, 'Elite cheese', ['cheese_factory'], ['cheese'],
    ['garlic', 'curds']));


  for R in FRooms do
  begin
    FKnowns.Add(TKnownElementOther.Create(Self, 'Harvest ' + R.Name, R.ID));
    FKnowns.Add(TKnownElementSoil.Create(Self, 'Plant ' + R.Name, R.ID));

    if R.ID = c_Reservation then
    begin
      FKnowns.Add(TKnownElementSimple.Create(Self, 'Pooh Swan', ['house_swans5'], ['cranberries'], R.ID));
      FKnowns.Add(TKnownElementTimedFarm.Create(Self, 'Trout', ['house_crayfish_hunter_level2'], [], ['trout_contract'], R.ID));

    end else
    if R.ID = c_MountainValey then
    begin
      FKnowns.Add(TKnownElementTimedFarm.Create(Self, 'Mineral Water', ['mountain_spring'], [], ['mountain_spring_contract4'], R.ID));
      FKnowns.Add(TKnownElementTimedFarm.Create(Self, 'Grove', ['grove'], [], ['grove_contract4'], R.ID));
      FKnowns.Add(TKnownElementTimedFarm.Create(Self, 'Coal mine', ['coalmine'], [], ['give_coal'], R.ID));


      FKnowns.Add(TKnownElementSimpleClass.Create(Self, 'Sparkling Water', ['aerated_water_factory'], ['mineral_water'], R.ID));

    end;
  end;
end;

procedure TFarmUser.InitRooms;
begin
  FRooms.Add(TFarmRoom.Create(c_MainRoom, 'Country', 60 * 60 * 1000));
  FRooms.Add(TFarmRoom.Create(c_Reservation, 'Reservation', 60 * 60 * 1000));
  FRooms.Add(TFarmRoom.Create(c_TropicIsland, 'Tropic island', 60 * 60 * 1000));
  FRooms.Add(TFarmRoom.Create(c_ZooIsland, 'Zoo island', 60 * 60 * 1000));
  FRooms.Add(TFarmRoom.Create(c_DinoIsland, 'Dino island', 60 * 60 * 1000));
  FRooms.Add(TFarmRoom.Create(c_MagicIsland, 'Magic island', 60 * 60 * 1000));
  FRooms.Add(TFarmRoom.Create(c_MountainValey, 'Mountain Valley', 60 * 60 * 1000));
end;

function TFarmUser.InitUser(aHttp: TIdHTTP): Boolean;
var
  SL: TStringList;

  function _MakePart(const AName: string): string;
  var
    S: string;
  begin
    S := SL.Values[aName];

    if (S = '') then
    begin
      if SameText(aName, 'refplace') then
        S := 'user_apps'
      else
        raise Exception.CreateFmt('Variable "%s" not found.', [aName]);
    end;

    Result := ConvertStr(AName + '=') + ConvertStr(HTTPDecode(S));
  end;

var
  P, R: TStringStream;
  actS, S, sPost, SName, sFlash: string;
  N: Integer;
  sLoginUrl: string;
begin
  R := TStringStream.Create;
  P := TStringStream.Create;
  try
    ahttp.HandleRedirects := True;
    WriteLog('Detecting login url...');

    ahttp.Get('http://odnoklassniki.ru', R);

    actS := 'http://www.odnoklassniki.ru/dk?cmd=AnonymLogin';
    S := R.DataString;
    N := Pos(actS, S);

    if N <= 0 then Exit(False);

    S := Copy(S, N, Length(S));
    N := Pos('"', S);
    if N <= 0 then Exit(False);

    Delete(S, N, Length(S));
    WriteLog('Login url detected');


    WriteLog(HTTPEncode(fLogin));
    SName := StringReplace(fLogin, '@', '%40', [rfReplaceAll]);
    sPost := 'st.redirect=&st.asr=&st.posted=set&st.email=' + SName + '&st.password=' + fPassword +
      '&st.remember=on&st.fJS=enabled&st.st.screenSize=1920+x+1080&st.st.browserSize=' +
      '508&st.st.flashVer=&button_go=%D0%92%D0%BE%D0%B9%D1%82%D0%B8';
    P.WriteString(AnsiString(sPost));
    P.Position := 0;

    ahttp.Request.Accept := 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
//    ahttp.Request.UserAgent := 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) ' +
//        'Chrome/24.0.1312.57 Safari/537.17';
    ahttp.Request.ContentType := 'application/x-www-form-urlencoded';
    ahttp.Request.AcceptEncoding := 'gzip,deflate,sdch';
//    ahttp.Request.AcceptLanguage := 'en-US,en;q=0.8';
//    ahttp.Request.AcceptCharSet := 'ISO-8859-1,utf-8;q=0.7,*;q=0.3';

    WriteLog('Try login...');
    R.Size := 0;
    ahttp.Post(S, P, R);
    P.Clear;

    if not SameText(ahttp.Response.ContentEncoding, 'gzip') then
    begin
      WriteLog('Login failed.');
      Exit(False);
    end;

    WriteLog('Login OK.');

    R.Size := 0;;

    WriteLog('Getting game variables...');
    ahttp.Get('http://www.odnoklassniki.ru/game/fermer', R);

    if SameText(ahttp.Response.ContentEncoding, 'gzip') then
    begin
      R.Position := 0;
      GZDecompressStream(R, p);
      S := P.DataString;
    end else
      S := R.DataString;

    actS := '/assets/app.html?authorized';//http://88.212.226.153/assets/app.html?
    N := Pos(ActS, S);
    if N <= 0 then
    begin
      WriteLog('Cannot detect game vars...');
      Exit(False);
    end;

    while Copy(S, N, 7) <> 'http://' do
      Dec(N);

    S := Copy(S, N, Length(S));
    N := Pos('"', S);
    if N <= 0 then
    begin
      WriteLog('Cannot detect game vars...');
      Exit(False);
    end;

    Delete(S, N, Length(S));

    WriteLog(S);
    sLoginUrl := S;

    WriteLog('Initializing user...');
    SL := TStringList.Create;
    try
      SL.Delimiter := '&';
      SL.DelimitedText := Copy(S, Pos('?', S) + 1, Length(S));

      FUserID := SL.Values['logged_user_id'];

      WriteLog('Loaded values...');
      WriteLog(FFlashVars);
      WriteLog(FAuthKey);
      WriteLog('Detected values...');

      sFlash := _MakePart('api_server') +
        _MakePart('apiconnection') +
        _MakePart('application_key') +
        _MakePart('auth_sig') +
        _MakePart('authorized') +
        _MakePart('clientLog') +
        _MakePart('first_start') +
        _MakePart('ip_geo_location') +
        _MakePart('logged_user_id') +
        _MakePart('refplace') +
        _MakePart('session_key') +
        _MakePart('session_secret_key') +
        _MakePart('web_server') + '&';

      WriteLog(SL.Values['sig']);
      WriteLog(sFlash);
{$IFDEF UseDetectEngine}
      FAuthKey := SL.Values['sig'];

      FFlashVars := sFlash;
{$ENDIF}

    finally
      SL.Free;
    end;

    if FAuthKey = '' then
    begin
      WriteLog('AuthKey detect failed.');
      Exit(False);
    end;

    if FUserID = '' then
    begin
      WriteLog('UserID detect failed.');
      Exit(False);
    end;

//    R.Size := 0;
//    ahttp.Get(sLoginUrl, R);
//    if not SameText(ahttp.Response.ContentEncoding, 'gzip') then
//      WriteLog('�����');
////
////
//    R.Size := 0;
//    ahttp.Request.Referer := sLoginUrl;
//    ahttp.Get('http://88.212.226.153/assets/iframe/js/iframe_helper.js?q=9', R);
//
//    R.Size := 0;
//    ahttp.Get('http://88.212.226.153/assets/iframe/sqbar/od.html?app_id=CBAFKBABABABABABA&bot=1', R);
//
//    R.Size := 0;
//    ahttp.Get('http://88.212.226.154/assets/loader.swf', R);
//
//
//    R.Size := 0;
//    ahttp.Get('http://88.212.226.156/crossdomain.xml', R);
//
    fIsInited := True;
    Result := True;
  finally
    R.Free;
    P.Free;
  end;
end;

function TFarmUser.IsRunning: Boolean;
begin
  Result := Assigned(fThread);
end;

function TFarmUser.PrepareExecQuery(const ARoomID: Integer; const ARev, ASes: string;
  const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn; var ACmdCount: Integer): string;
var
  k: Integer;
  Itm: TKnownElementSimple;
  Cmd: TCachedCommand;
  S: string;
  L: TObjectList<TCachedCommand>;
  RoomCmds: TList<TKnownElementSimple>;
  InsertLevelUp: Boolean;
begin
  S := '';
  Result := '';
  InsertLevelUp := False;

  L := TObjectList<TCachedCommand>.Create(True);
  RoomCmds := TList<TKnownElementSimple>.Create;
  try
    GetElementsByRoom(ARoomID, RoomCmds, False);

    for Itm in RoomCmds do
      Itm.PrepareCommands(L, aFieldNode, ABarn);

    K := 0;
    for Cmd in L do
    begin
      S := S + Cmd.AsString + '&';
      Inc(K);
      if CurExp >= NextExp then
      begin
        InsertLevelUp := True;
        Break;
      end;

      if K >= 2000 then
        Break;
    end;
  finally
    L.Free;
    RoomCmds.Free;
  end;

  if S <> '' then
  begin
    CalcBaseTime;

    if InsertLevelUp then
    begin
      S := S +
        'cached%5B' + IntToStr(K) + '%5D%5Buser%5Fid%5D=' + UserID + '&' +
        'cached%5B' + IntToStr(K) + '%5D%5Bcommand%5D=level%5Fup&' +
        'cached%5B' + IntToStr(K) + '%5D%5Broom%5Fnumber%5D=' + IntToStr(ARoomID) + '&' +
        'cached%5B' + IntToStr(K) + '%5D%5Bago%5D=0&' + //IntToStr(Random(10)) + '&' +
        'cached%5B' + IntToStr(K) + '%5D%5Bexp%5D=' + IntToStr(CurExp) + '&' +
        'cached%5B' + IntToStr(K) + '%5D%5Bvsemogutor%5Fhash%5D=0&' +
        'cached%5B' + IntToStr(K) + '%5D%5Broll%5Fcounter%5D=' + IntToStr(RollCounter) +
        'cached%5B' + IntToStr(K) + '%5D%5Bnew%5Fcalc%5D=0&';
      Inc(K);
      WriteLog('Congratulation! New level.');
    end;
    ACmdCount := K;

    Result := S +
      'rand=' + ConvertStr(FloatToStr(Random(High(Integer)) / High(Integer))) + '&' +
      'fp%5Fver=11' +
      'version=383&' +
      'room%5Fnumber=' + IntToStr(ARoomID) + '&' +
      'time%5Fpl=' + IntToStr(FBaseTime) + '&' +
      'avg%5Ffps=30&' +
//      'avg%5Fmem=318136320&' +
      'serv%5Fver=0&' +
      'revision=' + ARev + '&' +
      'server%5Fupdate%5Fversion=' + IntToStr(ServerUpdateRevision) + '&' +
      'cl%5Fver=1&' +
      'session%5Fkey=' + ConvertStr(ASes) + '&' +
      'user%5Fid=' + UserID + '&' +
      'flash%5Fvars=' + FlashVars +
      'auth%5Fkey=' + AuthKey;
  end;
end;

procedure TFarmUser.PropCheckChange(Sender: TObject);
begin
  TKnownElementSimple(FTree.FocusedNode.Data).Checked := TcxCheckbox(Sender).Checked;
end;

procedure TFarmUser.LoadSettings(const AUserNode: IXMLDOMElement);
var
  nChild, nCheck: IXMLDOMNode;
  Itm: TKnownElementSimple;
  I: Integer;
begin
  FLogin := VarToStr(AUserNode.getAttribute('login'));
  fPassword := VarToStr(AUserNode.getAttribute('password'));

  FUserID := VarToStr(AUserNode.getAttribute('user_id'));
  FAuthKey := VarToStr(AUserNode.getAttribute('auth_key'));
  FFlashVars := VarToStr(AUserNode.getAttribute('flash_vars'));

  for I := 0 to AUserNode.childNodes.length - 1 do
  begin
    nChild := AUserNode.childNodes.item[i];

    for Itm in FKnowns do
    begin
      if Itm.NSName <> nChild.nodeName then Continue;

      nCheck := nChild.attributes.getNamedItem('checked');
      Itm.Checked := Assigned(nCheck) and Boolean(nCheck.nodeValue);

      Itm.LoadSettings(nChild as IXMLDOMElement);
    end;{for itm}
  end;{for i}
end;

procedure TFarmUser.SaveSettings(const ARootNode: IXMLDOMElement);
var
  nUser, nChild: IXMLDOMElement;
  Itm: TKnownElementSimple;
begin
  nUser := ARootNode.selectSingleNode(Name) as IXMLDOMElement;
  if not Assigned(nUser) then
  begin
    nUser := ARootNode.ownerDocument.createElement(Name);
    ARootNode.appendChild(nUser);
  end;

  nUser.setAttribute('login', fLogin);
  nUser.setAttribute('password', fPassword);

{$IFNDEF UseDetectEngine}
  nUser.setAttribute('user_id', FUserID);
  nUser.setAttribute('auth_key', FAuthKey);
  nUser.setAttribute('flash_vars', FFlashVars);
{$ENDIF}

  for Itm in FKnowns do
  begin
    nChild := nUser.selectSingleNode(Itm.NSName) as IXMLDOMElement;
    if not Assigned(nChild) then
    begin
      nChild := nUser.ownerDocument.createElement(Itm.NSName);
      nUser.appendChild(nChild);
    end;

    nChild.setAttribute('checked', Itm.Checked);

    Itm.SaveSettings(nChild);
  end;
end;

procedure TFarmUser.SetFlashVars(const AVars: string);
begin
//  if UserID = '' then
//    raise Exception.Create('Required USER_ID.');

  FFlashVars := StringReplace(AVars, '$$USER_ID$$', FUserID, [rfReplaceAll]);

//  FFlashVars := Value;
  if FFlashVars[Length(FFlashVars)] <> '&' then
    FFlashVars := FFlashVars + '&';
end;

procedure TFarmUser.SetServerTime(const Value: Integer);
begin
  FServerTime := Value;
  FTimeInitedST := GetTickCount;
end;

procedure TFarmUser.ShowKnownElements;
var
  Itm: TKnownElementSimple;
  nRoom, ND: TcxTreeListNode;
  R: TFarmRoom;
  L: TObjectList<TKnownElementSimple>;
begin
  L := TObjectList<TKnownElementSimple>.Create(False);
  try
    for R in FRooms do
    begin
      nRoom := FTree.Add;
      nRoom.Values[0] := R.Name;

      L.Clear;
      GetElementsByRoom(R.ID, L, True);

      for Itm in L do
      begin
//        ND := FTree.Add;//
        ND := nRoom.AddChild;

        ND.Values[0] := Itm.Checked;
        ND.Values[1] := Itm.Name;
//        ND.Values[2] := R.Name;
        ND.Data := Itm;
      end;
      nRoom.Expanded := True;
    end;
  finally
    L.Free;
  end;
end;

procedure TFarmUser.Start;
begin
  if Assigned(fThread) then Exit;

  fThread := TExecutorThread.Create(Self);
  fThread.OnTerminate := ThreadTerminated;
  WriteLog('Started...');
  FThread.Suspended := False;
end;

procedure TFarmUser.Stop;
begin
  if Assigned(fThread) then
  begin
    WriteLog('Stopping...');
//    TExecutorThread(fThread).FIdHTTP.Disconnect;
    fThread.Terminate;
    fThread := nil;
  end;
end;

procedure TFarmUser.ThreadTerminated(Sender: TObject);
var
  I: Integer;
begin
  WriteLog('Stoped');
  fThread := nil;

  for I := 0 to FRooms.Count - 1 do
    FRooms.Items[i].LastActiveTime := 0;
end;

procedure TFarmUser.TreeListCollapsing(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode; var Allow: Boolean);
begin
  Allow := False;
end;

procedure TFarmUser.TreeListColumnGetEditProperties(Sender: TcxTreeListColumn;
  ANode: TcxTreeListNode; var EditProperties: TcxCustomEditProperties);
begin
  if ANode.Level = 0 then
    EditProperties := cxEditRepositoryLabel.Properties
  else
    EditProperties := cxEditRepositoryCheckBoxItem.Properties;
end;

procedure TFarmUser.TreeListIsGroupNode(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode; var IsGroup: Boolean);
begin
  IsGroup := ANode.Level = 0;
end;

procedure TFarmUser.WriteLog(const AMsg: string);
begin
  FLog.Lines.Insert(0, TimeToStr(Time) + ': ' + AMsg);
end;

{ TExecutorThread }

constructor TExecutorThread.Create(const AUser: TFarmUser);
begin
  inherited Create(True);
  FreeOnTerminate := True;

  fCurServerNo := Random(High(cFarmServers));;
  FSessionKey := '';
  FPostErrorCount := 0;
  fRevision := '';

  FIdHTTP := nil;
  RecreateHTTP(False);
  FUser := AUser;
end;

procedure TExecutorThread.Execute;
var
  sGetStat, sPostData: string;
  Res: TStringStream;
  ResXML: IXMLDOMDocument3;
  nCountry, nField: IXMLDOMNode;
  fBarn: TFarmBarn;
  cmdCount, rn, SleepTime: Integer;
  CurRoom: TFarmRoom;
  RoomEl: TList<TKnownElementSimple>;
  FNextRequestTime: TDateTime;
  AsFirst, FFirstReset: Boolean;
  et: TTime;
//  PrevTime: Cardinal;
begin
  CoInitializeEx(nil, COINIT_APARTMENTTHREADED);

//  Sleep(10*60*1000);

  FUser.fIsInited := True;
//  if not FUser.fIsInited then
//  begin
//    try
//      FUser.InitUser(FIdHTTP);
//    except
//      on E: Exception do
//        WriteLog(E.Message);
//    end;
//  end;

  DetectRevision;
  rn := 0;
  FUSer.PostVersion := 1;

  FNextRequestTime := IncHour(Now, -1);
  FFirstReset := True;

  Res := TStringStream.Create;
  fBarn := TFarmBarn.Create;
  RoomEl := TList<TKnownElementSimple>.Create;
  ResXML := CoDOMDocument60.Create;
  try
    while not Terminated do
    begin
      try
        if not FUser.fIsInited then
          Continue;

        //if (FPostErrorCount > 0) or (fRevision = '') then
          DetectRevision;
        if fRevision = '' then
          Continue;

        if isSiestaTime(et) then begin
          FSyncMsg := Format('Siesta to %s...', [TimeToStr(et)]);
          Synchronize(SyncWriteLog);
          Pause(SecondsBetween(Time, et));
        end;

      // ��������� ����������
        for CurRoom in FUser.Rooms do
        begin
          if Terminated then Exit;

          if (GetTickCount - CurRoom.LastActiveTime) < (60 * 1000) then
            Continue;

          if (CurRoom.LastCmdCount <= 0) and (Cardinal(CurRoom.SleepTime) > (GetTickCount - CurRoom.LastActiveTime)) then
            Continue;
          if Terminated then Exit;

          fUser.GetElementsByRoom(CurRoom.ID, RoomEl, False);
          if RoomEl.Count <= 0 then
            Continue;
          if Terminated then Exit;

          Res.Clear;

          AsFirst := False;
          if CurRoom.ID = c_MainRoom then
          begin
            AsFirst := CompareDateTime(FNextRequestTime, Now) = LessThanValue;
            if AsFirst then
            begin
              if (not IsNight) and (not FFirstReset) and ((GetTickCount - CurRoom.LastActiveTime) < (5 * 60 * 1000)) then
              begin
                SleepTime := 15 + Random(30);
                FSyncMsg := 'Sleep for ' + IntToStr(SleepTime) + ' minute';
                Synchronize(SyncWriteLog);
                Pause(SleepTime * 60);
              end;
              FFirstReset := False;

              FNextRequestTime := IncMinute(Now, 90 + Random(90));
              Rn := 0;

              FSyncMsg := 'Reset query status (first request). Next time: ' + DateTimeToStr(FNextRequestTime);
              Synchronize(SyncWriteLog);
            end;
          end;

          FSyncMsg := 'Get info for ' + CurRoom.Name + '...';
          Synchronize(SyncWriteLog);

          sGetStat := FUser.GetUserStatQuery(CurRoom.ID, fRevision, AsFirst);
          if not PostData('/get_user_stat?uid=' + FUser.UserID + '&crev=' + fRevision + '&rn=' + IntToStr(rn), sGetStat, Res) then
            Continue;
          if Terminated then Exit;
          Inc(rn);
          FUser.PostVersion := FUser.PostVersion + 1;

          FSyncMsg := 'Analizing...';
          Synchronize(SyncWriteLog);

        // ������ ��������
          ResXML.loadXML(Res.DataString);
          if Terminated then Exit;

          if not InitVariable(ResXML, nCountry, nField, True) then
            Continue;
          if Terminated then Exit;

          FBarn.LoadBarn(nCountry);
          if Terminated then Exit;

        // ���������� �������
          cmdCount := 0;
          sPostData := FUser.PrepareExecQuery(CurRoom.ID, fRevision, FSessionKey, nField, fBarn, cmdCount);
          if Terminated then Exit;

        // ������� �������
          if sPostData <> '' then
          begin
            FSyncMsg := Format('Posting %d commands...', [cmdCount]);
            Synchronize(SyncWriteLog);

            Res.Clear;
            PostData('/check_and_perform?uid=' + FUser.UserID + '&crev=' + fRevision + '&rn=' + IntToStr(rn), sPostData, Res);
            FSyncMsg := 'Posted...';
            Synchronize(SyncWriteLog);

            Inc(rn);

            if Terminated then Exit;
            ResXML.loadXML(Res.DataString);
            if Terminated then Exit;
            InitVariable(ResXML, nCountry, nField, False);

            if Terminated then Exit;
          end else begin
            FSyncMsg := 'Nothing to do...';
            Synchronize(SyncWriteLog);
          end;

          CurRoom.LastActiveTime := GetTickCount;
          CurRoom.LastCmdCount := cmdCount;
        end;

        Pause(1);

//        if bPosted then
//        begin
//          SleepDelta := 15 * 100;
//          FZeroPostCount := 0;
//        end else begin
//          Inc(FZeroPostCount);
//          if FZeroPostCount > 10 then
//            SleepDelta := 2 * 60 * 100
//          else
//          if FZeroPostCount > 20 then
//            SleepDelta := 5 * 60 * 100;
//
//          if FZeroPostCount > 1000 then
//            FZeroPostCount := 21;
//        end;
      finally
      // ������
        if not Terminated then
          Pause(5);
      end;

    end;
  finally
    Res.Free;
    fBarn.Free;
    RoomEl.Free;
    nCountry := nil;
    nField := nil;
    ResXML := nil;
  end;
end;

function TExecutorThread.GetCurServer: string;
var
  OldSrv: string;
begin
  if FPostErrorCount >= 5 then
  begin
    OldSrv := cFarmServers[fCurServerNo];

    inc(fCurServerNo);
    if fCurServerNo >= Length(cFarmServers) then
      fCurServerNo := 0;
    FPostErrorCount := 0;

    FSyncMsg := Format('Switch server "%s" -> "%s"', [OldSrv, cFarmServers[fCurServerNo]]);
    Synchronize(SyncWriteLog);
  end;
  Result := cFarmServers[fCurServerNo];
end;

function TExecutorThread.InitVariable(const AXML: IXMLDOMDocument3;
  var aCountry, aField: IXMLDOMNode; aInitField: Boolean): Boolean;
var
  nChild: IXMLDOMNode;
begin
  aCountry := AXML.selectSingleNode('country');
  if not Assigned(aCountry) then
  begin
    WriteLog('node "country" not found');
    Exit(False);
  end;

  nChild := aCountry.attributes.getNamedItem('exp');
  if Assigned(nChild) then
    FUser.CurExp := nChild.nodeValue;

  nChild := aCountry.attributes.getNamedItem('next_level_exp');
  if Assigned(nChild) then
    FUser.NextExp := nChild.nodeValue;

  nChild := aCountry.attributes.getNamedItem('session_key');
  if Assigned(nChild) then
    FSessionKey := nChild.nodeValue;

  nChild := aCountry.attributes.getNamedItem('roll_counter');
  if Assigned(nChild) then
    FUser.RollCounter := nChild.nodeValue;

  nChild := aCountry.attributes.getNamedItem('server_time');
  if Assigned(nChild) then
    FUser.ServerTime := nChild.nodeValue;

  aField := nil;
  if aInitField then
  begin
    aField := aCountry.selectSingleNode('field');
    if not Assigned(aField) then
    begin
      WriteLog('node "field" not found');
      Exit(False);
    end;
  end;

  Result := True;
end;

function TExecutorThread.IsNight: Boolean;
begin
  Result := ((CompareTime(Time, EncodeTime(23, 00, 00, 00)) = GreaterThanValue) and
    (CompareTime(Time, EncodeTime(6, 00, 00, 00)) = LessThanValue));
end;

function TExecutorThread.IsSiestaTime(var AEndTime: TTime): Boolean;
begin
  if FUser.IgnoreSiesta then
    Exit(False);

  aEndTime := EncodeTime(11, 00, 00, 00);
  Result := ((CompareTime(Time, EncodeTime(8, 30, 00, 00)) = GreaterThanValue) and
    (CompareTime(Time, AEndTime) = LessThanValue));
  if Result then Exit;

  aEndTime := EncodeTime(14, 00, 00, 00);
  Result := ((CompareTime(Time, EncodeTime(12, 00, 00, 00)) = GreaterThanValue) and
    (CompareTime(Time, AEndTime) = LessThanValue));
  if Result then Exit;

  aEndTime := EncodeTime(16, 00, 00, 00);
  Result := ((CompareTime(Time, EncodeTime(14, 30, 00, 00)) = GreaterThanValue) and
    (CompareTime(Time, AEndTime) = LessThanValue));
  if Result then Exit;

  aEndTime := EncodeTime(18, 00, 00, 00);
  Result := ((CompareTime(Time, EncodeTime(17, 00, 00, 00)) = GreaterThanValue) and
    (CompareTime(Time, AEndTime) = LessThanValue));
  if Result then Exit;

  aEndTime := EncodeTime(20, 00, 00, 00);
  Result := ((CompareTime(Time, EncodeTime(19, 00, 00, 00)) = GreaterThanValue) and
    (CompareTime(Time, AEndTime) = LessThanValue));
end;

procedure TExecutorThread.Pause(aSec: Integer);
var
  i: Integer;
  K: Integer;
begin
  K := aSec * 100;
  for I := 1 to K do
  begin
    if FUser.IgnoreSiesta then Break;
    
    if Terminated then Break;

    Sleep(10);
  end;
end;

function TExecutorThread.PostData(const AUrl, AData: string; const ARes: TStringStream): Boolean;
var
  MemS: TMemoryStream;
  S: AnsiString;
  header: TGZHeader;
begin
  FCheckThread := THTTPUnfreez.Create(Self);
  FCheckThread.Suspended := False;

  Result := False;
  MemS := TMemoryStream.Create;
  try
    try
      S := AnsiString(aData);
      MemS.Write(S[1], Length(S));
      MemS.Position := 0;
//      FIdHTTP.Request.Clear;
      FIdHTTP.Request.Accept := '*/*';
      FIdHTTP.Request.AcceptEncoding := 'gzip,deflate,sdch';
//      FIdHTTP.Request.AcceptCharSet := 'ISO-8859-1,utf-8;q=0.7,*;q=0.3';
//      FIdHTTP.Request.AcceptLanguage := 'en-US,en;q=0.8';
//      FIdHTTP.Request.Referer := 'http://88.212.226.154/assets/loader.swf';
//      FIdHttp.Request.ContentType := 'application/x-www-form-urlencoded';
//
//      FIdHttp.Response.Clear;
//      FIdHttp.Response.ResponseText := '';

//      FIdHttp.HandleRedirects := False;
      FIdHTTP.Response.Clear;
      FIdHttp.Post(GetCurServer + AUrl, MemS, ARes);

      if (FIdHTTP.Response.ContentEncoding = 'gzip') then
      begin

        MemS.Clear;
        aRes.Position := 0;

        aRes.Read(header, SizeOf(TGZHeader));
        if (header.Id1 = $1F) and (header.Id2 = $8B) then
        begin
          aRes.Position := 0;
          MemS.CopyFrom(aRes, aRes.Size);
          aRes.Size := 0;
          MemS.Position := 0;

          GZDecompressStream(MemS, aRes);
        end;
      end;

      FPostErrorCount := 0;
      Result := True;
    except
      on E: Exception do
      begin
        FSyncMsg := E.Message;
        Synchronize(SyncWriteLog);
        Inc(FPostErrorCount);

        if E is EIdSocketError then
        begin
          FSyncMsg := 'Recreate HTTP. Waiting 30 seconds...';
          Synchronize(SyncWriteLog);
          RecreateHTTP(True);
        end;

        Exit;
      end;
    end;
  finally
    MemS.Free;
    FCheckThread := nil;
  end;
end;

procedure TExecutorThread.RecreateHTTP;
begin
  if Assigned(FIdHTTP) then
  begin
    FreeAndNil(FIdHTTP);
    if NeedSleep then
      Pause(30);
  end;

  FIdHTTP := TIdHTTP.Create(nil);
  FIdHTTP.HandleRedirects := True;
  FIdHTTP.AllowCookies := True;
  FIdHTTP.ConnectTimeout := 3 * 60 * 1000;
  FIdHTTP.ReadTimeout := 2 * 60 * 1000;
//hoInProcessAuth, hoKeepOrigProtocol, hoForceEncodeParams,
//    hoNonSSLProxyUseConnectVerb, hoNoParseMetaHTTPEquiv, hoWaitForUnexpectedData
  FIdHTTP.HTTPOptions := [hoKeepOrigProtocol, hoForceEncodeParams];
end;

procedure TExecutorThread.SyncWriteLog;
begin
  WriteLog(FSyncMsg);
end;

//procedure TExecutorThread.SyncWriteLog2(const AMsg: string);
//begin
//  WaitForSingleObject(hMutex, INFINITE);
//  try
//    FUser.MsgStack.Add(AMsg);  //WriteLog(AMsg)
//  finally
//    ReleaseMutex(hMutex);
//  end;
//end;

procedure TExecutorThread.WriteLog(const AMsg: string);
begin
  FUser.WriteLog(AMsg);
end;

destructor TExecutorThread.Destroy;
begin
//  if Assigned(FCheckThread) then
//    FCheckThread.Terminate;

  FIdHTTP.Free;
  inherited;
end;

procedure TExecutorThread.DetectRevision;
var
  R: TMemoryStream;
  S: AnsiString;
begin
  R := TMemoryStream.Create;
  try
    try
      FIdHTTP.Request.AcceptEncoding := '';
      FIdHTTP.Get(GetCurServer + '/REVISION.txt', R);
      FIdHTTP.Request.AcceptEncoding := 'gzip,deflate,sdch';
    except
      on E: EIdSocketError do
      begin
        fRevision := '';
        FSyncMsg := 'Recreate HTTP. Waiting 30 seconds...';
        Synchronize(SyncWriteLog);
        RecreateHTTP(True);
      end;
      on E: Exception do
      begin
        fRevision := '';
        FSyncMsg := E.Message;
        Synchronize(SyncWriteLog);
        Inc(FPostErrorCount);
        Exit;
      end;
    end;

    SetLength(S, 6);
    R.Position := 0;
    R.Read(S[1], 6);

    if (fRevision <> '') and (fRevision <> string(S)) then
    begin
      FSyncMsg := 'New revision was found. Sleeping 15 minumtes...';
      Synchronize(SyncWriteLog);

      Pause(15 * 60);
    end;

    fRevision := string(S);
  finally
    R.Free;
  end;
end;


{ TCachedCommand }

constructor TCachedCommand.Create(const AOwner: TObjectList<TCachedCommand>; const aUser: TFarmUser);
begin
  FSecondID := 0;
  FItemClass := '';
  FOwner := AOwner;
  FUser := AUSer;
end;

function TCachedCommand.AsString: string;
var
  N: Integer;
begin
  N := FOwner.IndexOf(Self);

  Result := 'cached%5B' + IntToStr(N) + '%5D%5Bx%5D=0&' +
    'cached%5B' + IntToStr(N) + '%5D%5Bcommand%5D=' + ConvertStr(FCommand) + '&' +
    'cached%5B' + IntToStr(N) + '%5D%5Bago%5D=0&' + //IntToStr(Random(10)) + '&' +
    'cached%5B' + IntToStr(N) + '%5D%5Buser%5Fid%5D=' + FUser.UserID + '&' +
    'cached%5B' + IntToStr(N) + '%5D%5Bexp%5D=' + IntToStr(FUser.CurExp) + '&' +
    'cached%5B' + IntToStr(N) + '%5D%5Bitem%5Fid%5D=' + IntToStr(FItemID) + '&' +
    'cached%5B' + IntToStr(N) + '%5D%5Broom%5Fnumber%5D=' + IntToStr(FRoom) + '&' +
    'cached%5B' + IntToStr(N) + '%5D%5Bvsemogutor%5Fhash%5D=0&' +
    'cached%5B' + IntToStr(N) + '%5D%5Bfixed%5Fobj%5Fcorr%5Fy%5D=0&' +
    'cached%5B' + IntToStr(N) + '%5D%5Buxtime%5D=' + IntToStr(FUser.ServerTime + FUser.GetDeltaTime) + '&' +
    'cached%5B' + IntToStr(N) + '%5D%5By%5D=0&';

  if FSecondID <> 0 then
    Result := Result + 'cached%5B' + IntToStr(N) + '%5D%5Bsecond%5Fitem%5Fid%5D=' + IntToStr(FSecondID) + '&';
  if FItemClass <> '' then
    Result := Result + 'cached%5B' + IntToStr(N) + '%5D%5Bklass%5D=' + FItemClass + '&';

  Result := Result +
    'cached%5B' + IntToStr(N) + '%5D%5Broll%5Fcounter%5D=' + IntToStr(fUser.RollCounter);
  fUser.RollCounter := fUser.RollCounter + 1;

  if (FCommand = 'put') or (FCommand = 'apply') then
    FUser.CurExp := FUser.CurExp + 1;
end;

{ TUserTabSheet }

destructor TUserTabSheet.Destroy;
begin
  if Assigned(FUser) then
    FUser.Free;

//  if Assigned(FThread) then
//  begin
//    FThread.Terminate;
//    FThread.Free;
//  end;

  inherited;
end;

{ TFarmRoom }

constructor TFarmRoom.Create(AID: Integer; const AName: string;
  ASleepTime: Integer);
begin
  FID := AID;
  FName := AName;
  FSleepTime := ASleepTime;
  FLastActiveTime := -60 * 60 * 1000;
  FLastCmdCount := 0;
end;

{ TKnownElementHive }

procedure TKnownElementHive.PrepareCommands(
  const AList: TObjectList<FarmBotClasses.TCachedCommand>;
  const AFieldNode: IXMLDOMNode; const ABarn: TFarmBarn);
var
  i, J, ChldID, CloverID: Integer;
  L, LClover: IXMLDOMNodeList;
  nChild, nClover: IXMLDOMNode;
  ProcessedIDs: TList<Integer>;
begin
  FLastCommandCount := 0;
//  FLastActiveTime := GetTickCount;

  L := AFieldNode.selectNodes('hive');
  if not Assigned(L) then
    Exit;


  ProcessedIDs := TList<Integer>.Create;
  LClover := AFieldNode.selectNodes('clover');
  try
    for I := 0 to L.length - 1 do
    begin
      nChild := L.item[i];
      PickNode(nChild, aList, 'output_fill');

      for J := 0 to LClover.length - 1 do
      begin
        nClover := LClover.item[J];

        if not Assigned(nClover.attributes.getNamedItem('output_fill')) then
          Continue;

        if Assigned(nClover.attributes.getNamedItem('pollinated')) then
          Continue;

        CloverID := nClover.attributes.getNamedItem('id').nodeValue;

        if ProcessedIDs.IndexOf(CloverID) >= 0 then
          Continue;

        ChldID := nChild.attributes.getNamedItem('id').nodeValue;

        AddCommand('bee_pollinate', '', RoomID,  ChldID, CloverID, AList);
        ProcessedIDs.Add(CloverID);
        Break;
      end;
    end;
  finally
    L := nil;
    LClover := nil;
    ProcessedIDs.Free;
  end;
end;

{ TKnownElementPumpkin }

procedure TKnownElementPumpkin.ProcessSimpleNode(const AItemNode: IXMLDOMNode;
  const ARes: TFarmBarnItem; const aList: TObjectList<TCachedCommand>;
  const ABarn: TFarmBarn);
var
  ChldID: Integer;
  nInputFill: IXMLDOMNode;
begin
  ChldID := AItemNode.attributes.getNamedItem('id').nodeValue;

  nInputFill := AItemNode.attributes.getNamedItem('watering_wait');
  if not Assigned(nInputFill) then Exit;

  if VarToStr(nInputFill.nodeValue) = '0' then
    AddCommand('put', '', RoomID, ChldID, aList)
end;

{ THTTPUnfreez }

constructor THTTPUnfreez.Create(const AThread: TExecutorThread);
begin
  inherited Create(True);

  FreeOnTerminate := True;
  fUser := AThread.FUser;
end;

procedure THTTPUnfreez.Execute;
var
  N: Cardinal;
begin
  N := 3 * 60 * 1000;
  Sleep(N);
//  FUser.WriteLog('Che�king...');
  if Terminated then Exit;

//  FUser.WriteLog('Is user have thread?');
  if not Assigned(fUser.fThread) then Exit;

//  FUser.WriteLog('Is user have this check thread?');
  if (TExecutorThread(fUser.fThread).FCheckThread <> Self) then Exit;

  FUser.WriteLog('-------->Freezing HTTP. Stopping thread.');
  if Terminated then Exit;

//  FUser.WriteLog('Call stop.');
  FUser.Stop;
//  if Terminated then Exit;

//  FUser.WriteLog('Sleep.');
  Sleep(5 * 1000);
//  if Terminated then Exit;

//  FUser.WriteLog('Call start.');
  FUser.Start;
end;

end.

