program FarmBot;

uses
  Vcl.Forms,
  _wndMain in '_wndMain.pas' {wndFarmBot},
  FarmBotClasses in 'FarmBotClasses.pas',
  _dlgSelectableResources in '_dlgSelectableResources.pas' {dlgSelRes};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TwndFarmBot, wndFarmBot);
  Application.Run;
end.
