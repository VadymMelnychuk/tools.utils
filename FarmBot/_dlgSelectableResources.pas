unit _dlgSelectableResources;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxCheckBox, cxTextEdit,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, Vcl.StdCtrls,
  FarmBotClasses, cxRadioGroup, cxCheckGroup, Vcl.Samples.Spin;

type
  TdlgSelRes = class(TCustomizeForm)
    btnOK: TButton;
    btnCancel: TButton;
    tlRes: TcxTreeList;
    clnCheck: TcxTreeListColumn;
    clnName: TcxTreeListColumn;
    Label1: TLabel;
    seSleepTime: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure clnCheckPropertiesChange(Sender: TObject);
  private
    FOnlyOne: Boolean;
  public
    procedure ItemToForm(const AItem: TKnownElementSimple); override;
    procedure FormToItem(const AItem: TKnownElementSimple); override;
  end;

implementation

{$R *.dfm}

{ TdlgSelRes }

procedure TdlgSelRes.clnCheckPropertiesChange(Sender: TObject);
var
  i: Integer;
  ND: TcxTreeListNode;
begin
  if not FOnlyOne then Exit;

  if not TcxCheckBox(Sender).Checked then
  begin
    tlRes.FocusedNode.Values[0] := True;
    TcxCheckBox(Sender).Checked := True;
  end;

  for i := 0 to tlRes.Count - 1 do
  begin
    ND := tlRes.Items[i];
    if ND <> tlRes.FocusedNode then
      ND.Values[0] := False;
  end;
end;

procedure TdlgSelRes.FormCreate(Sender: TObject);
begin
  FOnlyOne := False;
end;

procedure TdlgSelRes.FormToItem(const AItem: TKnownElementSimple);
var
  CurI: TSelectableRes;
  ND: TcxTreeListNode;
  I: Integer;
  S: string;
begin
  if AItem is TKnownElementSoil then
  begin
    for I := 0 to tlRes.Count - 1 do
    begin
      ND := tlRes.Items[i];
      if Boolean(ND.Values[0]) then
        TKnownElementSoil(aITem).PlantClass := ND.Values[1];
    end;
  end else

  if AItem is TKnownElementSelectable then
  begin
    for I := 0 to tlRes.Count - 1 do
    begin
      ND := tlRes.Items[i];
      S := ND.Values[1];

      for CurI in TKnownElementSelectable(AITem).SelectableRes do
      begin
        if CurI.Name <> S then Continue;

        CurI.Checked := Boolean(ND.Values[0]);
        Break;
      end;
    end;
  end;

//  AItem.SleepTime  := seSleepTime.Value * 1000;
end;

procedure TdlgSelRes.ItemToForm(const AItem: TKnownElementSimple);
var
  CurI: TSelectableRes;
  S: string;
  ND: TcxTreeListNode;
begin
  FOnlyOne := AItem.OneResource;

  if (AItem is TKnownElementSoil) then
  begin

    for S in TKnownElementSoil(AITem).PlantClasses do
    begin
      ND := tlRes.Add;
      ND.Values[0] := TKnownElementSoil(AITem).PlantClass = S;
      ND.Values[1] := S;
    end;
  end else

  if AItem is TKnownElementSelectable then
  begin
    for CurI in TKnownElementSelectable(AITem).SelectableRes do
    begin
      ND := tlRes.Add;
      ND.Values[0] := CurI.Checked;
      ND.Values[1] := CurI.Name;
    end;
  end;

//  seSleepTime.Value := Round(AItem.SleepTime / 1000);
end;

end.
