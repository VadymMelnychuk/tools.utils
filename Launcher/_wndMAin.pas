unit _wndMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, ImgList;

type
  TwndMain = class(TForm)
    TrayIcon: TTrayIcon;
    PopupMenu: TPopupMenu;
    Des_ImageList: TImageList;
    ImageList: TImageList;
    procedure miExitClick(Sender: TObject);
    procedure miReloadClick(Sender: TObject);
    procedure TrayIconClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEventsMinimize(Sender: TObject);
    procedure ApplicationEventsRestore(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Links: TStringList;
    procedure QuickLaunchClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  wndMain: TwndMain;

implementation

{$R *.dfm}

uses
  CommCtrl, ShellApi, ShlObj,
  SimpleXML;

type
  TabfWindowsVersion = (wvUnknown, wvWin95, wvWin95OSR2, wvWin98, wvWin98SE,
    wvWinNT3, wvWinNT4, wvWin2000, wvWinME, wvWinXP);
  TabfNtProductType = (ptUnknown, ptWorkStation, ptServer, ptAdvancedServer);

const
  IsWin95    : Boolean = False;
  IsWin95OSR2: Boolean = False;
  IsWin98    : Boolean = False;
  IsWin98SE  : Boolean = False;
  IsWinME    : Boolean = False;
  IsWinNT    : Boolean = False;
  IsWinNT3   : Boolean = False;
  IsWinNT4   : Boolean = False;
  IsWin2K    : Boolean = False;
  IsWinXP    : Boolean = False;

  IsWin98orHigher  : Boolean = False;
  IsWin2000orHigher: Boolean = False;
  IsWinXPorHigher  : Boolean = False;

const
  c_CmdFile = 'Commands.xml';


procedure abfApplicationTaskBarButtonShow;
begin
  with Application do
    if IsWindow(Handle) then ShowWindow(Handle, SW_SHOWNORMAL{SW_RESTORE});
end;

//------------------------------------------------------------------------------
// Removes application's button from the taskbar.
// Date: 09/01/2000

procedure abfApplicationTaskBarButtonHide;
begin
  with Application do
    if IsWindow(Handle) and IsWindowVisible(Handle) then
      ShowWindow(Handle, SW_HIDE);
end;


procedure TwndMain.ApplicationEventsMinimize(Sender: TObject);
begin
  Hide;
  abfApplicationTaskBarButtonHide;
end;

function abfGetWindowsVersion: TabfWindowsVersion;
const
  BuildNumbers: array [TabfWindowsVersion] of Integer = (0, 0, 1111{67109975},
    1998{67766222}, 2222{67766446}, 0, 1381, 2195, 3000, 2600);
var
  Build: Integer;
begin
  Result := wvUnknown;
  Build := 0;

  case Win32Platform of
    VER_PLATFORM_WIN32_WINDOWS:
      begin
        Build := (Win32BuildNumber and $0000FFFF); // For Win9x
        case Win32MinorVersion of
          0:  if Win32CSDVersion = 'B' then Result := wvWin95OSR2 else Result := wvWin95;
          10: if Win32CSDVersion = 'A' then Result := wvWin98SE   else Result := wvWin98;
          90: Result := wvWinME;
        end;
        IsWin98orHigher := (Result <> wvWin95) and (Result <> wvWin95OSR2);
      end;

    VER_PLATFORM_WIN32_NT:
      begin
        Build := Win32BuildNumber; // For WinNT
        case Win32MajorVersion of
          3: Result := wvWinNT3;
          4: Result := wvWinNT4;
          5:
            if (Win32MinorVersion >= 1) then
              Result := wvWinXP // No ";" here!!!
            else
              Result := wvWin2000;
        end;
        IsWin2000orHigher := (Result <> wvWinNT3) and (Result <> wvWinNT4);
        IsWinXPorHigher   := (Result <> wvWinNT3) and (Result <> wvWinNT4) and
          (Result <> wvWin2000)
      end;
  end;{case Win32Platform of}

  if (BuildNumbers[Result] <> 0) and
    (Build <> BuildNumbers[Result]) then Result := wvUnknown;
end;{function abfGetWindowsVersion}


procedure TwndMain.ApplicationEventsRestore(Sender: TObject);
begin
  abfApplicationTaskBarButtonShow;
end;

procedure TwndMain.FormCreate(Sender: TObject);
const
  Mask: array[Boolean] of Longint = (0, ILC_MASK);
var
  SFld: string;
  Flags: Cardinal;
begin
  abfGetWindowsVersion;

  Links := TStringList.Create;
//  Application.OnMinimize := ApplicationEventsMinimize;
//  Application.OnRestore := ApplicationEventsRestore;

  if IsWinXPorHigher then
    Flags := ILC_COLOR32
  else
    Flags := ILC_COLORDDB;

  Flags := Flags or Mask[Des_ImageList.Masked];
  ImageList.Handle := ImageList_Create(16, 16, Flags, 4, 4);

  miReloadClick(nil);
end;

procedure TwndMain.FormDestroy(Sender: TObject);
begin
  Links.Free;
end;

procedure TwndMain.miExitClick(Sender: TObject);
begin
  Close;
end;

procedure TwndMain.miReloadClick(Sender: TObject);

  procedure _PopulateItems(const ARoot: IXMLNode; ARootMenu: TMenuItem);
  var
    MI: TMenuItem;
    Nodes: IXmlNodeList;
    I, N, Flags: Integer;
    S: String;
    Node: IXMLNode;
    FileInfo: TSHFileInfo;
  begin
    Nodes := ARoot.SelectNodes('FOLDER');
    for I := 0 to Nodes.Count - 1 do
    begin
      Node := Nodes.Item[i];
      MI := TMenuItem.Create(Self);
      MI.Caption := Node.GetAttr('CAPTION');
      MI.ImageIndex := 1;
      ARootMenu.Add(MI);

      _PopulateItems(Nodes.Item[i], MI);
    end;{for i}

    Nodes := ARoot.SelectNodes('ITEM');
    for I := 0 to Nodes.Count - 1 do
    begin
      Node := Nodes.Item[i];
      MI := TMenuItem.Create(Self);
      ARootMenu.Add(MI);

      MI.Caption := Node.GetAttr('CAPTION');
      MI.OnClick := QuickLaunchClick;
      S := Node.GetAttr('FILE_NAME');
      if S <> '' then
      begin
        N := Links.Add(S);
        MI.Tag := N;
      end else
        MI.Tag := -1;

      if Node.AttrExists('ICON') then
        S := Node.GetAttr('ICON');

      if FileExists(S) then
      begin
        Flags := SHGFI_ICON or SHGFI_SMALLICON;
        SHGetFileInfo(PChar(S), 0, FileInfo, SizeOf(FileInfo), Flags);
        N := ImageList_AddIcon(ImageList.Handle, FileInfo.hIcon);
        if N >= 0 then
          MI.ImageIndex := N;
      end;

      _PopulateItems(Nodes.Item[i], MI);
    end;{for i}
  end;

var
  SFld: string;
  Doc: IXmlDocument;
  Root: IXmlNode;
  MI: TMenuItem;
  Flags: Cardinal;
  HIc: HICON;
  I: Integer;
begin
  PopupMenu.Items.Clear;
  ImageList.Clear;


  for I := 0 to Des_ImageList.Count - 1 do
  begin
    HIc := ImageList_GetIcon(Des_ImageList.Handle, i, ILD_NORMAL);
    if HIc <> 0 then
    begin
      ImageList_AddIcon(ImageList.Handle, Hic);
      DestroyIcon(HIc);
    end;
  end;

  SFld := ExtractFilePath(Application.ExeName);
  if not FileExists(SFld + c_CmdFile) then Exit;

  Doc := CreateXmlDocument;
  try
    Doc.Load(SFld + c_CmdFile);
    Root := Doc.DocumentElement;

    if Assigned(Root) then
    begin
      _PopulateItems(Root, PopupMenu.Items);
    end;
  finally
    Doc := nil;
  end;


  MI := TMenuItem.Create(Self);
  MI.ImageIndex := 2;
  MI.Caption := 'Reload menu';
  MI.OnClick := miReloadClick;
  PopupMenu.Items.Add(MI);
  PopupMenu.Items.InsertNewLineBefore(MI);

  MI := TMenuItem.Create(Self);
  MI.ImageIndex := 0;
  MI.Caption := 'Exit';
  MI.OnClick := miExitClick;
  PopupMenu.Items.Add(MI);
  PopupMenu.Items.InsertNewLineBefore(MI);
end;

procedure TwndMain.QuickLaunchClick(Sender: TObject);
var
  S: string;
  N: Integer;
begin
  N := TMenuItem(sender).Tag;
  if N < 0 then Exit;

  S := Links[N];
  ShellExecute(0, 'OPEN', PChar(S), nil, nil, SW_SHOW);
end;

procedure TwndMain.TrayIconClick(Sender: TObject);
begin
  SetForegroundWindow(Application.Handle);
  Application.ProcessMessages;
  TrayIcon.PopupMenu.AutoPopup := False;
  TrayIcon.PopupMenu.PopupComponent := TrayIcon;
  TrayIcon.PopupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

end.
