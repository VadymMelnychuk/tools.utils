const 
  cx = 22;
  cy = 11.5;
  cnt = 21;
var
  i: Integer;
  StartX, StartY: Integer;
begin
  GetActiveCoord(StartX, StartY);
  for i := 0 to cnt - 1 do
  begin
    MouseClick(StartX + Round(cx * i), 
      StartY - Round(cy * i), 1);
  end;
end.