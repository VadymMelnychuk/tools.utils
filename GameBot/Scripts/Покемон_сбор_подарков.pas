const
  c_ClickCount = 50; 
  c_StratX = 359;
  c_EndX = 334;
//  c_XOffset = 83; // второй слот
var
  I: Integer;
  StartX, StartY, DecX: Integer;
begin
  GetActiveCoord(StartX, StartY);
  
  DecX := Trunc((c_StratX + c_EndX) / c_ClickCount);
  for i := 0 to c_ClickCount - 1 do
  begin
    MouseClick(StartX - c_StratX + (DecX * i), StartY - 97, 1);
    Pause(100);
    MouseClick(StartX + 31, StartY -41, 1);
    Pause(100);
  end;
end.  