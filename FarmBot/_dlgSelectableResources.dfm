object dlgSelRes: TdlgSelRes
  Left = 0
  Top = 0
  Caption = 'Select available resources'
  ClientHeight = 301
  ClientWidth = 294
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    294
    301)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 272
    Width = 76
    Height = 13
    Caption = 'Sleep time (sec)'
    Visible = False
  end
  object btnOK: TButton
    Left = 130
    Top = 268
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 211
    Top = 268
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object tlRes: TcxTreeList
    Left = 6
    Top = 8
    Width = 280
    Height = 257
    Hint = ''
    Anchors = [akLeft, akTop, akRight, akBottom]
    Bands = <
      item
      end>
    Navigator.Buttons.CustomButtons = <>
    OptionsCustomizing.ColumnHorzSizing = False
    OptionsCustomizing.ColumnMoving = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.Deleting = False
    OptionsView.ColumnAutoWidth = True
    OptionsView.ShowRoot = False
    TabOrder = 2
    object clnCheck: TcxTreeListColumn
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Properties.OnChange = clnCheckPropertiesChange
      DataBinding.ValueType = 'String'
      Width = 36
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object clnName: TcxTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Caption.Text = 'Name'
      DataBinding.ValueType = 'String'
      Options.Editing = False
      Width = 286
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object seSleepTime: TSpinEdit
    Left = 88
    Top = 269
    Width = 69
    Height = 22
    Increment = 5
    MaxValue = 7200
    MinValue = 5
    TabOrder = 3
    Value = 10
    Visible = False
  end
end
