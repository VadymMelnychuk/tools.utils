const
  C_Klever = 1;//наличие клевера
  C_Milk = 0;//изготовление сыра
  c_Yoghurt = 1; // Делать йогурт
  C_Wool = 1;//наличие шерсти
  C_Bur = 1;//наличие колючек
  c_Carrot = 1;//наличие морковки
  c_Jam = 0;//наличие сырья для джема
  C_Chips = 1;//наличие сырья для чипсов
  C_Ostrich = 0; //наличие капусты
  c_Perfume = 1; // тзготовление духов
//------------------------------------------------------
  c_LoopCount = 0;
  c_SleepTime = 180;

procedure KleverUpload(aWnd, AX, AY: Integer);

  procedure _CowProcess(CowX, CowY, AClicks: Integer);
  begin
    MouseClickEx(aWnd, CowX, CowY, AClicks);
    MouseClickEx(aWnd, CowX + 74, CowY + 8, AClicks);
  end;

var
  i, j: Integer;
  CurX, CurY: Integer;
begin
  if C_Klever = 0 then Exit;

  for j := 1 to 2 do
  begin
    CurX := AX;
    CurY := AY;
  // Первая корова
    _CowProcess(CurX, CurY, j);
    _CowProcess(CurX + 86, CurY - 49, j);
    _CowProcess(CurX + 176, CurY - 95, j);

    CurX := CurX - 109;
    CurY := CurY - 58;
    _CowProcess(CurX, CurY, j);
    _CowProcess(CurX + 86, CurY - 49, j);
    _CowProcess(CurX + 176, CurY - 95, j);
  end;{for j}
end;

//------------------------------------------------------------------------------

procedure MilkUpload(aWnd, AX, AY: Integer);
begin
  if C_Milk = 0 then Exit;

  MouseClickEx(aWnd, AX, AY, 6);
  MouseClickEx(aWnd, AX - 88, AY - 47, 6);
  MouseClickEx(aWnd, AX + 71, AY - 39, 6);
  MouseClickEx(aWnd, AX - 14, AY - 86, 6);
end;

//------------------------------------------------------------------------------

procedure PerfumeUpload(aWnd, AX, AY: Integer);
var
  i: Integer;
begin
  if c_Perfume = 0 then Exit;

  for i := 1 to 2 do
  begin
    MouseClickEx(aWnd, AX, AY, i);
    MouseClickEx(aWnd, AX + 73, AY + 4, i);

    MouseClickEx(aWnd, AX - 92, AY - 51, i);
    MouseClickEx(aWnd, AX - 21, AY - 44, i);
  end;
end;

//------------------------------------------------------------------------------

procedure YoghurtUpload(aWnd, AX, AY: Integer);
var
  i, j: Integer;
  CurX, CurY: Integer;
begin
  if c_Yoghurt = 0 then Exit;

  for j := 1 to 2 do
  begin
    CurX := AX;
    CurY := AY;
    for i := 0 to 2 do
    begin
      MouseClickEx(aWnd, CurX, CurY, j);
      MouseClickEx(aWnd, CurX + 37, CurY + 33, j);
      MouseClickEx(aWnd, CurX + 33, CurY - 30, j);

      CurX := CurX + 88;
      CurY := CurY - 45;
    end;
  end;{for j}
end;

//------------------------------------------------------------------------------

procedure CarrotUpload(aWnd, AX, AY: Integer);
var
  CurX, CurY: Integer;
begin
  if C_Carrot = 0 then Exit;

  CurX := AX;
  CurY := AY;
  MouseClickEx(aWnd, CurX, CurY, 6);

  CurX := CurX + 75;
  CurY := CurY - 40;
  MouseClickEx(aWnd, CurX, CurY, 6);
end;

//------------------------------------------------------------------------------

procedure BurUpload(aWnd, AX, AY: Integer);
var
  CurX, CurY: Integer;
begin
  if C_Bur = 0 then Exit;

  CurX := AX;
  CurY := AY;
  MouseClickEx(aWnd, CurX, CurY, 6);
end;

//------------------------------------------------------------------------------

procedure JamUpload(aWnd, AX, AY: Integer);

  procedure _ProcessJam(JamX, JamY, AClicks: Integer);
  begin
    MouseClickEx(aWnd, JamX, JamY, AClicks);
    MouseClickEx(aWnd, JamX + 21, JamY - 53, AClicks);
    MouseClickEx(aWnd, JamX + 30, JamY - 22, AClicks);
  end;

var
  I, j, CurX, CurY: Integer;
begin
  if C_Jam = 0 then Exit;

  for j := 1 to 2 do
  begin
    _ProcessJam(AX, AY, j);
    _ProcessJam(AX + 89, AY + 46, j);
    _ProcessJam(AX + 177, AY, j);
  end;
end;

//------------------------------------------------------------------------------

procedure OstrichUpload(aWnd, AX, AY: Integer);
begin
  if C_Ostrich = 0 then Exit;

  MouseClickEx(aWnd, AX, AY, 6);
  MouseClickEx(aWnd, AX + 132, AY - 69, 6);
end;

//------------------------------------------------------------------------------

procedure WoolUpload(aWnd, AX, AY: Integer);
var
  i, j,
  CurX, CurY: Integer;
begin
  if C_Wool = 0 then Exit;

  for j := 1 to 2 do
  begin
    CurX := AX;
    CurY := AY;
    for i := 0 to 2 do
    begin
      MouseClickEx(aWnd, CurX, CurY, j);
      MouseClickEx(aWnd, CurX + 48, CurY + 30, j);

      CurX := CurX + 88;
      CurY := CurY - 48;
    end;{for i}
  end;{for j}
end;

//------------------------------------------------------------------------------

procedure ChipsUpload(aWnd, AX, AY: Integer);

  procedure _ProcessChips(ChipsX, ChipsY, AClicks: Integer);
  begin
    MouseClickEx(aWnd, ChipsX, ChipsY, AClicks);
    MouseClickEx(aWnd, ChipsX + 48, ChipsY + 22, AClicks);
    MouseClickEx(aWnd, ChipsX + 95, ChipsY + 20, AClicks);
  end;

var
  I, j, CurX, CurY: Integer;
begin
  if C_Chips = 0 then Exit;

  for j := 1 to 2 do
  begin
    _ProcessChips(AX, AY, J);
    _ProcessChips(AX + 104, AY + 65, J);
    _ProcessChips(AX + 191, AY + 9, J);
  end;
end;

//------------------------------------------------------------------------------

var
  I, Wnd,
  StartX, StartY, CurX, CurY: Integer;
begin
  GetActiveCoord(StartX, StartY);
  Wnd := GetWindowFromPos(StartX, StartY);
  ScreenToClient(Wnd, StartX, StartY);

  for i := 0 to c_LoopCount do
  begin
    if I > 0 then Pause(c_SleepTime * 1000);

    CurX := StartX;
    CurY := StartY;

    PerfumeUpload(Wnd, CurX - 151, CurY - 144);
    KleverUpload(Wnd, CurX, CurY);
    MilkUpload(Wnd, CurX + 11, CurY - 186);
    YoghurtUpload(Wnd, CurX + 148, CurY + 37);

    if (c_Wool <> 0) or (c_Ostrich <> 0) then
    begin
      MoveMapEx(Wnd, CurX + 125, CurY, CurX - 125, CurY - 270);

{      setmousepos(startx, starty);
      exit;{}
      WoolUpload(Wnd, CurX - 5, CurY - 189);
      OstrichUpload(Wnd, CurX + 160, CurY - 136);

      MoveMapEx(Wnd, CurX - 125, CurY - 270, CurX + 125, CurY);
    end;

    if (c_Chips <> 0) or (c_Carrot <> 0) or
       (c_Bur <> 0) or (c_Jam <> 0)
    then begin
  // Переход к чипсам
      MoveMapEx(Wnd, CurX + 130, CurY - 400, CurX - 33, CurY + 10);

{      setmousepos(startx, starty);
      exit;{}

      JamUpload(Wnd, CurX + 133, CurY - 312);
      CarrotUpload(Wnd, CurX + 100, CurY - 187);
      ChipsUpload(Wnd, CurX - 258, CurY - 191);
      BurUpload(Wnd, CurX - 143, CurY - 220);

      MoveMapEx(Wnd, CurX - 33, CurY + 10, CurX + 130, CurY - 400);
    end;
  end;{for i}
end.
