object dlgCommandEdit: TdlgCommandEdit
  Left = 0
  Top = 0
  Caption = 'Edit command'
  ClientHeight = 371
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    479
    371)
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 30
    Height = 15
    Caption = 'Code'
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 43
    Height = 15
    Caption = 'Caption'
  end
  object Label3: TLabel
    Left = 8
    Top = 108
    Width = 21
    Height = 15
    Caption = 'Text'
  end
  object edCode: TcxTextEdit
    Left = 8
    Top = 24
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Width = 461
  end
  object edCaption: TcxTextEdit
    Left = 8
    Top = 72
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    Width = 461
  end
  object memText: TcxMemo
    Left = 8
    Top = 124
    Anchors = [akLeft, akTop, akRight, akBottom]
    Properties.ScrollBars = ssBoth
    Properties.WordWrap = False
    TabOrder = 2
    ExplicitHeight = 172
    Height = 202
    Width = 461
  end
  object Panel1: TPanel
    Left = 0
    Top = 336
    Width = 479
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitTop = 265
    object Panel2: TPanel
      Left = 313
      Top = 0
      Width = 166
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 314
      ExplicitHeight = 41
      object btnOK: TButton
        Left = 0
        Top = 4
        Width = 75
        Height = 25
        Caption = 'OK'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btnCancel: TButton
        Left = 81
        Top = 4
        Width = 75
        Height = 25
        Cancel = True
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
end
