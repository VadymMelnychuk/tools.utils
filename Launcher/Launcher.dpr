program Launcher;

uses
  Forms,
  _wndMAin in '_wndMAin.pas' {wndMain};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := False;
  Application.ShowMainForm := False;
  Application.CreateForm(TwndMain, wndMain);
  Application.Run;
end.
