unit _svcMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs;

type
  TsvcAutoBuilder = class(TService)
    procedure ServiceAfterInstall(Sender: TService);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  svcAutoBuilder: TsvcAutoBuilder;

implementation

{$R *.DFM}
uses
  Winapi.WinSvc;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  svcAutoBuilder.Controller(CtrlCode);
end;

function TsvcAutoBuilder.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TsvcAutoBuilder.ServiceAfterInstall(Sender: TService);
const
  NTServiceType: array[TServiceType] of Integer = ( SERVICE_WIN32_OWN_PROCESS,
    SERVICE_KERNEL_DRIVER, SERVICE_FILE_SYSTEM_DRIVER);
var
  SvMgr, Svc: THandle;
  N: Cardinal;
  S: string;
begin
  SvMgr := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SvMgr = 0 then RaiseLastOSError;
  try
    Svc := OpenService(SvMgr, PChar(Name), SERVICE_ALL_ACCESS);
    if Svc = 0 then RaiseLastOSError;

    try
      S := ParamStr(0) + ' /service';
      N := NTServiceType[Sender.ServiceType];// or SERVICE_INTERACTIVE_PROCESS;
      if (Sender.ServiceType = stWin32) and (Application.ServiceCount > 1) then
        N := (N xor SERVICE_WIN32_OWN_PROCESS) or SERVICE_WIN32_SHARE_PROCESS;
      if not ChangeServiceConfig(Svc, N, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE,
        PChar(S), nil, nil, nil, nil, nil, nil)
      then
        RaiseLastOSError;
    finally
      CloseServiceHandle(Svc);
    end;
  finally
    CloseServiceHandle(SvMgr);
  end;
end;

end.
