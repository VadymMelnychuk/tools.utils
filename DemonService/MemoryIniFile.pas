{*******************************************************}
{                                                       }
{       �����: ����� ��������                           }
{       ���� ��������: 2005/01/21                       }
{       ���� ���������: 2005/01/21                      }
{                                                       }
{*******************************************************}
unit MemoryIniFile;

//******************************************************************************
interface
//******************************************************************************

uses
  SysUtils, Classes, IniFiles;

type

  TMemoryIniFile = class(TCustomIniFile)
  private
    function GetSectionName(Index: Integer): string;
    function GetSectionsCount: Integer;
    procedure SetSectionName(Index: Integer; const Value: string);
  protected
    FSections: TStringList;
    function AddSection(const Section: string): TStrings; virtual;
    procedure LoadValues; virtual;
  public
    constructor Create(const AStrings: TStrings = nil);
    destructor Destroy; override;
    procedure Clear;

  // ����� �� �����������
    procedure UpdateFile; override;
  // �������� ������ � ���� �������
    procedure GetStrings(List: TStrings); virtual;
  // �������� ������ �� ������� � ������ ���
    procedure SetStrings(List: TStrings); overload; virtual;
  // �������� ������ �� ������ � ������ ���
    procedure SetStrings(const AText: string); overload; virtual;
  // ������� ����
    procedure DeleteKey(const Section, Ident: string); override;
  // �������� ������
    procedure EraseSection(const Section: string); override;
  // ��������� ������ � �������
    procedure ReadSectionText(const Section: string; const Text: TStrings); virtual;
  // �������� � ������ �������
    procedure WriteSectionText(const Section: string; const Text: TStrings); virtual;
  // ������ ������
    procedure ReadSection(const Section: string; Strings: TStrings); override;
  // �������� ������ ������
    procedure ReadSections(Strings: TStrings); override;
  // �������� �������� �� ������
    procedure ReadSectionValues(const Section: string; Strings: TStrings); override;
  // ������� �����
    function ReadString(const Section, Ident, Default: string): string; override;
  // �������� �����
    procedure WriteString(const Section, Ident, Value: string); override;

    property SectionsCount: Integer read GetSectionsCount;
    property SectionName[Index: Integer]: string read GetSectionName write SetSectionName;
  end;


//******************************************************************************
implementation
//******************************************************************************


//==============================================================================
// TMemoryIniFile
//==============================================================================
{ TMemoryIniFile }

constructor TMemoryIniFile.Create(const AStrings: TStrings);
begin
  FSections := TStringList.Create;
  if Assigned(AStrings) then
    SetStrings(AStrings);
end;

//------------------------------------------------------------------------------

destructor TMemoryIniFile.Destroy;
begin
  if Assigned(FSections) then
  begin
    Clear;
    FSections.Free;
  end;
  FSections := nil;

  inherited Destroy;
end;

//------------------------------------------------------------------------------

function TMemoryIniFile.AddSection(const Section: string): TStrings;
begin
  Result := TStringList.Create;
  try
    FSections.AddObject(Section, Result);
  except
    Result.Free;
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.LoadValues;
begin

end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.Clear;
var
  i: Integer;
begin
  for i := 0 to FSections.Count - 1 do
    TStrings(FSections.Objects[i]).Free;
  FSections.Clear;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.GetStrings(List: TStrings);
var
  i, j: Integer;
  Strings: TStrings;
begin
  List.BeginUpdate;
  try
    for i := 0 to FSections.Count - 1 do
    begin
      List.Add('[' + FSections[i] + ']');
      Strings := TStrings(FSections.Objects[i]);
      for j := 0 to Strings.Count - 1 do List.Add(Strings[j]);
      List.Add('');
    end;
  finally
    List.EndUpdate;
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.SetStrings(List: TStrings);

  //-------------------------------------

  function _TrimAroundEqual(const S: string): string;
  var
    P: Integer;
    Name: string;
  begin
    Result := {Trim}(S);
    P := AnsiPos('=', Result);
    if P <= 0 then Exit;
//    Name := Result;
    Name := Copy(Result, 1, P - 1);//    abfDeleteAfterChar(Name, '=');
    Result := Copy(Result, P + 1, MaxInt);// abfDeleteBeforeChar(Result, '=');
    Result := TrimRight(Name) + '=' + TrimLeft(Result);
  end;{Internal _TrimAroundEqual}

  //-------------------------------------

var
  i: Integer;
  S: string;
  Strings: TStrings;
begin
  Clear;
  Strings := nil;
  for i := 0 to List.Count - 1 do
  begin
    S := Trim(List[i]);
    if (S <> '') and (S[1] <> ';') then
      if (S[1] = '[') and (S[Length(S)] = ']') then
        Strings := AddSection(Copy(S, 2, Length(S) - 2))
      else
        if Strings <> nil then Strings.Add(_TrimAroundEqual(S));
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.SetStrings(const AText: string);
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    List.Text := AText;
    SetStrings(List);
  finally
    List.Free;
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.DeleteKey(const Section, Ident: string);
var
  i, j: Integer;
  Strings: TStrings;
begin
  i := FSections.IndexOf(Section);
  if i >= 0 then
  begin
    Strings := TStrings(FSections.Objects[i]);
    j := Strings.IndexOfName(Ident);
    if j >= 0 then Strings.Delete(j);
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.EraseSection(const Section: string);
var
  i: Integer;
begin
  i := FSections.IndexOf(Section);
  if i >= 0 then
  begin
    TStrings(FSections.Objects[i]).Free;
    FSections.Delete(I);
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.ReadSectionText(const Section: string;
  const Text: TStrings);
var
  i: Integer;
begin
  if not Assigned(Text) then Exit;
  Text.BeginUpdate;
  try
    Text.Clear;
    i := FSections.IndexOf(Section);
    if i >= 0 then Text.AddStrings(TStrings(FSections.Objects[i]));
  finally
    Text.EndUpdate;
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.WriteSectionText(const Section: string;
  const Text: TStrings);
var
  ASection: TStrings;
  i: Integer;
begin
  if not Assigned(Text) then Exit;
  i := FSections.IndexOf(Section);
  if i >= 0 then
    ASection := TStrings(FSections.Objects[i])
  else
    ASection := AddSection(Section);
  ASection.Clear;
  ASection.AddStrings(Text);
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.ReadSection(const Section: string;
  Strings: TStrings);
var
  i, j: Integer;
  SectionStrings: TStrings;
begin
  Strings.BeginUpdate;
  try
    Strings.Clear;
    i := FSections.IndexOf(Section);
    if i >= 0 then
    begin
      SectionStrings := TStrings(FSections.Objects[i]);
      for j := 0 to SectionStrings.Count - 1 do
        Strings.Add(SectionStrings.Names[j]);
    end;
  finally
    Strings.EndUpdate;
  end;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.ReadSections(Strings: TStrings);
begin
  Strings.Assign(FSections);
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.ReadSectionValues(const Section: string;
  Strings: TStrings);
var
  i: Integer;
begin
  Strings.BeginUpdate;
  try
    Strings.Clear;
    i := FSections.IndexOf(Section);
    if i >= 0 then Strings.Assign(TStrings(FSections.Objects[i]));
  finally
    Strings.EndUpdate;
  end;
end;

//------------------------------------------------------------------------------

function TMemoryIniFile.ReadString(const Section, Ident,
  Default: string): string;
var
  i: Integer;
  Strings: TStrings;
begin
  i := FSections.IndexOf(Section);
  if i >= 0 then
  begin
    Strings := TStrings(FSections.Objects[i]);
    i := Strings.IndexOfName(Ident);
    if i >= 0 then
    begin
      Result := Copy(Strings[i], Length(Ident) + 2, Maxint);
      Exit;
    end;
  end;
  Result := Default;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.WriteString(const Section, Ident, Value: string);
var
  i: Integer;
  S: string;
  Strings: TStrings;
begin
  i := FSections.IndexOf(Section);
  if i >= 0 then
    Strings := TStrings(FSections.Objects[i]) else
    Strings := AddSection(Section);
  S := Ident + '=' + Value;
  i := Strings.IndexOfName(Ident);
  if i >= 0 then Strings[i] := S else Strings.Add(S);
end;

//------------------------------------------------------------------------------
// Property get/set
function TMemoryIniFile.GetSectionsCount: Integer;
begin
  Result := FSections.Count;
end;

//------------------------------------------------------------------------------

function TMemoryIniFile.GetSectionName(Index: Integer): string;
begin
  Result := FSections[Index];
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.SetSectionName(Index: Integer;
  const Value: string);
begin
  FSections[Index] := Value;
end;

//------------------------------------------------------------------------------

procedure TMemoryIniFile.UpdateFile;
begin
// Not used
end;

//------------------------------------------------------------------------------

end.
 