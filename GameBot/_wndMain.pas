unit _wndMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter, dxSkinsdxBarPainter,
  dxSkinsdxStatusBarPainter, dxSkinsdxRibbonPainter,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxPC, ExtCtrls, Menus, cxButtons, cxMemo,
  SynEditHighlighter, SynHighlighterPas, SynEdit, dxBar, cxClasses, fs_ipascal,
  fs_iinterpreter, ActnList, ImgList, fs_iformsrtti, fs_iclassesrtti, ComCtrls,
  cxButtonEdit, cxBarEditItem, dxStatusBar,
  cxListView,
  dxRibbon, dxRibbonForm, cxDropDownEdit, cxCheckBox,
  dxRibbonGallery, dxRibbonStatusBar, dxRibbonSkins, cxPCdxBarPopupMenu,
  dxBarApplicationMenu;

type
  TFermerSynEdit = class(TSynEdit)
  end;


  TFermfsScript = class(TfsScript)
  private
    FTerminated: Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Terminate2;
    property Terminated: Boolean read FTerminated;
  end;

  TfsScriptHelper = class
  private
    FScript: TFermfsScript;
    FSendMessageMode: Integer;
  public
    constructor Create;
    destructor Destroy; override;

    procedure RegisterScripts;
    function FermMethodHandler(Instance: TObject; ClassType: TClass;
      const MethodName: String; Caller: TfsMethodHelper): Variant;
    function RunScript(const AText: string;
      AOnlyCompile: Boolean; var AErrorStr: string): Boolean;
    property Script: TFermfsScript read FScript;
  end;

  TScriptThread = class(TThread)
  private
    FScriptHelper: TfsScriptHelper;
    FErrorStr: string;
  protected
    FScriptText: string;
    procedure Execute; override;
    procedure DoShowError;
  public
    constructor Create(const AScriptText: string);
    destructor Destroy; override;
    procedure BreakScript;
  end;

  TPageData = class
  private
    FFileName: string;
    FScriptThread: TScriptThread;
  public
    constructor Create;
    procedure OnThreadTerminate(Sender: TObject);
    property ScriptThread: TScriptThread read FScriptThread write FScriptThread;
    property FileName: string read FFileName write FFileName;
  end;

  TwndMain = class(TdxRibbonForm)
    tmrCurMouseCoord: TTimer;
    lfController: TcxLookAndFeelController;
    fsPascal: TfsPascal;
    BarManager: TdxBarManager;
    barFile: TdxBar;
    bbNew: TdxBarButton;
    bbSave: TdxBarButton;
    ilMain: TImageList;
    alMain: TActionList;
    bbRun: TdxBarButton;
    actNew: TAction;
    actOpen: TAction;
    actSave: TAction;
    actRun: TAction;
    bbCompile: TdxBarButton;
    actCompile: TAction;
    OpenDialog: TOpenDialog;
    fsClassesRTTI1: TfsClassesRTTI;
    fsFormsRTTI1: TfsFormsRTTI;
    bbBreak: TdxBarButton;
    actBreak: TAction;
    SaveDialog: TSaveDialog;
    actCloseScript: TAction;
    bbOpen: TdxBarButton;
    SynPasSyn: TSynPasSyn;
    pmMRU: TdxBarPopupMenu;
    bbSaveAs: TdxBarButton;
    pmSaveAs: TdxBarPopupMenu;
    actSaveAs: TAction;
    actDetect: TAction;
    bbWindows: TdxBarListItem;
    bbStayOnTop: TdxBarButton;
    actStayOnTop: TAction;
    rbTab: TdxRibbonTab;
    Ribbon: TdxRibbon;
    BarApplicationMenu: TdxBarApplicationMenu;
    pcScripts: TcxPageControl;
    tshDetector: TcxTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    btnSaveCoord: TcxButton;
    lvCoord: TcxListView;
    barScript: TdxBar;
    barOther: TdxBar;
    dxBarButton1: TdxBarButton;
    actShowDetector: TAction;
    bbExit: TdxBarButton;
    barQuickAccess: TdxBar;
    dxBarListItem1: TdxBarListItem;
    bbSpeed: TcxBarEditItem;
    bbEnableSpeedUp: TcxBarEditItem;
    StatusBar: TdxRibbonStatusBar;
    bbCloseScript: TdxBarLargeButton;
    pmTree: TdxBarPopupMenu;
    actCopyAsMouseClick: TAction;
    bbCopyAsMouseClick: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmrCurMouseCoordTimer(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actCompileExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actRunUpdate(Sender: TObject);
    procedure actBreakExecute(Sender: TObject);
    procedure actBreakUpdate(Sender: TObject);
    procedure actNewExecute(Sender: TObject);
    procedure actCloseScriptExecute(Sender: TObject);
    procedure SynEditStatusChange(Sender: TObject; Changes: TSynStatusChanges);
    procedure MRUListClick(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure actDetectExecute(Sender: TObject);
    procedure lvCoordKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bbWindowsGetData(Sender: TObject);
    procedure bbWindowsClick(Sender: TObject);
    procedure actStayOnTopExecute(Sender: TObject);
    procedure actStayOnTopUpdate(Sender: TObject);
    procedure actShowDetectorExecute(Sender: TObject);
    procedure actShowDetectorUpdate(Sender: TObject);
    procedure actSaveUpdate(Sender: TObject);
    procedure bbEnableSpeedUpPropertiesChange(Sender: TObject);
    procedure bbSpeedPropertiesChange(Sender: TObject);
    procedure actCopyAsMouseClickExecute(Sender: TObject);
  private
    SLMru: TStringList;
    procedure ReloadMRUList;
    procedure AddFileToMRUList(const AFileName: string);
    procedure NewScriptPage;
    function GetScriptText: string;
    function GetActiveSynEdit: TFermerSynEdit;
    function GetActivePageData: TPageData;
    procedure OpenScriptFile(const AFileName: string);
  public
    { Public declarations }
    procedure LoadSettings;
    procedure SaveSettings;
  end;

var
  wndMain: TwndMain;

implementation

uses
  Math, Registry, ClipBrd;

{$R *.dfm}
{$R windowsxp.res}


procedure InstallHook(AHandle: THandle); stdcall; external 'Hook.dll' name 'InstallHook';
procedure SetgMul(ASpeed: Integer); stdcall; external 'Hook.dll' name 'SetgMul';
procedure UninstallHook; stdcall; external 'Hook.dll' name 'UninstallHook';


constructor TfsScriptHelper.Create;
begin
  FScript := TFermfsScript.Create(nil);
  FScript.Parent := fsGlobalUnit;
  FSendMessageMode := 0;
  RegisterScripts;
end;

destructor TfsScriptHelper.Destroy;
begin
  FScript.Free;
  inherited;
end;

procedure TfsScriptHelper.RegisterScripts;
begin
  //RegisterHotKey()
  FScript.AddMethod('procedure GetActiveCoord(var AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure GetMousePos(var AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure SetMousePos(AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure SetMousePosEx(aWnd, AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure MouseLDown(AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure MouseLUp(AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure MouseClick(X, Y, ACount: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure MoveMap(AFromX, AFromY, AToX, AToY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure Pause(AMSec: Integer)', FermMethodHandler);

  FScript.AddMethod('procedure MouseLDownEx(aWnd, AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure MouseLUpEx(aWnd, AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure MouseClickEx(aWnd, X, Y, ACount: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure MoveMapEx(aWnd, AFromX, AFromY, AToX, AToY: Integer)', FermMethodHandler);
  FScript.AddMethod('function GetWindowFromPos(AX, AY: Integer): Integer', FermMethodHandler);
  FScript.AddMethod('procedure SetWindowPos(Wnd, AX, AY: Integer)', FermMethodHandler);
  FScript.AddMethod('procedure ScreenToClient(aWnd: Integer; var AX, AY: Integer)', FermMethodHandler);

  FScript.AddMethod('function GetWindowRect(aWnd: Integer; var ALeft, ATop, ARight, ABottom: Integer): Boolean', FermMethodHandler);
  FScript.AddMethod('function FindPicturePart(aWnd: Integer; const APicName: string;' +
    ' ALeft, ATop, ARight, ABottom: Integer; var AX, AY: Integer): Boolean', FermMethodHandler);
  FScript.AddMethod('function IsWindow(aWnd: Integer): Boolean', FermMethodHandler);
  FScript.AddMethod('function IsWindowVisible(aWnd: Integer): Boolean', FermMethodHandler);
  FScript.AddMethod('procedure SetSendMessageMode(AMode: Integer): Boolean', FermMethodHandler);
end;

function TfsScriptHelper.FermMethodHandler(Instance: TObject; ClassType: TClass;
  const MethodName: String; Caller: TfsMethodHelper): Variant;

  //----------------------------------------

  function _CheckTerminated(AProg: TfsScript): Boolean;
  begin
    Result := False;
    if not Assigned(AProg) then
      Exit;

    if AProg is TFermfsScript then
      Result := TFermfsScript(Caller.Prog).Terminated;
    if Result then
      Exit;

    Result := _CheckTerminated(AProg.Parent);
  end;

  //----------------------------------------

  procedure _FermPause(AMSec: Cardinal);
  var
    N: Cardinal;
  begin
    N := GetTickCount;
    while (GetTickCount - N) < AMSec do
    begin
      Sleep(10);
      Application.ProcessMessages;
      if Application.Terminated then Exit;

      if _CheckTerminated(Caller.Prog) then
        Exit;
    end;
  end;

  //----------------------------------------

  procedure _SendMessage(hWnd: HWND; Msg: UINT; wParam: WPARAM; lParam: LPARAM);
  var
    R: Cardinal;
  begin
    case FSendMessageMode of
      0: PostMessage(hWnd, Msg, wParam, lParam);
      1: SendMessageTimeout(hWnd, Msg, wParam, lParam, 0, 1000, R);
      2: SendMessage(hWnd, Msg, wParam, lParam);
    end;
  end;

  //----------------------------------------

  procedure _MouseDown(aWnd, AX, AY: Integer);
  begin
 {   _SendMessage(AWnd, WM_MOUSEMOVE, 0, PointToLParam(Point(AX, AY)));
    Sleep(2); }
    _SendMessage(aWnd, WM_LBUTTONDOWN, MK_LBUTTON, PointToLParam(Point(AX, AY)));
    Sleep(10);
  end;

  //----------------------------------------

  procedure _MouseUp(aWnd, AX, AY: Integer);
  begin
    _SendMessage(AWnd, WM_LBUTTONUP, MK_LBUTTON, PointToLParam(Point(AX, AY)));
   { Sleep(2);
    _SendMessage(AWnd, WM_MOUSEMOVE, 0, PointToLParam(Point(AX, AY)));   }
    Sleep(10);
  end;

  //----------------------------------------

  procedure _MouseMove(aWnd, AX, AY, wParam: Integer);
  begin
    _SendMessage(AWnd, WM_MOUSEMOVE, wParam, PointToLParam(Point(AX, AY)));
    Sleep(2);
  end;

  //----------------------------------------

  procedure _PrepareClick(var AWnd: THandle; var AX, AY: Integer);
  var
    P: TPoint;
  begin
    P := Point(AX, AY);
    aWnd := Windows.WindowFromPoint(P);
    Windows.ScreenToClient(aWnd, P);
    AX := P.X;
    AY := P.Y;
  end;

  //----------------------------------------

  function _ScanImage(AX, AY: Integer; ABigImg, ASmallImg: TBitmap): Boolean;
  var
    i, j: Integer;
    CBig, CSmall: TCanvas;
  begin
    Result := False;
    CBig := ABigImg.Canvas;
    CSmall := ASmallImg.Canvas;

    for J := 0 to ASmallImg.Height - 1 do
      for i := 0 to ASmallImg.Width - 1 do
      begin
        Application.ProcessMessages;
        if CBig.Pixels[AX + I, AY + J] <> CSmall.Pixels[I, J] then Exit;
      end;

    Result := True;
  end;

  //----------------------------------------

  function _FindPicPart(const AWnd: THandle; const APicName: string;
    ALeft, ATop, ARight, ABottom: Integer; var AX, AY: Integer): Boolean;
  var
    bmp, FBmp: TBitmap;
    R: TRect;
    i, j: Integer;
    B: Boolean;
 //   P: TPoint;
    DC: HDC;
  begin
    if not IsWindowVisible(AWND) then
    begin
      Result := True;
      Exit;
    end else
      Result := False;

    if (ALeft >= 0) and (ATop >= 0) and (ARight > 0) and (ABottom > 0) then
      R := Rect(ALeft, ATop, ARight, ABottom)
    else begin
      if not GetWindowRect(aWnd, R) then
        Exit;
    end;

    bmp := TBitmap.Create;
    FBmp := TBitmap.Create;
    try
      bmp.Width := R.Right - R.Left;
      bmp.Height := R.Bottom - R.Top;

      DC := GetDC(aWnd);
      try
        BitBlt(bmp.Canvas.Handle, 0,0, bmp.Width, Bmp.Height, DC, R.Left, R.Top, SRCCOPY);
      finally
        ReleaseDC(aWnd, DC);
      end;

      //Bmp.SaveToFile('d:\' + IntToStr(GetTickCount) + '.bmp');
    // check image
      J := Bmp.Height div 2;
      B := True;
      for I := 0 to Bmp.Width - 2 do
      begin
        B := bmp.Canvas.Pixels[i, j] = bmp.Canvas.Pixels[i + 1, j];
        if not B then
          Break;
      end;

    // image not valid
      if B then
      begin
        Result := True;
        Exit;
      end;


      FBmp.LoadFromFile(APicName); 

      for J := Bmp.Height - FBmp.Height downto 0 do
        for I := 0 to Bmp.Width - FBMP.Width do
        begin
          if _ScanImage(I, J, Bmp, FBmp) then
          begin
            Result := True;
            AX := R.Left + I;
            AY := R.Top + J;
            Exit;
          end;

          Application.ProcessMessages;
        end;
    finally
      bmp.Free;
      Fbmp.Free;
    end;
  end;

var
  i, FX, FY: Integer;
  SName: string;
  P: TPoint;
  R: TRect;
  FWnd: THandle;
  FOldSendMessage: Integer;
begin
  SName := UpperCase(MethodName);
  if (SName = UpperCase('GetActiveCoord')) or (SName = UpperCase('GetMousePos')) then
  begin
    Caller.Params[0] := Mouse.CursorPos.X;
    Caller.Params[1] := Mouse.CursorPos.Y;
  end else

  if SName = UpperCase('MouseClick') then
  begin
    FX := Caller.Params[0];
    FY := Caller.Params[1];
    _PrepareClick(FWnd, FX, FY);
    for I := 0 to Caller.Params[2] - 1 do
    begin
      _MouseDown(FWnd, FX, FY);
      _MouseUp(FWnd, FX, FY);
    end;
  end else

  if SName = UpperCase('MouseClickEx') then
  begin
    for I := 0 to Caller.Params[3] - 1 do
    begin
      _MouseDown(Caller.Params[0], Caller.Params[1], Caller.Params[2]);
      _MouseUp(Caller.Params[0], Caller.Params[1], Caller.Params[2]);
    end;
  end else

  if SName = UpperCase('MoveMap') then
  begin
    FX := Caller.Params[0];
    FY := Caller.Params[1];
    _PrepareClick(FWnd, FX, FY);

    _MouseDown(FWnd, FX, FY);
    Sleep(100);
    P := Point(Caller.Params[3], Caller.Params[4]);
    Windows.ScreenToClient(FWnd, P);
    _MouseUp(FWnd, P.X, P.Y);
    Sleep(100);
  end else

  if SName = UpperCase('MoveMapEx') then
  begin
//    FOldSendMessage := FSendMessageMode;
//    FSendMessageMode := 1;
    try
      _MouseDown(Caller.Params[0], Caller.Params[1], Caller.Params[2]);
      _MouseMove(Caller.Params[0], Caller.Params[1], Caller.Params[2], MK_LBUTTON);
      Sleep(100);
      _MouseMove(Caller.Params[0], Caller.Params[3], Caller.Params[4], MK_LBUTTON);
      _MouseUp(Caller.Params[0], Caller.Params[3], Caller.Params[4]);
      Sleep(100);
    finally
//      FSendMessageMode := FOldSendMessage;
    end;
  end else

  if SName = UpperCase('Pause') then
  begin
    _FermPause(Caller.Params[0]);
  end else

  if SName = UpperCase('SetMousePos') then
  begin
    Mouse.CursorPos := Point(Caller.Params[0], Caller.Params[1]);
  end else

  if SName = UpperCase('SetMousePosEx') then
  begin
    P.X := Caller.Params[1];
    P.Y := Caller.Params[2];
    Windows.ClientToScreen(Caller.Params[0], P);
    Mouse.CursorPos := P;
  end else

  if SName = UpperCase('MouseLDown') then
  begin
    FX := Caller.Params[0];
    FY := Caller.Params[1];
    _PrepareClick(FWnd, FX, FY);
    _MouseDown(FWnd, FX, FY);
  end else

  if SName = UpperCase('MouseLDownEx') then
  begin
    _MouseDown(Caller.Params[0], Caller.Params[1], Caller.Params[2]);
  end else

  if SName = UpperCase('MouseLUp') then
  begin
    FX := Caller.Params[0];
    FY := Caller.Params[1];
    _PrepareClick(FWnd, FX, FY);
    _MouseUp(FWnd, FX, FY);
  end else

  if SName = UpperCase('MouseLUpEx') then
  begin
    _MouseUp(Caller.Params[0], Caller.Params[1], Caller.Params[2]);
  end else

  if SName = UpperCase('GetWindowFromPos') then
  begin
    Result := Windows.WindowFromPoint(Point(Caller.Params[0], Caller.Params[1]));
  end else

  if SName = UpperCase('ScreenToClient') then
  begin
    P := Point(Caller.Params[1], Caller.Params[2]);
    Windows.ScreenToClient(Caller.Params[0], P);
    Caller.Params[1] := P.X;
    Caller.Params[2] := P.Y
  end else

  if SName = UpperCase('GetWindowRect') then
  begin
    Result := Windows.GetWindowRect(Caller.Params[0], R);
    if Result then
    begin
      Caller.Params[1] := 0;//R.Left;
      Caller.Params[2] := 0;//R.Top;
      Caller.Params[3] := R.Right - R.Left;
      Caller.Params[4] := R.Bottom - R.Top;
    end
  end else

  if SName = UpperCase('IsWindow') then
  begin
    Result := IsWindow(Caller.Params[0]);
  end else

  if SName = UpperCase('IsWindowVisible') then
  begin
    Result := IsWindowVisible(Caller.Params[0]);
  end else

  if SName = UpperCase('FindPicturePart') then
  begin
    FX := Caller.Params[6];
    FY := Caller.Params[7];

    Result := _FindPicPart(Caller.Params[0], Caller.Params[1], Caller.Params[2], Caller.Params[3],
      Caller.Params[4], Caller.Params[5], FX, FY);
    if Result then
    begin
      Caller.Params[6] := FX;
      Caller.Params[7] := FY;
    end;
  end else

  if SName = UpperCase('SetSendMessageMode') then
  begin
    FSendMessageMode := Caller.Params[0];
  end else

  if SName = UpperCase('SetWindowPos') then
  begin
    Windows.SetWindowPos(Caller.Params[0], HWND_TOP, Caller.Params[1], Caller.Params[2], 0, 0, SWP_NOSIZE)
  end;

end;

function TfsScriptHelper.RunScript(const AText: string; AOnlyCompile: Boolean; var AErrorStr: string): Boolean;
var
  FLocalScript: TFermfsScript;
  ScriptText: string;
begin
//  FLocalScript := TFermfsScript.Create(nil);

  try
    FLocalScript{.Parent} := FScript;

    FLocalScript.ExtendedCharset := True;
    FLocalScript.SyntaxType := 'PascalScript';

  // ��������� begin..end ��� ������� ��� {} ��� �++.
  // Pascal
    ScriptText := Trim(AText);
    if ScriptText = '' then
    begin
      Result := True;
      Exit;
    end;

    if not SameText(Copy(ScriptText, Length(ScriptText) - 3, 4), 'end.') then
      ScriptText := 'begin'#13#10 + ScriptText + #13#10'end.';
    FLocalScript.Lines.Text := ScriptText;

    if AOnlyCompile then
      Result := FLocalScript.Compile
    else
      Result := FLocalScript.Run;

    if Result then Exit;

    AErrorStr := Format('������� ��������� �������:'#13#10'%s'#13#10'�������: %s',
      [FLocalScript.ErrorMsg, FLocalScript.ErrorPos]);
{    MessageDlg(Format('������� ��������� �������:'#13#10'%s'#13#10'�������: %s',
      [FLocalScript.ErrorMsg, FLocalScript.ErrorPos]), mtError, [mbOK], 0);}
  finally
   // FreeAndNil(FLocalScript);
  end;
end;

//==============================================================================
//
//==============================================================================

procedure TwndMain.actBreakExecute(Sender: TObject);
var
  PD: TPageData;
begin
  PD := GetActivePageData;
  if Assigned(PD) and Assigned(PD.ScriptThread) then
  begin
    PD.ScriptThread.BreakScript;
{    PD.ScriptThread.DoTerminate;
    PD.ScriptThread.Free;}
  end;
end;

procedure TwndMain.actBreakUpdate(Sender: TObject);
var
  PD: TPageData;
begin
  PD := GetActivePageData;
  TAction(Sender).Enabled := Assigned(PD) and
    Assigned(PD.ScriptThread) and
    (not GetActivePageData.ScriptThread.Terminated);
end;

procedure TwndMain.actCloseScriptExecute(Sender: TObject);
var
  P: TcxTabSheet;
  PD: TPageData;
begin
  if pcScripts.PageCount = 1 then
    Exit;

  P := pcScripts.ActivePage;

  PD := GetActivePageData;
  if not Assigned(PD) then
    Exit;

  P.Tag := 0;
  PD.Free;

  if pcScripts.ActivePageIndex = 0 then
    pcScripts.ActivePageIndex := 1
  else
    pcScripts.ActivePageIndex := pcScripts.ActivePageIndex - 1;

  P.Free;
end;

procedure TwndMain.actNewExecute(Sender: TObject);
begin
  NewScriptPage;
end;

procedure TwndMain.actOpenExecute(Sender: TObject);
begin
  if not OpenDialog.Execute then
    Exit;

  OpenScriptFile(OpenDialog.FileName);
end;

procedure TwndMain.actSaveAsExecute(Sender: TObject);
var
//  FName: string;
  ed: TFermerSynEdit;
  pd: TPageData;
begin
  PD := GetActivePageData;
  if not Assigned(PD) then
    Exit;

  if PD.FileName <> '' then
  begin
    SaveDialog.InitialDir := ExtractFilePath(PD.FileName);
    SaveDialog.FileName := ExtractFileName(pd.FileName);
  end;

  if not SaveDialog.Execute then
    Exit;

  Ed := GetActiveSynEdit;
  if not Assigned(Ed) then Exit;

  Ed.Lines.SaveToFile(SaveDialog.FileName, TEncoding.Unicode);
  pcScripts.ActivePage.Caption := ChangeFileExt(ExtractFileName(SaveDialog.FileName), '');
  pd.FileName := SaveDialog.FileName;

  AddFileToMRUList(pd.FileName);
end;

procedure TwndMain.actSaveExecute(Sender: TObject);
var
//  FName: string;
  ed: TFermerSynEdit;
  pd: TPageData;
begin
  Ed := GetActiveSynEdit;
  if not Assigned(Ed) then Exit;

  PD := GetActivePageData;
  if not Assigned(PD) then
    Exit;

  if PD.FileName <> '' then
    Ed.Lines.SaveToFile(PD.FileName, TEncoding.Unicode)
  else
    actSaveAs.Execute;
end;

procedure TwndMain.actSaveUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := GetActivePageData <> nil;
end;

procedure TwndMain.actShowDetectorExecute(Sender: TObject);
begin
//  actShowDetector.Checked := not actShowDetector.Checked;
  tshDetector.TabVisible := not tshDetector.TabVisible;//actShowDetector.Checked;
end;

procedure TwndMain.actShowDetectorUpdate(Sender: TObject);
begin
  actShowDetector.Checked := tshDetector.TabVisible;
end;

procedure TwndMain.actStayOnTopExecute(Sender: TObject);
begin
  if FormStyle = fsStayOnTop then
    FormStyle := fsNormal
  else FormStyle := fsStayOnTop;
end;

procedure TwndMain.actStayOnTopUpdate(Sender: TObject);
begin
  TAction(Sender).Checked := (FormStyle = fsStayOnTop);
end;

procedure TwndMain.AddFileToMRUList(const AFileName: string);
var
  N, i: Integer;
begin
  N := SLMru.IndexOf(AFileName);
  if N >= 0 then
    SLMru.Delete(N);

  SLMru.Insert(0, AFileName);

  for i := SLMRU.Count - 1 downto 11 do
    SLMRU.Delete(i);

  ReloadMRUList;
end;

procedure TwndMain.bbEnableSpeedUpPropertiesChange(Sender: TObject);
begin
  if Boolean(bbEnableSpeedUp.EditValue) then
  begin
    InstallHook(Handle);
    SetgMul($100000);
  end else begin
    SetgMul($100000);
    UninstallHook;
  end;

  bbSpeed.Enabled := Boolean(bbEnableSpeedUp.EditValue);
end;

procedure TwndMain.bbSpeedPropertiesChange(Sender: TObject);
begin
  if not Boolean(bbEnableSpeedUp.EditValue) then
    Exit;

  case Integer(bbSpeed.EditValue) of
    2: SetgMul($200000);
    4: SetgMul($300000);
    8: SetgMul($400000);
    16: SetgMul($500000);
    32: SetgMul($600000);
    64: SetgMul($700000);
    128: SetgMul($800000);
  else
    SetgMul($100000);
  end;
end;

procedure TwndMain.bbWindowsClick(Sender: TObject);
begin
  pcScripts.ActivePage := TcxTabSheet(bbWindows.Items.Objects[bbWindows.ItemIndex]);
end;

procedure TwndMain.bbWindowsGetData(Sender: TObject);
var
  i: Integer;
  P: TcxTabSheet;
  K, L: Integer;
begin
  bbWindows.Items.Clear;
  K := -1;
  for I := 0 to pcScripts.PageCount - 1 do
  begin
    P := pcScripts.Pages[i];
    if not P.TabVisible then
      Continue;

    L := bbWindows.Items.AddObject(P.Caption, P);
    if pcScripts.ActivePage = P then
      K := L;
  end;
  bbWindows.ItemIndex := K;
end;

procedure TwndMain.actCompileExecute(Sender: TObject);
var
  ScrHlp: TfsScriptHelper;
  ErrStr: string;
begin
  ScrHlp := TfsScriptHelper.Create;
  try
    if not ScrHlp.RunScript(GetScriptText, True, ErrStr) then
      MessageDlg(ErrStr, mtError, [mbOK], 0);
  finally
    ScrHlp.Free;
  end;
end;

procedure TwndMain.actCopyAsMouseClickExecute(Sender: TObject);

  procedure _DetectCoord(const AStr: string; var X, Y: Integer);
  var
    K: Integer;
  begin
    K := Pos(';', AStr);
    if K <= 0 then
      raise Exception.Create('Wrong coordinate: ' + AStr);
    X := StrToInt(Copy(AStr, 1, K - 1));
    Y := StrToInt(Copy(AStr, K + 1, Length(AStr) - K));
  end;

var
  i, K, X, Y, FirstX, FirstY: Integer;
  S, S1, Sx, Sy: string;
  Itm: TListItem;
  BFirst: Boolean;
begin
  S := '';
  BFirst := True;
  for I := 0 to lvCoord.Items.Count - 1 do
  begin
    Itm := lvCoord.Items[i];
    if not Itm.Selected then Continue;

    if BFirst then
    begin
      BFirst := False;
      SX := 'AX';
      SY := 'AY';
      _DetectCoord(Itm.Caption, X, Y);
      FirstX := X;
      FirstY := Y;
    end else
    begin
      _DetectCoord(Itm.Caption, X, Y);
      X := X - FirstX;
      Y := Y - FirstY;
      if X < 0 then
        Sx := 'AX' + IntToStr(X)
      else
        Sx := 'AX + ' + IntToStr(X);

      if Y < 0 then
        SY := 'AY' + IntToStr(Y)
      else
        SY := 'AY + ' + IntToStr(Y);
    end;

    S := S + Format('  MouseClickEx(Wnd, %s, %s, 1);'#13#10, [Sx, Sy]);
  end;

  Clipboard.AsText := S;

end;

procedure TwndMain.actDetectExecute(Sender: TObject);

  procedure _GlobalCoordToClient(const AGlobalX, AGlobalY: Integer; var AClientX, AClientY: Integer);
  var
    Wnd: THandle;
    P: TPoint;
  begin
    P := Point(AGlobalX, AGlobalY);
    Wnd := Windows.WindowFromPoint(P);
    Windows.ScreenToClient(Wnd, P);
    AClientX := P.X;
    AClientY := P.Y
  end;

  function GetXYFromItem(const AItem: TListItem; var AX, AY: Integer): Boolean;
  var
    S: string;
    K: Integer;
  begin
    AX := -1;
    AY := -1;
    Result := False;

    if (AItem.Caption <> '') then
    begin
      S := AItem.Caption;

      K := Pos(';', S);
      if K > 0 then
      begin
        AX := StrToInt(Copy(S, 1, K - 1));
        AY := StrToInt(Trim(Copy(S, K + 1, Length(S) - K)));
        Result := True;
      end;
    end;
  end;

var
  N: Integer;
  bPrev, bFirst: Boolean;
  PrevX, PrevY, FirstX, FirstY, WndX, WndY, X, Y: Integer;
  Itm: TListItem;
begin
  N := lvCoord.Items.Count;

  X := Mouse.CursorPos.X;
  Y := Mouse.CursorPos.Y;
  _GlobalCoordToClient(X, Y, WndX, WndY);

  bPrev := False;
  bFirst := False;
  if N > 0 then
  begin
    bPrev := GetXYFromItem(lvCoord.Items[N - 1], PrevX, PrevY);
    if N > 1 then
      bFirst := GetXYFromItem(lvCoord.Items[0], FirstX, FirstY);
  end;

  Itm := lvCoord.Items.Add;
  Itm.Caption := Format('%d;%d', [X, Y]);
  Itm.SubItems.Add(Format('%d;%d', [WndX, WndY]));
  Itm.SubItems.Add('');
  Itm.SubItems.Add('');
  if bFirst then
  begin
    Itm.SubItems[1] := Format('%d;%d', [X - FirstX, Y - FirstY]);
    if bPrev then
      Itm.SubItems[2] := Format('%d;%d', [X - PrevX, Y - PrevY]);
  end else

  if bPrev then
  begin
    Itm.SubItems[1] := Format('%d;%d', [X - PrevX, Y - PrevY]);
    Itm.SubItems[2] := Format('%d;%d', [X - PrevX, Y - PrevY]);
  end;
end;

procedure TwndMain.actRunExecute(Sender: TObject);
var
  PD: TPageData;
begin
  PD := GetActivePageData;
  if not Assigned(PD) then
    Exit;

  if Assigned(PD.ScriptThread) then Exit;

  PD.ScriptThread := TScriptThread.Create(GetScriptText);
  PD.ScriptThread.OnTerminate := PD.OnThreadTerminate;
  PD.ScriptThread.Start;
//  RunScript(GetScriptText, False);
end;

procedure TwndMain.actRunUpdate(Sender: TObject);
var
  Ed: TFermerSynEdit;
begin
  Ed := GetActiveSynEdit;
  if not Assigned(Ed) then Exit;

  TAction(Sender).Enabled := (ed.Lines.Count > 0) and not Assigned(GetActivePageData.ScriptThread);
end;

procedure TwndMain.ReloadMRUList;
var
  i: Integer;
  L: TList;
  Btn: TdxBarButton;
begin
  L := TList.Create;
  try
    BarManager.GetItemsByCategory(1, L);
    for I := 0 to L.Count - 1 do
      TObject(L[i]).Free;
  finally
    L.Free;
  end;

  pmMRU.ItemLinks.Clear;

  for I := 0 to SLMru.Count - 1 do
  begin
    Btn := BarManager.AddButton;
    Btn.Category := 1;
    Btn.Caption := SLMru[i];
    Btn.Hint := Btn.Caption;
    Btn.OnClick := MRUListClick;

    pmMRU.ItemLinks.Add.Item := Btn;
  end;
end;

procedure TwndMain.FormCreate(Sender: TObject);
begin
  SLMru := TStringList.Create;
  tshDetector.TabVisible := False;
  NewScriptPage;
end;

procedure TwndMain.FormShow(Sender: TObject);
begin
  LoadSettings;
end;

procedure TwndMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveSettings;
  SLMru.Free;
end;

function TwndMain.GetActivePageData: TPageData;
begin
  if pcScripts.ActivePage.Tag = -1 then
    Result := nil
  else
    Result := TPageData(pcScripts.ActivePage.Tag);
end;

function TwndMain.GetActiveSynEdit: TFermerSynEdit;
var
  i: Integer;
  C: TControl;
//  Ed: TFermerSynEdit;
begin
  Result := nil;
  for I := 0 to pcScripts.ActivePage.ControlCount - 1 do
  begin
    C := pcScripts.ActivePage.Controls[i];
    if C is TFermerSynEdit then
    begin
      Result := TFermerSynEdit(C);
      Break;
    end;
  end;
end;

function TwndMain.GetScriptText: string;
var
  Ed: TFermerSynEdit;
begin
  Result := '';

  Ed := GetActiveSynEdit;
  if Ed <> nil then
    Result := Ed.Lines.Text;
end;

procedure TwndMain.LoadSettings;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    Reg.OpenKey('Shao\TerrFermer', True);

    {if Reg.ValueExists('ActiveTab') then
      pcMain.ActivePageIndex := Reg.ReadInteger('ActiveTab');
    }
    if Reg.ValueExists('MRUList') then
    begin
      SLMru.Text := Reg.ReadString('MRUList');
      ReloadMRUList;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TwndMain.lvCoordKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Itm: TListItem;
begin
  if (lvCoord.SelCount <= 0) then Exit;

  if (Key = VK_DELETE) then
    lvCoord.DeleteSelected
  else
  if (Key = Ord('C')) and (ssCtrl in Shift) then
  begin
    Itm := lvCoord.Selected;
    Clipboard.AsText := Format('Cursor: %s; Window: %s; Offset from first: %s; Offset from previus: %s',
      [Itm.Caption, Itm.SubItems[0], Itm.SubItems[1], Itm.SubItems[2]]);
  end;

end;

procedure TwndMain.SaveSettings;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    Reg.OpenKey('Shao\TerrFermer', True);

    //Reg.WriteInteger('ActiveTab', pcMain.ActivePageIndex);

    Reg.WriteString('MRUList', SLMru.Text);
  finally
    Reg.Free;
  end;
end;

procedure TwndMain.MRUListClick(Sender: TObject);
begin
  if Sender is TdxBarButton then
    OpenScriptFile(TdxBarButton(Sender).Hint);
end;

procedure TwndMain.NewScriptPage;
var
  Page: TcxTabSheet;
  SE: TFermerSynEdit;
begin
  Page := TcxTabSheet.Create(pcScripts);
  Page.Caption := '������ (' + IntToStr(pcScripts.PageCount) + ')';
  Page.PageControl := pcScripts;
  pcScripts.ActivePage := Page;

  Page.Tag := Integer(TPageData.Create);

  SE := TFermerSynEdit.Create(Page);
  SE.Parent := Page;
  SE.Align := alClient;
  SE.Gutter.DigitCount := 3;
  SE.Gutter.ShowLineNumbers := True;
  SE.Highlighter := SynPasSyn;
  SE.OnStatusChange := SynEditStatusChange;
end;

procedure TwndMain.OpenScriptFile(const AFileName: string);
var
  Ed: TFermerSynEdit;
begin
  Ed := GetActiveSynEdit;
  if not Assigned(Ed) then Exit;

  ed.Lines.Clear;
  ed.Lines.LoadFromFile(AFileName, TEncoding.Unicode);
  pcScripts.ActivePage.Caption := ChangeFileExt(ExtractFileName(AFileName), '');
  GetActivePageData.FileName := AFileName;

  AddFileToMRUList(AFileName);
end;

procedure TwndMain.SynEditStatusChange(Sender: TObject;
  Changes: TSynStatusChanges);
var
  Ed: TFermerSynEdit;
begin
  Ed := TFermerSynEdit(Sender);
  if (scCaretX in Changes) or (scCaretY in Changes) then
    StatusBar.Panels[0].Text := IntToStr(Ed.CaretY) + ': ' + IntToStr(Ed.CaretX);
end;

procedure TwndMain.tmrCurMouseCoordTimer(Sender: TObject);
begin
  Label3.Caption := IntToStr(mouse.CursorPos.X);
  Label4.Caption := IntToStr(mouse.CursorPos.Y);

  Label6.Caption := '$' + IntToHex(WindowFromPoint(mouse.CursorPos), 8);
end;

{ TPageData }

constructor TPageData.Create;
begin
  FFileName := '';
  FScriptThread := nil;
end;

procedure TPageData.OnThreadTerminate(Sender: TObject);
begin
  FScriptThread := nil;
end;

{ TScriptThread }

constructor TScriptThread.Create(const AScriptText: string);
begin
  inherited Create(True);
  FScriptHelper := TfsScriptHelper.Create;
  FreeOnTerminate := True;
  FScriptText := AScriptText;
end;

destructor TScriptThread.Destroy;
begin
  FScriptHelper.Free;
  inherited;
end;

procedure TScriptThread.DoShowError;
begin
  MessageDlg(FErrorStr, mtError, [mbOK], 0);
end;

procedure TScriptThread.BreakScript;
begin
  FScriptHelper.Script.Terminate2;
  Terminate;
end;

procedure TScriptThread.Execute;
var
  ErrStr: string;
begin
  if not FScriptHelper.RunScript(FScriptText, False, ErrStr) then
  begin
    FErrorStr := ErrStr;
    Synchronize(DoShowError);
  end;
end;

{ TFermfsScript }

constructor TFermfsScript.Create(AOwner: TComponent);
begin
  inherited;
  FTerminated := False;
end;

procedure TFermfsScript.Terminate2;
begin
  FTerminated := True;
  Terminate;
end;

end.
