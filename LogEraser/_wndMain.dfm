object wndMain: TwndMain
  Left = 0
  Top = 0
  Caption = 'File killer'
  ClientHeight = 691
  ClientWidth = 815
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 815
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 635
    DesignSize = (
      815
      48)
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 48
      Height = 13
      Caption = 'Base path'
    end
    object edBasePath: TcxButtonEdit
      Left = 8
      Top = 24
      Anchors = [akLeft, akTop, akRight]
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = edBasePathPropertiesButtonClick
      TabOrder = 0
      ExplicitWidth = 616
      Width = 799
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 815
    Height = 623
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 264
    ExplicitTop = 56
    ExplicitWidth = 185
    ExplicitHeight = 41
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 815
      Height = 400
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 635
      DesignSize = (
        815
        400)
      object Label2: TLabel
        Left = 8
        Top = 2
        Width = 50
        Height = 13
        Caption = 'File names'
      end
      object tlFiles: TcxTreeList
        Left = 8
        Top = 18
        Width = 799
        Height = 369
        Anchors = [akLeft, akTop, akRight, akBottom]
        Bands = <
          item
          end>
        OptionsData.Deleting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.ShowRoot = False
        PopupMenu = PopupMenu
        TabOrder = 0
        object clnFiles: TcxTreeListColumn
          PropertiesClassName = 'TcxCheckBoxProperties'
          DataBinding.ValueType = 'String'
          Width = 32
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object clnFileName: TcxTreeListColumn
          Caption.Text = 'Name'
          DataBinding.ValueType = 'String'
          Options.Editing = False
          Width = 577
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 408
      Width = 815
      Height = 177
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 400
      ExplicitHeight = 300
      DesignSize = (
        815
        177)
      object Label3: TLabel
        Left = 8
        Top = 2
        Width = 52
        Height = 13
        Caption = 'Extensions'
      end
      object tlExt: TcxTreeList
        Left = 8
        Top = 18
        Width = 799
        Height = 157
        Anchors = [akLeft, akTop, akRight, akBottom]
        Bands = <
          item
          end>
        OptionsData.Deleting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.ShowRoot = False
        PopupMenu = PopupMenu
        TabOrder = 0
        ExplicitHeight = 165
        object clnExt: TcxTreeListColumn
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.OnChange = clnExtPropertiesChange
          DataBinding.ValueType = 'String'
          Width = 32
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object clnExtName: TcxTreeListColumn
          Caption.Text = 'Name'
          DataBinding.ValueType = 'String'
          Options.Editing = False
          Width = 577
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          SortOrder = soAscending
          SortIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 585
      Width = 815
      Height = 38
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        815
        38)
      object btnScan: TcxButton
        Left = 651
        Top = 6
        Width = 75
        Height = 25
        Action = actScan
        Anchors = [akTop, akRight]
        TabOrder = 0
      end
      object btnErase: TcxButton
        Left = 732
        Top = 6
        Width = 75
        Height = 25
        Action = actErase
        Anchors = [akTop, akRight]
        TabOrder = 1
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 0
      Top = 400
      Width = 815
      Height = 8
      HotZoneClassName = 'TcxXPTaskBarStyle'
      AlignSplitter = salTop
      Control = Panel4
      ExplicitWidth = 185
    end
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 671
    Width = 815
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarContainerPanelStyle'
        PanelStyle.Container = dxStatusBar1Container0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = -20
    object dxStatusBar1Container0: TdxStatusBarContainerControl
      Left = 0
      Top = 0
      Width = 799
      Height = 18
      object pbProgress: TcxProgressBar
        Left = 0
        Top = 0
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 328
        ExplicitTop = -3
        ExplicitWidth = 121
        ExplicitHeight = 21
        Width = 799
      end
    end
  end
  object LookAndFeelController: TcxLookAndFeelController
    Left = 584
    Top = 320
  end
  object ActionList: TActionList
    Left = 616
    Top = 320
    object actScan: TAction
      Caption = 'Scan'
      Hint = 'Scan'
      OnExecute = actScanExecute
      OnUpdate = actScanUpdate
    end
    object actErase: TAction
      Caption = 'Erase'
      Hint = 'Erase'
      OnExecute = actEraseExecute
      OnUpdate = actEraseUpdate
    end
  end
  object PopupMenu: TPopupMenu
    Left = 648
    Top = 320
    object miCheckAll: TMenuItem
      Caption = 'Check all'
      OnClick = miCheckAllClick
    end
    object miUncheckAll: TMenuItem
      Tag = 1
      Caption = 'Uncheck all'
      OnClick = miCheckAllClick
    end
    object miInvert: TMenuItem
      Caption = 'Invert check'
      OnClick = miInvertClick
    end
  end
end
